/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_LIST_ROLLER_H
#define _RHAYADER_WEBBROWSER_LIST_ROLLER_H

#include <glib-object.h>

#include <clutter/clutter.h>

#pragma GCC diagnostic push
/* FIXME: stop disabling these warnings when the libraries are fixed.
 * https://bugs.apertis.org/show_bug.cgi?id=566 (Thornbury)
 * https://bugs.apertis.org/show_bug.cgi?id=567 (Mx)
 * https://bugs.apertis.org/show_bug.cgi?id=569 (Lightwood) */
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <liblightwood-roller.h>
#include <libthornbury-model.h>
#include <mx/mx.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_LIST_ROLLER rhayader_webbrowser_list_roller_get_type()

#define RHAYADER_WEBBROWSER_LIST_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_ROLLER, RhayaderWebBrowserListRoller))

#define RHAYADER_WEBBROWSER_LIST_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_LIST_ROLLER, RhayaderWebBrowserListRollerClass))

#define RHAYADER_IS_WEBBROWSER_LIST_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_ROLLER))

#define RHAYADER_IS_WEBBROWSER_LIST_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_LIST_ROLLER))

#define RHAYADER_WEBBROWSER_LIST_ROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_ROLLER, RhayaderWebBrowserListRollerClass))

typedef struct _RhayaderWebBrowserListRoller RhayaderWebBrowserListRoller;
typedef struct _RhayaderWebBrowserListRollerClass RhayaderWebBrowserListRollerClass;
typedef struct _RhayaderWebBrowserListRollerPrivate RhayaderWebBrowserListRollerPrivate;

struct _RhayaderWebBrowserListRoller
{
	ClutterActor parent;

  RhayaderWebBrowserListRollerPrivate *priv;
};

struct _RhayaderWebBrowserListRollerClass
{
	ClutterActorClass parent_class;
};

GType rhayader_webbrowser_list_roller_get_type (void) G_GNUC_CONST;

RhayaderWebBrowserListRoller *rhayader_webbrowser_list_roller_new (gfloat width,gfloat height,GType item_type,ThornburyModel *model);
gboolean rhayader_webbrowser_list_roller_is_moving(RhayaderWebBrowserListRoller * self);
gint rhayader_webbrowser_list_roller_get_focused_row(RhayaderWebBrowserListRoller * self);
void rhayader_webbrowser_list_roller_set_focused_row(RhayaderWebBrowserListRoller * self,gint row,gboolean is_animate,gboolean is_activate);
ClutterActor* rhayader_webbrowser_list_roller_get_item(RhayaderWebBrowserListRoller * self,gint rowid);
gfloat rhayader_webbrowser_list_roller_zoom_item_get_focussed_value(RhayaderWebBrowserListRoller * self);

G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_LIST_ROLLER_H */

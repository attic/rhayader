/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-list-item.h"
#include "rhayader-utils.h"


static  gfloat RHAYADER_WEBBROWSER_LIST_ITEM_WIDTH = 655.0;

#define RHAYADER_WEBBROWSER_TABS_THUMBNAIL_COORDS		0,1
#define RHAYADER_WEBBROWSER_TABS_THUMBNAIL_DIM 			(0.25*RHAYADER_WEBBROWSER_LIST_ITEM_WIDTH) -4,95.0

#define RHAYADER_WEBBROWSER_TAB_DETAIL_WIDTH				(0.75*RHAYADER_WEBBROWSER_LIST_ITEM_WIDTH)
#define RHAYADER_WEBBROWSER_TAB_DETAIL_HEIGHT 			97.0

#define RHAYADER_WEBBROWSER_TABS_DETAIL_COORDS			(0.25*RHAYADER_WEBBROWSER_LIST_ITEM_WIDTH),0
#define RHAYADER_WEBBROWSER_TABS_DETAIL_DIM			    RHAYADER_WEBBROWSER_TAB_DETAIL_WIDTH,97.0

#define	RHAYADER_WEBBROWSER_TAB_DETAIL_LEFT_LINE_COORDS  		0,0
#define RHAYADER_WEBBROWSER_TAB_DETAIL_LEFT_LINE_DIM          	1,RHAYADER_WEBBROWSER_TAB_DETAIL_HEIGHT

#define RHAYADER_WEBBROWSER_TAB_DETAIL_BOTTOM_LINE_COORDS        2,RHAYADER_WEBBROWSER_TAB_DETAIL_HEIGHT
#define RHAYADER_WEBBROWSER_TAB_DETAIL_BOTTOM_LINE_DIM          RHAYADER_WEBBROWSER_TAB_DETAIL_WIDTH - 2,1

#define RHAYADER_WEBBROWSER_TAB_DETAIL_TEXT_X					10.0
#define	RHAYADER_WEBBROWSER_TAB_DETAIL_TEXT1_Y            		37-28.0
#define RHAYADER_WEBBROWSER_TAB_DETAIL_TEXT2_Y            		73-18.0


#define RHAYADER_WEBBROWSER_FONT(size) "DejaVuSansCondensed " #size"px"
G_DEFINE_TYPE (RhayaderWebBrowserListItem, rhayader_webbrowser_list_item, CLUTTER_TYPE_GROUP)

enum
{
  PROP_0,
  PROP_FOCUSED,
  PROP_TAB,
};

enum
{
  COLUMN_TAB,
  COLUMN_LAST
};

#define WEBBROWSER_LIST_ITEM_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_LIST_ITEM, RhayaderWebBrowserListItemPrivate))

struct _RhayaderWebBrowserListItemPrivate
{
	ClutterText*    m_title;
	ClutterText*    m_url;
	ClutterActor*   m_tab;
	gboolean  		m_is_highlighted;

	ClutterActor*  m_tabThumbnail;
	ClutterActor* 	m_tabDetail;
	ClutterEffect *glow_effect_1;
	ClutterEffect *glow_effect_2;
};

static void
_rhayader_webbrowser_list_item_detail_url_change_cb(ClutterActor* tab,GParamSpec* pspec,RhayaderWebBrowserListItem* self);
static void
_rhayader_webbrowser_list_item_set_tab(RhayaderWebBrowserListItem *self,ClutterActor* tab);
static ClutterActor*
_rhayader_webbrowser_list_item_create_detail(RhayaderWebBrowserListItem *self);

static void
rhayader_webbrowser_list_item_get_property (GObject    *object,
                                       guint       property_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
	RhayaderWebBrowserListItem *self = RHAYADER_WEBBROWSER_LIST_ITEM (object);
	RhayaderWebBrowserListItemPrivate* priv =  WEBBROWSER_LIST_ITEM_PRIVATE (self);

  switch (property_id)
    {
  	  case PROP_TAB:
  		g_value_set_pointer(value,priv->m_tab);
  		break;

     default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}


static void
rhayader_webbrowser_list_item_set_property (GObject      *object,
                                       guint         property_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
	RhayaderWebBrowserListItem *self = RHAYADER_WEBBROWSER_LIST_ITEM (object);
	RhayaderWebBrowserListItemPrivate* priv =  WEBBROWSER_LIST_ITEM_PRIVATE (self);

  switch (property_id)
    {
  	  case PROP_TAB:
  	  {
  		//clutter_actor_set_opacity(self,0xFF);
  		  ClutterActor* tab = g_value_get_pointer (value);
  		  if(tab == priv->m_tab)
  			  return;

  		  g_print("\n setting tab");
  		_rhayader_webbrowser_list_item_set_tab(self,tab);


/*  		  if(!cur_tab)
  		  {
  			   clutter_actor_set_opacity(tab,0x00);
  			   clutter_actor_save_easing_state (tab);
  			   clutter_actor_set_easing_duration (tab, 500);
  			   clutter_actor_set_easing_mode (tab, CLUTTER_LINEAR);
  			   clutter_actor_set_opacity(tab,0xFF);
  			   clutter_actor_restore_easing_state (tab);
  		  }*/

  	  }
  	  break;

  	  case PROP_FOCUSED:
  	  {
  		rhayader_webbrowser_list_item_set_focus(self,g_value_get_boolean (value));
  	  }
       break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
rhayader_webbrowser_list_item_dispose (GObject *object)
{
  RhayaderWebBrowserListItemPrivate *priv = WEBBROWSER_LIST_ITEM_PRIVATE (object);

  g_clear_object (&priv->glow_effect_1);
  g_clear_object (&priv->glow_effect_2);

  G_OBJECT_CLASS (rhayader_webbrowser_list_item_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_list_item_class_init (RhayaderWebBrowserListItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GParamSpec *pspec;

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserListItemPrivate));

  object_class->get_property = rhayader_webbrowser_list_item_get_property;
  object_class->set_property = rhayader_webbrowser_list_item_set_property;
  object_class->dispose = rhayader_webbrowser_list_item_dispose;

  pspec = g_param_spec_pointer ("tab",
                               "tab",
                               "webview tab instance",
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_TAB, pspec);

  pspec = g_param_spec_boolean ("focused",
                               "focused",
                               "is focused",
                               FALSE,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_FOCUSED, pspec);
}




static void
rhayader_webbrowser_list_item_init (RhayaderWebBrowserListItem *self)
{
  RhayaderWebBrowserListItemPrivate* priv = self->priv = WEBBROWSER_LIST_ITEM_PRIVATE (self);

  clutter_actor_set_reactive(CLUTTER_ACTOR(self),TRUE);

  priv->m_tabDetail   = _rhayader_webbrowser_list_item_create_detail(self);
  clutter_actor_add_child(CLUTTER_ACTOR(self),CLUTTER_ACTOR(priv->m_tabDetail));
  clutter_actor_set_position(priv->m_tabDetail,RHAYADER_WEBBROWSER_TABS_DETAIL_COORDS);
//  ClutterActor* dummy = clutter_texture_new();
 // clutter_actor_hide(dummy);
 // clutter_actor_add_child(CLUTTER_ACTOR(self),dummy);
  priv->glow_effect_1 = g_object_ref_sink (lightwood_glow_shader_new ());
  priv->glow_effect_2 = g_object_ref_sink (lightwood_glow_shader_new ());

  return;
}
void rhayader_webbrowser_list_item_configure_width(gfloat item_width)
{
	RHAYADER_WEBBROWSER_LIST_ITEM_WIDTH = item_width;
}
RhayaderWebBrowserListItem *
rhayader_webbrowser_list_item_new (void)
{
	RhayaderWebBrowserListItem* self = g_object_new (RHAYADER_TYPE_WEBBROWSER_LIST_ITEM, NULL);
//clutter_actor_set_opacity(CLUTTER_ACTOR(self),0x00);
        clutter_actor_hide(CLUTTER_ACTOR(self));

    return self;
}

void rhayader_webbrowser_list_item_set_focus(RhayaderWebBrowserListItem* self,gboolean is_focus)
{
	RhayaderWebBrowserListItemPrivate* priv = self->priv = WEBBROWSER_LIST_ITEM_PRIVATE (self);
	if (is_focus)
	{
		if (clutter_actor_get_effect (CLUTTER_ACTOR(priv->m_title), "glow") != NULL)
			return;

		clutter_actor_add_effect_with_name (CLUTTER_ACTOR(priv->m_title), "glow", priv->glow_effect_1);
		clutter_actor_add_effect_with_name (CLUTTER_ACTOR(priv->m_url), "glow", priv->glow_effect_2);
	}
	else
	{
		if (clutter_actor_get_effect (CLUTTER_ACTOR(priv->m_title), "glow") == NULL)
			return;

		clutter_actor_remove_effect_by_name (CLUTTER_ACTOR(priv->m_title), "glow");
		clutter_actor_remove_effect_by_name (CLUTTER_ACTOR(priv->m_url), "glow");
	}


}

void rhayader_webbrowser_list_item_restore_focussed_tab(RhayaderWebBrowserListItem* self,ClutterActor* tab)
{
	//clutter_actor_show(tab);
	_rhayader_webbrowser_list_item_set_tab(self,tab);
}

static void
rhayader_webbrowser_list_item_refresh_detail (RhayaderWebBrowserListItem *self,
    const gchar *title,
    const gchar *url)
{
	RhayaderWebBrowserListItemPrivate* priv =  WEBBROWSER_LIST_ITEM_PRIVATE (self);

		if(!title || g_str_equal(title,""))
			title="NEW VIEW";
		clutter_text_set_text(CLUTTER_TEXT(priv->m_title),title);
		if(url && !g_str_equal(url,""))
			clutter_text_set_text(CLUTTER_TEXT(priv->m_url),url);
		//clutter_actor_queue_relayout(CLUTTER_ACTOR(self));
	  rhayader_webbrowser_list_item_set_focus(self,priv->m_is_highlighted);
}

static void _rhayader_webbrowser_list_item_detail_url_change_cb(ClutterActor* tab,GParamSpec* pspec,RhayaderWebBrowserListItem* self)
{
	RhayaderWebBrowserListItemPrivate* priv = self->priv = WEBBROWSER_LIST_ITEM_PRIVATE (self);

	  gchar* url="";gchar* title="NEW VIEW";
	  g_object_get(priv->m_tab,"url",&url,"title",&title,NULL);
	  g_print("\n url updated %s",url);
	 rhayader_webbrowser_list_item_refresh_detail(self,title,url);
}

static void set_webview_actor(RhayaderWebBrowserListItem *self)
{
	GList *pChildList = NULL;
		gint nChildren,index;
		gboolean isContains = FALSE;
		gboolean isRemoved = FALSE;
			pChildList = clutter_actor_get_children(CLUTTER_ACTOR(self));
			nChildren = clutter_actor_get_n_children(CLUTTER_ACTOR(self));
			for(index = 0 ; index < nChildren ; index++)
			{
				gpointer data = g_list_nth_data(pChildList,index);
				if(CLUTTER_IS_ACTOR(data) && g_strcmp0(clutter_actor_get_name(CLUTTER_ACTOR(data)), "webview-actor") == 0)
				{
					isContains = TRUE;
					if(self->priv->m_tab != CLUTTER_ACTOR(data))
					{
						clutter_actor_remove_child(CLUTTER_ACTOR(self),CLUTTER_ACTOR(data));
						isRemoved = TRUE;
					}
				}
			}

			if(isRemoved)
			{
				ClutterActor *parent = clutter_actor_get_parent(CLUTTER_ACTOR(self->priv->m_tab));
				if(parent)
					clutter_actor_remove_child(CLUTTER_ACTOR(parent),CLUTTER_ACTOR(self->priv->m_tab));
				clutter_actor_add_child(CLUTTER_ACTOR(self),CLUTTER_ACTOR(self->priv->m_tab));
				//clutter_actor_hide(CLUTTER_ACTOR(self->priv->m_tab));
			}
			if(!(isContains))
			{
				ClutterActor *parent = clutter_actor_get_parent(CLUTTER_ACTOR(self->priv->m_tab));
				if(parent)
					clutter_actor_remove_child(CLUTTER_ACTOR(parent),CLUTTER_ACTOR(self->priv->m_tab));
				clutter_actor_add_child(CLUTTER_ACTOR(self),CLUTTER_ACTOR(self->priv->m_tab));
				//clutter_actor_hide(CLUTTER_ACTOR(self->priv->m_tab));
			}
			clutter_actor_set_reactive(CLUTTER_ACTOR(self->priv->m_tab),TRUE);
}


static void _rhayader_webbrowser_list_item_set_tab(RhayaderWebBrowserListItem *self,ClutterActor* tab)
{
	RhayaderWebBrowserListItemPrivate* priv = self->priv = WEBBROWSER_LIST_ITEM_PRIVATE (self);
	gchar *url = NULL;
	gchar *title = NULL;

	if(CLUTTER_ACTOR(priv->m_tab))
		g_signal_handlers_disconnect_by_func(priv->m_tab,G_CALLBACK(_rhayader_webbrowser_list_item_detail_url_change_cb),self);
	priv->m_tab = g_object_ref(tab);
	clutter_actor_set_size(priv->m_tab,RHAYADER_WEBBROWSER_TABS_THUMBNAIL_DIM);
	clutter_actor_set_name(self->priv->m_tab, "webview-actor");

		clutter_actor_set_reactive(priv->m_tab,TRUE);
		clutter_actor_set_position(priv->m_tab,0.0,0.0);
		g_print("\n adding child tab %d",(gint)tab);
	 set_webview_actor(self);
	g_assert(CLUTTER_IS_ACTOR(priv->m_tab));
	g_print("\n added child");

	g_object_get(priv->m_tab,"url",&url,"title",&title,NULL);
	if(!url)
	{
		url = g_object_get_data(G_OBJECT(priv->m_tab),"request_url");
		title = g_object_get_data(G_OBJECT(priv->m_tab),"title");
	}
	rhayader_webbrowser_list_item_refresh_detail(self,title,url);

	g_signal_connect(priv->m_tab,
				  	 "notify::url",G_CALLBACK(_rhayader_webbrowser_list_item_detail_url_change_cb),self);

	return;


}


static ClutterActor*
_rhayader_webbrowser_list_item_create_detail(RhayaderWebBrowserListItem *self)
{
	 RhayaderWebBrowserListItemPrivate* priv = self->priv = WEBBROWSER_LIST_ITEM_PRIVATE (self);
  //build the stuff
	//ClutterColor backGroundColor = {0x00,0x00,0x00,0x00};
	const ClutterColor lineColor = {0xFF,0xFF,0xFF,0x33};
	const ClutterColor textColor = {0x98, 0xA9, 0xB2, 0xFF};
	ClutterActor *line;

	/*Set the width for the entire detail group */
	ClutterActor* detail_group = clutter_actor_new();
	clutter_actor_set_layout_manager(detail_group,clutter_fixed_layout_new());
	clutter_actor_set_width(CLUTTER_ACTOR(detail_group), RHAYADER_WEBBROWSER_TAB_DETAIL_WIDTH);
	clutter_actor_set_height(CLUTTER_ACTOR(detail_group), RHAYADER_WEBBROWSER_TAB_DETAIL_HEIGHT);
	//starting line
	create_and_position_line(lineColor,
				0,0,RHAYADER_WEBBROWSER_TAB_DETAIL_WIDTH,1,
				CLUTTER_ACTOR(detail_group));


	/*Create and position the line between the image and the text */
	line = create_and_position_line (lineColor,
			RHAYADER_WEBBROWSER_TAB_DETAIL_LEFT_LINE_COORDS,RHAYADER_WEBBROWSER_TAB_DETAIL_LEFT_LINE_DIM,
			CLUTTER_ACTOR(detail_group));
	//clutter_actor_set_opacity(line,20);

	/*Create and position the line at the bottom*/
	line = create_and_position_line(lineColor,
			RHAYADER_WEBBROWSER_TAB_DETAIL_BOTTOM_LINE_COORDS,RHAYADER_WEBBROWSER_TAB_DETAIL_BOTTOM_LINE_DIM,
			CLUTTER_ACTOR(detail_group));
	//clutter_actor_set_opacity(line,20);

	priv->m_title = (ClutterText*)create_and_position_text(RHAYADER_WEBBROWSER_FONT(24), textColor,
			RHAYADER_WEBBROWSER_TAB_DETAIL_TEXT_X, RHAYADER_WEBBROWSER_TAB_DETAIL_TEXT1_Y, CLUTTER_ACTOR(detail_group));
	clutter_actor_set_width(CLUTTER_ACTOR(priv->m_title),RHAYADER_WEBBROWSER_TAB_DETAIL_WIDTH -2.0);
	clutter_text_set_ellipsize(priv->m_title,3);
	priv->m_url = (ClutterText*)create_and_position_text(RHAYADER_WEBBROWSER_FONT(16), textColor,
			RHAYADER_WEBBROWSER_TAB_DETAIL_TEXT_X, RHAYADER_WEBBROWSER_TAB_DETAIL_TEXT2_Y, CLUTTER_ACTOR(detail_group));
	clutter_actor_set_width(CLUTTER_ACTOR(priv->m_url),RHAYADER_WEBBROWSER_TAB_DETAIL_WIDTH -2.0);
	clutter_text_set_ellipsize(priv->m_url,3);
	//rhayader_webbrowser_tab_detail_set_detail(self,"","NEW VIEW");

	return detail_group;
}



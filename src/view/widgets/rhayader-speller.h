/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __RHAYADER_WEBBROWSER_SPELLER_H__
#define __RHAYADER_WEBBROWSER_SPELLER_H__

#include <glib-object.h>
#include <clutter/clutter.h>

#pragma GCC diagnostic push
/* FIXME: stop disabling these warnings when the libraries are fixed.
 * https://bugs.apertis.org/show_bug.cgi?id=566 (Thornbury)
 * https://bugs.apertis.org/show_bug.cgi?id=567 (Mx) */
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <libthornbury-ui-texture.h>
#include <mx/mx.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_SPELLER rhayader_webbrowser_speller_get_type()

#define RHAYADER_WEBBROWSER_SPELLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_SPELLER, RhayaderWebbrowserSpeller))

#define RHAYADER_WEBBROWSER_SPELLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_SPELLER, RhayaderWebbrowserSpellerClass))

#define RHAYADER_IS_WEBBROWSER_SPELLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_SPELLER))

#define RHAYADER_IS_WEBBROWSER_SPELLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_SPELLER))

#define RHAYADER_WEBBROWSER_SPELLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_SPELLER, RhayaderWebbrowserSpellerClass))

typedef struct _RhayaderWebbrowserSpeller RhayaderWebbrowserSpeller;
typedef struct _RhayaderWebbrowserSpellerClass RhayaderWebbrowserSpellerClass;
typedef struct _RhayaderWebbrowserSpellerPrivate RhayaderWebbrowserSpellerPrivate;

struct _RhayaderWebbrowserSpeller
{
	ClutterActor parent;

  RhayaderWebbrowserSpellerPrivate *priv;
};

struct _RhayaderWebbrowserSpellerClass
{
	ClutterActorClass parent_class;
};

GType rhayader_webbrowser_speller_get_type (void) G_GNUC_CONST;

RhayaderWebbrowserSpeller *rhayader_webbrowser_speller_new (void);
void rhayader_webbrowser_speller_show (RhayaderWebbrowserSpeller* self);
void rhayader_webbrowser_speller_hide(RhayaderWebbrowserSpeller* self,gboolean is_animation);
//void rhayader_webbrowser_speller_set_visible(RhayaderWebbrowserSpeller* self,gboolean is_shown);
void rhayader_webbrowser_speller_set_progress (RhayaderWebbrowserSpeller* self,gdouble percent);
void rhayader_webbrowser_speller_set_title (RhayaderWebbrowserSpeller* self,
    const gchar *title);
void rhayader_webbrowser_speller_set_url (RhayaderWebbrowserSpeller* self,
    const gchar *url);
gchar* rhayader_webbrowser_speller_autocomplete_url(const gchar* url,const gboolean is_url);

G_END_DECLS

#endif /* __RHAYADER_WEBBROWSER_SPELLER_H__ */

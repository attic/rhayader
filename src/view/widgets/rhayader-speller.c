/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-speller.h"
#include "libthornbury-itemfactory.h"
#include "libthornbury-widgetparser.h"
#include <mildenhall/mildenhall_speller.h>
#include <libthornbury-ui-texture.h>
#include <libsoup-2.4/libsoup/soup.h>

enum //for default entry
{
    COLUMN_LEFT_IS_TEXT,
    COLUMN_LEFT_WIDTH,
    COLUMN_LEFT_ID,
    COLUMN_LEFT_INFO,
    COLUMN_RIGHT_IS_TEXT,
    COLUMN_RIGHT_WIDTH,
    COLUMN_RIGHT_ID,
    COLUMN_RIGHT_INFO,
    COLUMN_TEXT_BOX_DEFAULT_TEXT,
    COLUMN_ENTRY_ID,
    COLUMN_LAST
};
enum
{
	WEBBROWSER_SPELLER_VISIBLE,
	WEBBROWSER_SPELLER_DATA_ENTERED,
	WEBBROWSER_SPELLER_SCROLLTOTOP,
	WEBBROWSER_SPELLER_PAUSE,
	WEBBROWSER_SPELLER_LAST_SIGNAL
};
static guint signals[WEBBROWSER_SPELLER_LAST_SIGNAL] = { 0, };

G_DEFINE_TYPE (RhayaderWebbrowserSpeller, rhayader_webbrowser_speller, CLUTTER_TYPE_ACTOR)


#define WEBBROWSER_SPELLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_SPELLER, RhayaderWebbrowserSpellerPrivate))

struct _RhayaderWebbrowserSpellerPrivate
{
	MildenhallSpeller* 	m_speller;
	ThornburyModel 		*m_spellerModel;
	gboolean 		m_isShown;
	gboolean 		m_isSearch;

	gchar* m_title;
	gchar* m_url;
	gchar* m_search;

	ClutterActor* m_loadProgressBar;
	ClutterActor* m_progress_end_img;

	//gboolean m_is_speller_state_changed;
	ClutterAction* m_click_action;
};

static void
_rhayader_webbrowser_speller_refresh (RhayaderWebbrowserSpeller* self);
/*static void
_rhayader_webbrowser_speller_clicked_cb(ClutterClickAction* action,RhayaderWebbrowserSpeller* self);
static gboolean
_rhayader_webbrowser_speller_long_pressed_cb(ClutterClickAction* action,RhayaderWebbrowserSpeller* self,ClutterLongPressState state);*/
static gboolean
_rhayader_webbrowser_speller_shown_cb(MildenhallSpeller* speller,RhayaderWebbrowserSpeller* self);
static gboolean
_rhayader_webbrowser_speller_hidden_cb(MildenhallSpeller* speller,RhayaderWebbrowserSpeller* self);
static gboolean
_rhayader_webbrowser_speller_go_pressed_cb(MildenhallSpeller* speller,GVariant *pVariant,RhayaderWebbrowserSpeller* self);
static void
_rhayader_webbrowser_speller_entry_status_cb(GObject *pButton, gchar *pButtonName ,gchar *pButtonId,RhayaderWebbrowserSpeller *self);

static void
rhayader_webbrowser_speller_get_property (GObject    *object,
                                    guint       property_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
rhayader_webbrowser_speller_set_property (GObject      *object,
                                    guint         property_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
rhayader_webbrowser_speller_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_speller_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_speller_class_init (RhayaderWebbrowserSpellerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebbrowserSpellerPrivate));

  object_class->get_property = rhayader_webbrowser_speller_get_property;
  object_class->set_property = rhayader_webbrowser_speller_set_property;
  object_class->dispose = rhayader_webbrowser_speller_dispose;

  signals[WEBBROWSER_SPELLER_VISIBLE] =
      g_signal_new ("webbrowser_speller_visible",
    		  RHAYADER_TYPE_WEBBROWSER_SPELLER,
                    G_SIGNAL_RUN_LAST,
                    0,
                    NULL, NULL,
                    g_cclosure_marshal_VOID__BOOLEAN,
                    G_TYPE_NONE, 1, G_TYPE_BOOLEAN);

  signals[WEBBROWSER_SPELLER_DATA_ENTERED] =
      g_signal_new ("webbrowser_speller_data_entered",
    		  RHAYADER_TYPE_WEBBROWSER_SPELLER,
                    G_SIGNAL_RUN_LAST,
                    0,
                    NULL, NULL,
                    g_cclosure_marshal_VOID__STRING,
                    G_TYPE_NONE, 1, G_TYPE_STRING);

  signals[WEBBROWSER_SPELLER_SCROLLTOTOP] =
      g_signal_new ("webbrowser_speller_scrolltotop",
    		  RHAYADER_TYPE_WEBBROWSER_SPELLER,
                    G_SIGNAL_RUN_LAST,
                    0,
                    NULL, NULL,
                    g_cclosure_marshal_VOID__VOID,
                    G_TYPE_NONE, 0);

  signals[WEBBROWSER_SPELLER_PAUSE] =
      g_signal_new ("webbrowser_speller_PAUSE",
    		  RHAYADER_TYPE_WEBBROWSER_SPELLER,
                    G_SIGNAL_RUN_LAST,
                    0,
                    NULL, NULL,
                    g_cclosure_marshal_VOID__VOID,
                    G_TYPE_NONE, 0);

}


static void
rhayader_webbrowser_speller_init (RhayaderWebbrowserSpeller *self)
{
	RhayaderWebbrowserSpellerPrivate* priv = self->priv = WEBBROWSER_SPELLER_PRIVATE (self);
	ThornburyItemFactory *itemFactory;
	GVariantBuilder *pRowValues = NULL;
	GVariant *p;
	GVariantBuilder *pRightRowValues = NULL;
	GVariant *pRight;
	ClutterColor barColor = {0x3E,0x73,0xB3,0xFF};
	ClutterActor *loadProgress;

	clutter_actor_set_height(CLUTTER_ACTOR(self),66.0);
	clutter_actor_set_reactive(CLUTTER_ACTOR(self),TRUE);
	clutter_actor_set_layout_manager(CLUTTER_ACTOR(self),clutter_fixed_layout_new());
	//clutter_actor_set_layout_manager(CLUTTER_ACTOR(self),clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL));

	priv->m_isShown = FALSE;
//	priv->m_is_speller_state_changed = TRUE;
	clutter_actor_show(CLUTTER_ACTOR(self));

	itemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_SPELLER, STYLEDIR "/rhayader-speller-prop.json");

	  g_object_get(itemFactory, "object", &priv->m_speller, NULL);

		  priv->m_spellerModel = THORNBURY_MODEL(thornbury_list_model_new (  COLUMN_LAST,
													  G_TYPE_BOOLEAN ,0,
													  G_TYPE_FLOAT , 0,
													  G_TYPE_STRING, NULL,
													  G_TYPE_POINTER, NULL,
													  G_TYPE_BOOLEAN ,0,
													  G_TYPE_FLOAT , 0,
													  G_TYPE_STRING, NULL,
													  G_TYPE_POINTER, NULL,
													  G_TYPE_STRING, NULL,
													  G_TYPE_STRING, NULL,
													  -1));

		  pRowValues = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );
		  g_variant_builder_add(pRowValues, "{ss}", "url","URL");
		  g_variant_builder_add(pRowValues, "{ss}", "search","SEARCH");
		  p = g_variant_builder_end(pRowValues);

		  pRightRowValues = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );
		  g_variant_builder_add(pRightRowValues, "{ss}", "right", ICONDIR"/icon_browser_backtotop_AC.png");
		  pRight = g_variant_builder_end(pRightRowValues);

			  /* append model info for default entry */
		  thornbury_model_append ( priv->m_spellerModel,
							   COLUMN_LEFT_IS_TEXT ,TRUE,
							   COLUMN_LEFT_WIDTH ,100.0,
							   COLUMN_LEFT_ID ,g_strdup("urlorsearch"),
							   COLUMN_LEFT_INFO, (gpointer)p,
							   COLUMN_RIGHT_IS_TEXT ,FALSE,
							   COLUMN_RIGHT_WIDTH ,64.0,
							   COLUMN_RIGHT_ID ,g_strdup("scrolltotop"),
							   COLUMN_RIGHT_INFO, (gpointer)pRight,
							   COLUMN_TEXT_BOX_DEFAULT_TEXT, "",
							   COLUMN_ENTRY_ID ,g_strdup("entry"),
							   -1);

		  g_object_set(priv->m_speller,"model", priv->m_spellerModel, NULL);

    g_object_set(priv->m_speller,"reactive",FALSE,NULL);
	g_signal_connect_after (priv->m_speller, "speller-shown", G_CALLBACK (_rhayader_webbrowser_speller_shown_cb), self);
	g_signal_connect_after (priv->m_speller, "speller-hidden", G_CALLBACK (_rhayader_webbrowser_speller_hidden_cb), self);
	g_signal_connect(priv->m_speller, "go-pressed",G_CALLBACK(_rhayader_webbrowser_speller_go_pressed_cb), self);
	g_signal_connect_after (priv->m_speller, "entry-status", G_CALLBACK (_rhayader_webbrowser_speller_entry_status_cb), self);

	clutter_actor_set_size(CLUTTER_ACTOR(self),clutter_actor_get_width(CLUTTER_ACTOR(priv->m_speller)),62.0);

	   g_object_set(CLUTTER_ACTOR(self->priv->m_speller),
				"x-expand",FALSE,
				"x-align",CLUTTER_ACTOR_ALIGN_START,
				"x",6.0,
				NULL);
	clutter_actor_add_child(CLUTTER_ACTOR(self),CLUTTER_ACTOR(priv->m_speller));


	priv->m_loadProgressBar = clutter_actor_new();
	clutter_actor_set_reactive(priv->m_loadProgressBar,FALSE);
	clutter_actor_set_size(priv->m_loadProgressBar,50.0,60.0);
	clutter_actor_set_layout_manager(CLUTTER_ACTOR(priv->m_loadProgressBar),
			g_object_new(CLUTTER_TYPE_BOX_LAYOUT,"orientation",CLUTTER_ORIENTATION_HORIZONTAL,"pack-start",TRUE,NULL));
	clutter_actor_add_child(CLUTTER_ACTOR(self),priv->m_loadProgressBar);
	clutter_actor_set_position(priv->m_loadProgressBar,116.0,10.0);
	clutter_actor_show(priv->m_loadProgressBar);


	//GError* error = NULL;
	//priv->m_progress_end_img = clutter_texture_new_from_file(PKGDATADIR "/mmd-internet/mmd-browser/icon_progressbar_end.png",&error);
	priv->m_progress_end_img = thornbury_ui_texture_create_new(ICONDIR "/icon_progressbar_end.png",5,60,FALSE,FALSE);
	g_object_set(priv->m_progress_end_img,
				"x-expand",FALSE,
				"x-align",CLUTTER_ACTOR_ALIGN_END,
				"y-expand",TRUE,
				"y-align",CLUTTER_ACTOR_ALIGN_FILL,
				NULL);
	clutter_actor_add_child(priv->m_loadProgressBar,priv->m_progress_end_img);

	loadProgress = clutter_actor_new();
	clutter_actor_set_background_color(loadProgress,&barColor);
	g_object_set(loadProgress,
			    "opacity",80,
				"x-expand",TRUE,
				"x-align",CLUTTER_ACTOR_ALIGN_FILL,
				"y-expand",TRUE,
				"y-align",CLUTTER_ACTOR_ALIGN_FILL,
				NULL);
	clutter_actor_add_child(priv->m_loadProgressBar,loadProgress);



	/*priv->m_click_action = clutter_click_action_new();
	g_object_ref(priv->m_click_action);
	g_object_set(priv->m_click_action,"long-press-duration",2000,NULL);
	clutter_actor_add_action(self,priv->m_click_action);
	g_signal_connect(priv->m_click_action,"long-press",_rhayader_webbrowser_speller_clicked_cb,NULL);
	g_signal_connect(priv->m_click_action,"clicked",_rhayader_webbrowser_speller_long_pressed_cb,NULL);*/


}

RhayaderWebbrowserSpeller *
rhayader_webbrowser_speller_new (void)
{
  return g_object_new (RHAYADER_TYPE_WEBBROWSER_SPELLER,
      NULL);
}

void rhayader_webbrowser_speller_show(RhayaderWebbrowserSpeller* self)
{
	if(self->priv->m_isShown)
	return;

	self->priv->m_isShown = TRUE;
	clutter_actor_hide(self->priv->m_loadProgressBar);
	mildenhall_speller_show(CLUTTER_ACTOR(self->priv->m_speller),TRUE);
}

void
rhayader_webbrowser_speller_hide(RhayaderWebbrowserSpeller* self,gboolean is_animation)
{
	//self->priv->m_isShown = FALSE;
	mildenhall_speller_hide(CLUTTER_ACTOR(self->priv->m_speller),is_animation);
}

/*void rhayader_webbrowser_speller_set_visible(RhayaderWebbrowserSpeller* self,gboolean raise_it)
{
	RhayaderWebbrowserSpellerPrivate* priv =  WEBBROWSER_SPELLER_PRIVATE (self);
	if(!raise_it && self->priv->m_isShown)
	{
		mildenhall_speller_hide(CLUTTER_ACTOR(self->priv->m_speller),FALSE);
	}
	 clutter_actor_save_easing_state (CLUTTER_ACTOR(self));
	 clutter_actor_set_easing_mode (CLUTTER_ACTOR(self), CLUTTER_LINEAR);
	 clutter_actor_set_easing_duration (CLUTTER_ACTOR(self), 700);
	 clutter_actor_set_opacity(CLUTTER_ACTOR(self),(raise_it == TRUE)?0xff:0);
	 clutter_actor_restore_easing_state (CLUTTER_ACTOR(self));

}*/

void rhayader_webbrowser_speller_set_progress (RhayaderWebbrowserSpeller* self,gdouble percent)
{
	RhayaderWebbrowserSpellerPrivate* priv =  WEBBROWSER_SPELLER_PRIVATE (self);
	//g_print("load %f",percent);
	if((priv->m_isShown) || (percent == 100.0))
	{
		clutter_actor_set_width(priv->m_loadProgressBar,1.0);
		clutter_actor_hide(priv->m_loadProgressBar);
	}
	else
	{
		gfloat width;

		clutter_actor_show(priv->m_loadProgressBar);
		width = (clutter_actor_get_width (CLUTTER_ACTOR (self)) - (clutter_actor_get_x (priv->m_loadProgressBar) + 12)) * (percent / 100);
		//g_print("load width %f",width);
		clutter_actor_set_width(priv->m_loadProgressBar,width);
	}


}

void
rhayader_webbrowser_speller_set_title (RhayaderWebbrowserSpeller *self,
    const gchar *title)
{
	self->priv->m_title = g_strdup(title);
	_rhayader_webbrowser_speller_refresh(self);
}

void
rhayader_webbrowser_speller_set_url (RhayaderWebbrowserSpeller *self,
    const gchar *url)
{
	if(!url || !g_strcmp0(url,""))
		return;

	self->priv->m_url = g_strdup(url);
	_rhayader_webbrowser_speller_refresh(self);
}

gchar* rhayader_webbrowser_speller_autocomplete_url(const gchar* url,const gboolean is_url)
{
	GString* final_url = g_string_new (url);
	gchar *return_url;

	if(!is_url)
	{
		g_string_printf(final_url,
				"http://www.google.com/search?q=%s&ie=utf-8&oe=utf-8",
				&url[0]);
	}
	else
	{
		gboolean append_suffix=TRUE;
		SoupURI* uri = soup_uri_new(url);
		if(!uri)
		{
			g_string_prepend(final_url,"http://");
		}
		else
		{
			//g_print("\n scheme=%s,host=%s,path=%s",uri->scheme,uri->host,uri->path);
			append_suffix = !g_str_equal(uri->scheme,"javascript") && !g_str_equal(uri->scheme,"about");
		}

		if( append_suffix &&
		    (g_strrstr(url,".") == NULL) &&
			(g_strrstr(url,"/") == NULL)	)
		{
			g_string_append(final_url,".com");
		}

/*		if(g_strrstr(url,"://") == NULL){
			g_string_prepend(final_url,"http://");
		}
		*/

	}
	return_url = final_url->str;
	g_string_free(final_url,FALSE); //this doesn't free the char*

    return return_url;
}






static gboolean
_rhayader_webbrowser_speller_shown_cb(MildenhallSpeller* speller,RhayaderWebbrowserSpeller* self)
{
	RhayaderWebbrowserSpellerPrivate* priv = WEBBROWSER_SPELLER_PRIVATE (self);
	priv->m_isShown = TRUE;

	g_signal_emit(G_OBJECT(self),signals[WEBBROWSER_SPELLER_VISIBLE],0,TRUE);
	_rhayader_webbrowser_speller_refresh(self);
	return FALSE;
}

static gboolean
_rhayader_webbrowser_speller_hidden_cb(MildenhallSpeller* speller,RhayaderWebbrowserSpeller* self)
{
	RhayaderWebbrowserSpellerPrivate* priv = WEBBROWSER_SPELLER_PRIVATE (self);
	priv->m_isShown = FALSE;
/*	clutter_actor_hide(CLUTTER_ACTOR(self));
	g_object_set(speller,
			"x-expand",TRUE,
			"y-expand",TRUE,
			"x",0.0,
			"y",0.0,
			"x-align",CLUTTER_ACTOR_ALIGN_START,
			"y-align",CLUTTER_ACTOR_ALIGN_START,
			NULL);*/
	//clutter_actor_set_child_above_sibling(clutter_actor_get_parent(self),self,NULL);
	clutter_actor_show(priv->m_loadProgressBar);
	clutter_actor_set_y(CLUTTER_ACTOR(self->priv->m_speller),0.0);
	g_signal_emit(self,signals[WEBBROWSER_SPELLER_VISIBLE],0,FALSE);
	//priv->m_is_speller_state_changed = TRUE;
	_rhayader_webbrowser_speller_refresh(self);
	//clutter_actor_add_action(self,priv->m_click_action);
	return FALSE;
}

static void _rhayader_webbrowser_speller_refresh (RhayaderWebbrowserSpeller* self)
{
	RhayaderWebbrowserSpellerPrivate* priv =  WEBBROWSER_SPELLER_PRIVATE (self);
	GValue value = { 0 };

	g_object_set(priv->m_speller,"reactive",priv->m_isShown,NULL);
        mildenhall_speller_clear_text (priv->m_speller);
	g_value_init(&value, G_TYPE_STRING);
	g_print("\n ishown = %d,isSearch = %d \n searchText=%s, urlText=%s",priv->m_isShown,priv->m_isSearch,priv->m_search,priv->m_url);
	if(priv->m_isShown)
		g_value_set_string(&value, (priv->m_isSearch == TRUE)?g_strdup(priv->m_search):g_strdup(priv->m_url));
	else
		g_value_set_string(&value, g_strdup(priv->m_title));
	thornbury_model_insert_value(priv->m_spellerModel, 0, COLUMN_TEXT_BOX_DEFAULT_TEXT, &value);

	g_object_set(priv->m_speller,"text-selection",TRUE,NULL);

	g_value_unset(&value);

}
static gboolean
_rhayader_webbrowser_speller_go_pressed_cb(MildenhallSpeller* speller,GVariant *pVariant,RhayaderWebbrowserSpeller* self)
{
	GVariantIter iter;
	gchar *pText = NULL;
	gchar *key = NULL;

	RhayaderWebbrowserSpellerPrivate* priv =  WEBBROWSER_SPELLER_PRIVATE (self);
	if(!pVariant)
		return FALSE;

	//gchar* speller_text=g_strdup("");
   g_variant_iter_init (&iter, pVariant);
	while (g_variant_iter_next (&iter, "{ss}", &key, &pText))
	{
		g_print("\n key=%s",key);
		if(!g_strcmp0(key,"urlorsearch"))
		{
			priv->m_isSearch=!g_strcmp0(g_strstrip(pText),"search");
			//break;
		}
	}
	g_variant_iter_init (&iter, pVariant);
		while (g_variant_iter_next (&iter, "{ss}", &key, &pText))
		{
			if(!g_strcmp0(key,"entry"))
			{
				if(priv->m_isSearch)
						priv->m_search=g_strdup(g_strstrip(pText));

				priv->m_url=g_strdup(rhayader_webbrowser_speller_autocomplete_url(g_strdup(g_strstrip(pText)),!priv->m_isSearch));
				priv->m_title=g_strdup("Waiting....");

				break;
			}
		}
	g_print("\n go pressed-%s",self->priv->m_url);

	rhayader_webbrowser_speller_hide(self,TRUE);
	g_signal_emit(self,signals[WEBBROWSER_SPELLER_DATA_ENTERED],0,self->priv->m_url);
	return FALSE;
}


static void
_rhayader_webbrowser_speller_entry_status_cb(GObject *pButton, gchar *pButtonName ,gchar *pButtonId,RhayaderWebbrowserSpeller *self)
{
	g_print("\n _rhayader_webbrowser_speller_entry_status_cb %s",pButtonName);
   if( !g_strcmp0(pButtonName,"urlorsearch"))
   {
		self->priv->m_isSearch = !g_strcmp0(pButtonId,"search");
	   _rhayader_webbrowser_speller_refresh(self);
   }
   else if(!g_strcmp0(pButtonId,"entry"))
   {
	   g_signal_emit(self,signals[WEBBROWSER_SPELLER_PAUSE],0);
	   rhayader_webbrowser_speller_show(self);
   }
   else
   {
	   g_signal_emit(self,signals[WEBBROWSER_SPELLER_SCROLLTOTOP],0);
   }
}



/*static void
_rhayader_webbrowser_speller_clicked_cb(ClutterClickAction* action,RhayaderWebbrowserSpeller* self)
{
	RhayaderWebbrowserSpellerPrivate* priv = WEBBROWSER_SPELLER_PRIVATE (self);

	if(priv->m_isShown)
		return;

	gfloat stage_x,stage_y;
	clutter_click_action_get_coords (action,&stage_x,&stage_y);
	g_print("\n _rhayader_webbrowser_speller_clicked_cb %f,%f",stage_x,stage_y);
	if((stage_x > 150) && (stage_x < 600))
	{
		//rhayader_webbrowser_speller_show(self);
		g_print("\n _rhayader_webbrowser_speller_pause");
		g_signal_emit(self,signals[WEBBROWSER_SPELLER_PAUSE],0);
	}
	else if (stage_x > 600)
	{
		g_print("\n _rhayader_webbrowser_speller_scroll_totop");
		g_signal_emit(self,signals[WEBBROWSER_SPELLER_SCROLLTOTOP],0);
	}

}


static gboolean
_rhayader_webbrowser_speller_long_pressed_cb(ClutterClickAction* action,RhayaderWebbrowserSpeller* self,ClutterLongPressState state)
{
	RhayaderWebbrowserSpellerPrivate* priv = WEBBROWSER_SPELLER_PRIVATE (self);

	if((priv->m_isShown) || (state == CLUTTER_LONG_PRESS_CANCEL))
		return FALSE;

	if(state == CLUTTER_LONG_PRESS_QUERY)
		return TRUE;
	g_print("\n _rhayader_webbrowser_speller_long_pressed_cb");
	gfloat stage_x,stage_y;
	clutter_click_action_get_coords (action,&stage_x,&stage_y);
	if((stage_x > 150) && (stage_x < 600))
	{
		//rhayader_webbrowser_speller_show(self);
		g_print("\n _rhayader_webbrowser_speller_long_pressed_cb");
		g_signal_emit(self,signals[WEBBROWSER_SPELLER_PAUSE],0);
	}
	return TRUE;
}*/

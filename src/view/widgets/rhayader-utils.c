/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-utils.h"

#include "rhayader-scroll-item.h"

/*
ClutterActor *create_and_position_image(const gchar *imgPath, gfloat X, gfloat Y, ClutterActor *parent)
{
	GError *error = NULL;
	ClutterActor *image = clutter_texture_new_from_file(imgPath, &error);
	if(error != NULL)
		return NULL;
	clutter_actor_add_child(parent, image);
	clutter_actor_set_position(image, X, Y);
	return image;
}*/

ClutterActor *create_and_position_text(const gchar* fontName, ClutterColor fontColor, gfloat X, gfloat Y, ClutterActor *parent)
{
	/*Create the text actor, set it's font and position it within it's parent container*/
	ClutterActor *text = clutter_text_new_full(fontName,"",&fontColor);
	//clutter_text_set_font_name(CLUTTER_TEXT(text), fontName);
	//clutter_text_set_color(CLUTTER_TEXT(text), &fontColor);
	clutter_actor_add_child(CLUTTER_ACTOR(parent), text);
	clutter_actor_set_position(text, X, Y);
	return text;
}

ClutterActor *create_and_position_line(ClutterColor lineColor, gfloat X, gfloat Y, gfloat width, gfloat height, ClutterActor *parent)
{
		//ClutterActor *line = clutter_rectangle_new_with_color(&lineColor);
		ClutterActor *line = clutter_actor_new();
	    clutter_actor_set_background_color(line,&lineColor);
		clutter_actor_set_size(line, width, height);
		//g_assert(CLUTTER_IS_ACTOR(parent));
		clutter_actor_add_child(CLUTTER_ACTOR(parent), line);
		clutter_actor_set_position(line, X , Y);
		return line;
}


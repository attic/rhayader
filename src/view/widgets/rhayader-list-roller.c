/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-list-roller.h"
#include "libthornbury-itemfactory.h"
#include "mildenhall/mildenhall_roller_container.h"

enum
{
  ITEM_FOCUSSED,
  ITEM_ACTIVATED,
  LAST_SIGNAL
};

G_DEFINE_TYPE (RhayaderWebBrowserListRoller, rhayader_webbrowser_list_roller, CLUTTER_TYPE_ACTOR)

#define WEBBROWSER_LIST_ROLLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_LIST_ROLLER, RhayaderWebBrowserListRollerPrivate))

struct _RhayaderWebBrowserListRollerPrivate
{
	MildenhallRollerContainer* m_roller_container;

	gboolean m_in_motion;
	gulong m_idle_resize_id;

	ThornburyModel* m_model;
	GType m_item_type;
};
static guint signals[LAST_SIGNAL] = { 0, };
//private methds
static void
_rhayader_webbrowser_list_roller_change_focus_cb(ThornburyModel *model,guint focussed_row,RhayaderWebBrowserListRoller *self);
static void _rhayader_webbrowser_list_roller_item_activated_cb (MildenhallRollerContainer* roller,
                                                                guint row,
                                                                RhayaderWebBrowserListRoller *self);

static void
rhayader_webbrowser_list_roller_get_property (GObject    *object,
                                         guint       property_id,
                                         GValue     *value,
                                         GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
rhayader_webbrowser_list_roller_set_property (GObject      *object,
                                         guint         property_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
rhayader_webbrowser_list_roller_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_list_roller_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_list_roller_class_init (RhayaderWebBrowserListRollerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserListRollerPrivate));

  object_class->get_property = rhayader_webbrowser_list_roller_get_property;
  object_class->set_property = rhayader_webbrowser_list_roller_set_property;
  object_class->dispose = rhayader_webbrowser_list_roller_dispose;

  signals[ITEM_FOCUSSED] =
    g_signal_new ("item_focussed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__UINT_POINTER,
                  G_TYPE_NONE, 2,G_TYPE_UINT,G_TYPE_POINTER);

  signals[ITEM_ACTIVATED] =
    g_signal_new ("item_activated",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__POINTER,
                  G_TYPE_NONE, 1,G_TYPE_POINTER);
}

static gboolean
rhayader_webbrowser_list_roller_update_size_cb(RhayaderWebBrowserListRoller *self)
{
	RhayaderWebBrowserListRollerPrivate* priv = WEBBROWSER_LIST_ROLLER_PRIVATE (self);
	gfloat width, height;
	guint numCols;
	guint i;

	priv->m_idle_resize_id = 0;
	clutter_actor_get_size(CLUTTER_ACTOR(self),&width,&height);
	//g_print("\n rhayader_webbrowser_list_roller_size_changed_cb %f %f",width,height);
	if(priv->m_roller_container)
	  {
	    mildenhall_roller_container_set_width (
	      MILDENHALL_ROLLER_CONTAINER (priv->m_roller_container), width);
	    mildenhall_roller_container_set_height (
	      MILDENHALL_ROLLER_CONTAINER(priv->m_roller_container), height);
	    return G_SOURCE_REMOVE;
	  }

	priv->m_roller_container = g_object_new (MILDENHALL_TYPE_ROLLER_CONTAINER,
										"roller-type", MILDENHALL_FIXED_ROLLER_CONTAINER,
										"width",clutter_actor_get_width(CLUTTER_ACTOR(self)),
										"height",clutter_actor_get_height(CLUTTER_ACTOR(self)),
										"item-type", priv->m_item_type,
										"model", priv->m_model,
										"roll-over", TRUE,
										"name", "listroller",
										"reactive", TRUE,
										"clamp-duration", 1250,
										"clamp-mode", CLUTTER_EASE_OUT_ELASTIC,
										NULL);

	  	clutter_actor_set_reactive(CLUTTER_ACTOR(self),TRUE);

	  	g_assert(G_IS_OBJECT(priv->m_model));
	numCols = thornbury_model_get_n_columns(priv->m_model);
	for(i=0;i < numCols;i++)
          mildenhall_roller_container_add_attribute (
            MILDENHALL_ROLLER_CONTAINER (priv->m_roller_container),
            thornbury_model_get_column_name (priv->m_model, i), i);

	clutter_actor_add_child (CLUTTER_ACTOR(self), CLUTTER_ACTOR(priv->m_roller_container));

/*	clutter_actor_set_layout_manager(priv->m_roller_container,
				clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL)
				);
	g_object_set(priv->m_roller_container,
			"x-expand",TRUE,
			"y-expand",TRUE,
			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
			"y-align",CLUTTER_ACTOR_ALIGN_FILL,
			NULL);*/

//
//	g_assert(CLUTTER_IS_ACTOR(priv->m_listRoller));
	  //g_signal_connect(priv->rhayader_listRoller,"captured-event",_roller_captured_event_cb,NULL);
		 g_signal_connect(priv->m_roller_container,"roller-item-activated",
							 G_CALLBACK(_rhayader_webbrowser_list_roller_item_activated_cb),self);
	 g_signal_connect_after(priv->m_model,"item_focus",G_CALLBACK(_rhayader_webbrowser_list_roller_change_focus_cb),self);

    return G_SOURCE_REMOVE;
}

static void
_rhayader_webbrowser_list_roller_on_allocate_cb(ClutterActor* actor,const ClutterActorBox *allocation,
													ClutterAllocationFlags  flags,RhayaderWebBrowserListRoller* self)
{
	  if (self->priv->m_idle_resize_id == 0)
		  self->priv->m_idle_resize_id = clutter_threads_add_timeout (100, (GSourceFunc)rhayader_webbrowser_list_roller_update_size_cb, self);
}

static void
rhayader_webbrowser_list_roller_init (RhayaderWebBrowserListRoller *self)
{
  self->priv = WEBBROWSER_LIST_ROLLER_PRIVATE (self);
  self->priv->m_in_motion = FALSE;
  self->priv->m_idle_resize_id = 0;
  self->priv->m_roller_container = NULL;
  //g_signal_connect(self,);
/*	g_object_set (self,
				   "acceleration-factor", 2.0,
				   "deceleration", 1.03,
				   "clamp-duration", 1250,
				   "clamp-mode", get_elastic_mode (),
				   "clamp-to-center", TRUE,
				   "overshoot", 1.0,
				   NULL);*/
	clutter_actor_set_layout_manager(CLUTTER_ACTOR(self),
				clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL)
				);
	g_signal_connect (CLUTTER_ACTOR(self), "allocation-changed", G_CALLBACK (_rhayader_webbrowser_list_roller_on_allocate_cb), self);

}



RhayaderWebBrowserListRoller *
rhayader_webbrowser_list_roller_new (gfloat width,gfloat height,GType item_type,ThornburyModel *model)
{
	RhayaderWebBrowserListRoller *self = g_object_new(RHAYADER_TYPE_WEBBROWSER_LIST_ROLLER,"width",width,"height",height,NULL);
	self->priv->m_item_type=item_type;
	self->priv->m_model=model;

    return self;
}

gint rhayader_webbrowser_list_roller_get_focused_row(RhayaderWebBrowserListRoller * self)
{
  gint focused_row = -1;

  if (thornbury_model_get_n_rows (self->priv->m_model) > 0)
    focused_row = mildenhall_roller_container_get_focused_row (self->priv->m_roller_container);

  return focused_row;
}

static gboolean
_rhayader_webbrowser_list_roller_item_activate(RhayaderWebBrowserListRoller* self)
{
	 _rhayader_webbrowser_list_roller_item_activated_cb(self->priv->m_roller_container,rhayader_webbrowser_list_roller_get_focused_row(self),self);
	 return G_SOURCE_REMOVE;
}

void rhayader_webbrowser_list_roller_set_focused_row(RhayaderWebBrowserListRoller * self,gint row,gboolean is_animate,gboolean is_activate)
{
	ClutterActor *list_item;

	if((row < 0) ||
	    row >= (thornbury_model_get_n_rows(self->priv->m_model))
		//(rhayader_webbrowser_list_roller_get_focused_row(self) == row)
	  )
		return;
	list_item = mildenhall_roller_container_get_actor_for_row (self->priv->m_roller_container, row);
	/* FIXME: Can't we use property instead of _set_data? */
	g_object_set_data (G_OBJECT (list_item), "new", GINT_TO_POINTER (1));
	mildenhall_roller_container_set_focused_row (self->priv->m_roller_container, row, TRUE);

	if (is_activate
	  && (rhayader_webbrowser_list_roller_get_focused_row (self) == row))
	  {
	    g_timeout_add (20, (GSourceFunc) _rhayader_webbrowser_list_roller_item_activate, self);
	  }
}

ClutterActor* rhayader_webbrowser_list_roller_get_item(RhayaderWebBrowserListRoller * self,gint rowid)
{
  g_assert(rowid >= 0);

  return mildenhall_roller_container_get_actor_for_row (self->priv->m_roller_container, rowid);
}

gboolean rhayader_webbrowser_list_roller_is_moving(RhayaderWebBrowserListRoller * self)
{
	return self->priv->m_in_motion;
}

static void
_rhayader_webbrowser_list_roller_item_activated_cb (MildenhallRollerContainer* roller,
                                                    guint row,
                                                    RhayaderWebBrowserListRoller *self)
{
  gint focused_row = mildenhall_roller_container_get_focused_row (roller);

  if(row == focused_row)
    {
      ClutterActor* focused_item = mildenhall_roller_container_get_actor_for_row (roller, row);
      g_object_set_data (G_OBJECT (focused_item), "row", row);
      g_signal_emit (G_OBJECT(self), signals[ITEM_ACTIVATED], 0, focused_item);
    }
}


static void
_rhayader_webbrowser_list_roller_change_focus_cb (ThornburyModel *model,
                                                  guint focused_row,
                                                  RhayaderWebBrowserListRoller *self)
{
  RhayaderWebBrowserListRollerPrivate* priv = WEBBROWSER_LIST_ROLLER_PRIVATE (self);

  ClutterActor* list_item = NULL;

  g_return_if_fail (focused_row < thornbury_model_get_n_rows (model));

  list_item = mildenhall_roller_container_get_actor_for_row (priv->m_roller_container, focused_row);
  
  if (list_item == NULL)
    return;

  clutter_actor_set_opacity (list_item, 0x00);
  clutter_actor_show (list_item);
  clutter_actor_save_easing_state (list_item);
  clutter_actor_set_easing_duration (list_item, 1000);
  clutter_actor_set_easing_mode (list_item, CLUTTER_LINEAR);
  clutter_actor_set_opacity (list_item, 0xFF);
  clutter_actor_restore_easing_state (list_item);
  g_object_set_data (G_OBJECT (list_item), "new", GINT_TO_POINTER (1));
  mildenhall_roller_container_set_focused_row (priv->m_roller_container, focused_row, TRUE);

  g_object_unref (list_item);
}

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_ZOOM_ITEM_H
#define _RHAYADER_WEBBROWSER_ZOOM_ITEM_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_ZOOM_ITEM rhayader_webbrowser_zoom_item_get_type()

#define RHAYADER_WEBBROWSER_ZOOM_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_ZOOM_ITEM, RhayaderWebBrowserZoomItem))

#define RHAYADER_WEBBROWSER_ZOOM_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_ZOOM_ITEM, RhayaderWebBrowserZoomItemClass))

#define RHAYADER_IS_WEBBROWSER_ZOOM_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_ZOOM_ITEM))

#define RHAYADER_IS_WEBBROWSER_ZOOM_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_ZOOM_ITEM))

#define RHAYADER_WEBBROWSER_ZOOM_ITEM_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_ZOOM_ITEM, RhayaderWebBrowserZoomItemClass))

typedef struct _RhayaderWebBrowserZoomItem RhayaderWebBrowserZoomItem;
typedef struct _RhayaderWebBrowserZoomItemClass RhayaderWebBrowserZoomItemClass;
typedef struct _RhayaderWebBrowserZoomItemPrivate RhayaderWebBrowserZoomItemPrivate;

struct _RhayaderWebBrowserZoomItem
{
  ClutterActor parent;

  RhayaderWebBrowserZoomItemPrivate *priv;
};

struct _RhayaderWebBrowserZoomItemClass
{
	ClutterActorClass parent_class;
};

GType rhayader_webbrowser_zoom_item_get_type (void) G_GNUC_CONST;
void rhayader_webbrowser_zoom_item_configure_width(gfloat item_width);
guint rhayader_webbrowser_zoom_item_get_displayed_index(gdouble zoom_val);
guint* rhayader_webbrowser_zoom_item_get_values(guint* num);

RhayaderWebBrowserZoomItem *rhayader_webbrowser_zoom_item_new (void);
void rhayader_webbrowser_zoom_item_set_focus(RhayaderWebBrowserZoomItem* self,gboolean is_focus);

G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_ZOOM_ITEM_H */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include "rhayader-scroll-item.h"
#include "rhayader-utils.h"

//dimensions
static gfloat RHAYADER_BROWSERSCROLLBAR_WIDTH =65.0;

#define RHAYADER_BROWSERSCROLLBAR_RECTANGLE_GROUP_HEIGHT         63.0
#define RHAYADER_BROWSERSCROLLBAR_RECTANGLE_WIDTH                (RHAYADER_BROWSERSCROLLBAR_WIDTH -5.0)
#define RHAYADER_BROWSERSCROLLBAR_RECTANGLE_HEIGHT                1.0

#define RHAYADER_BROWSERSCROLLBAR_LINE_X                          2.0
#define RHAYADER_BROWSERSCROLLBAR_LINE_Y                         63.0
#define RHAYADER_BROWSERSCROLLBAR_LABEL_X                        RHAYADER_BROWSERSCROLLBAR_RECTANGLE_WIDTH/2-10.0
#define RHAYADER_BROWSERSCROLLBAR_LABEL_Y                        41.0



#define RHAYADER_BROWSERSCROLLBAR_SIZINGBAR_FONT_HEIGHT 21

#define RHAYADER_BROWSERSCROLLBAR_FONT         28 //24

#define RHAYADER_WEBBROWSER_FONT(size) "DejaVuSansCondensed " #size"px"

G_DEFINE_TYPE (RhayaderWebBrowserScrollItem, rhayader_webbrowser_scroll_item, CLUTTER_TYPE_ACTOR)

enum
{
  PROP_0,
  PROP_FOCUSED,
  PROP_VALUE,
};

enum
{
  COLUMN_VALUE,
  COLUMN_LAST
};

#define WEBBROWSER_SCROLL_ITEM_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_SCROLL_ITEM, RhayaderWebBrowserScrollItemPrivate))

struct _RhayaderWebBrowserScrollItemPrivate
{
	ClutterText* m_scrollText;
	guint m_scrollval;
	gboolean m_is_highlighted;
};

static void
rhayader_webbrowser_scroll_item_get_property (GObject    *object,
                                       guint       property_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
	RhayaderWebBrowserScrollItem *self = RHAYADER_WEBBROWSER_SCROLL_ITEM (object);
	RhayaderWebBrowserScrollItemPrivate* priv =  WEBBROWSER_SCROLL_ITEM_PRIVATE (self);

  switch (property_id)
    {
  	  case PROP_VALUE:
  		g_value_set_uint(value,priv->m_scrollval);
  		break;

     default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}


static void
rhayader_webbrowser_scroll_item_set_property (GObject      *object,
                                       guint         property_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
	RhayaderWebBrowserScrollItem *self = RHAYADER_WEBBROWSER_SCROLL_ITEM (object);
	RhayaderWebBrowserScrollItemPrivate* priv =  WEBBROWSER_SCROLL_ITEM_PRIVATE (self);

  switch (property_id)
    {
  	  case PROP_VALUE:
  	  {
  		gchar *textValue = NULL;

  		 priv->m_scrollval = g_value_get_uint (value);
  		textValue=g_strdup_printf("%d ",priv->m_scrollval);
  		 clutter_text_set_text(priv->m_scrollText,textValue);
  	  }
  	  break;

  	  case PROP_FOCUSED:
  	  {
  		rhayader_webbrowser_scroll_item_set_focus(self,g_value_get_boolean (value));
  	  }
       break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
rhayader_webbrowser_scroll_item_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_scroll_item_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_scroll_item_class_init (RhayaderWebBrowserScrollItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GParamSpec *pspec;

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserScrollItemPrivate));

  object_class->get_property = rhayader_webbrowser_scroll_item_get_property;
  object_class->set_property = rhayader_webbrowser_scroll_item_set_property;
  object_class->dispose = rhayader_webbrowser_scroll_item_dispose;

  pspec = g_param_spec_uint ("value",
                               "value",
                               "scroll value",
                               0,
                               200,
                               0,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_VALUE, pspec);

  pspec = g_param_spec_boolean ("focused",
                               "focused",
                               "is focused",
                               FALSE,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_FOCUSED, pspec);
}

static void
rhayader_webbrowser_scroll_item_init (RhayaderWebBrowserScrollItem *self)
{
  static ClutterColor labelcolor = {0xE4,0xE8,0xE9,0xFF};
  static ClutterColor rectcolor = {0x00,0x00,0x00,0x66};
  static ClutterColor separatorColor = {0xFF,0xFF,0xFF,0x33};
  RhayaderWebBrowserScrollItemPrivate *priv = self->priv = WEBBROWSER_SCROLL_ITEM_PRIVATE (self);
  ClutterActor *xpercentLine;
  gchar *textValue = NULL;

   priv->m_scrollval = 0;

   	clutter_actor_set_layout_manager(CLUTTER_ACTOR(self),clutter_fixed_layout_new());
   	clutter_actor_set_height(CLUTTER_ACTOR(self), RHAYADER_BROWSERSCROLLBAR_RECTANGLE_GROUP_HEIGHT);
   	clutter_actor_set_background_color(CLUTTER_ACTOR(self),&rectcolor);
   	clutter_actor_set_reactive(CLUTTER_ACTOR(self),TRUE);

	xpercentLine = clutter_actor_new ();
	clutter_actor_set_background_color(xpercentLine,&separatorColor);
	clutter_actor_set_size(xpercentLine, RHAYADER_BROWSERSCROLLBAR_RECTANGLE_WIDTH ,RHAYADER_BROWSERSCROLLBAR_RECTANGLE_HEIGHT);
	g_assert(CLUTTER_IS_ACTOR(self));
	clutter_actor_add_child(CLUTTER_ACTOR(self), xpercentLine);
	clutter_actor_set_position(xpercentLine, RHAYADER_BROWSERSCROLLBAR_LINE_X, RHAYADER_BROWSERSCROLLBAR_LINE_Y);

	 textValue= g_strdup_printf("%d",0);
   	priv->m_scrollText = (ClutterText*)clutter_text_new_full(RHAYADER_WEBBROWSER_FONT(28),textValue,&labelcolor);
   	clutter_actor_add_child(CLUTTER_ACTOR(self), CLUTTER_ACTOR(priv->m_scrollText) );
   	clutter_actor_set_position(CLUTTER_ACTOR(priv->m_scrollText), RHAYADER_BROWSERSCROLLBAR_LABEL_X, RHAYADER_BROWSERSCROLLBAR_LABEL_Y-20.0);
    if(textValue)
    {
    	g_free(textValue);
    	textValue=NULL;
    }
}

void rhayader_webbrowser_scroll_item_configure_width(gfloat item_width)
{
	RHAYADER_BROWSERSCROLLBAR_WIDTH = item_width;
}

RhayaderWebBrowserScrollItem *
rhayader_webbrowser_scroll_item_new (void)
{
  return g_object_new (RHAYADER_TYPE_WEBBROWSER_SCROLL_ITEM, NULL);
}

void rhayader_webbrowser_scroll_item_set_focus(RhayaderWebBrowserScrollItem* self,gboolean is_focus)
{
	RhayaderWebBrowserScrollItemPrivate *priv = self->priv = WEBBROWSER_SCROLL_ITEM_PRIVATE (self);
	ClutterColor details_color = {0x98, 0xA9, 0xB2, 0xFF};

	priv->m_is_highlighted = is_focus;
	if(priv->m_is_highlighted){
		details_color.red = 0xFF;details_color.blue = 0xFF;details_color.green = 0xFF;
		details_color.alpha = 0xFF;
	}
	clutter_text_set_color(CLUTTER_TEXT(priv->m_scrollText),&details_color);

}





/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_SCROLL_ITEM_H
#define _RHAYADER_WEBBROWSER_SCROLL_ITEM_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_SCROLL_ITEM rhayader_webbrowser_scroll_item_get_type()

#define RHAYADER_WEBBROWSER_SCROLL_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_SCROLL_ITEM, RhayaderWebBrowserScrollItem))

#define RHAYADER_WEBBROWSER_SCROLL_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_SCROLL_ITEM, RhayaderWebBrowserScrollItemClass))

#define RHAYADER_IS_WEBBROWSER_SCROLL_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_SCROLL_ITEM))

#define RHAYADER_IS_WEBBROWSER_SCROLL_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_SCROLL_ITEM))

#define RHAYADER_WEBBROWSER_SCROLL_ITEM_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_SCROLL_ITEM, RhayaderWebBrowserScrollItemClass))

typedef struct _RhayaderWebBrowserScrollItem RhayaderWebBrowserScrollItem;
typedef struct _RhayaderWebBrowserScrollItemClass RhayaderWebBrowserScrollItemClass;
typedef struct _RhayaderWebBrowserScrollItemPrivate RhayaderWebBrowserScrollItemPrivate;

struct _RhayaderWebBrowserScrollItem
{
  ClutterActor parent;

  RhayaderWebBrowserScrollItemPrivate *priv;
};

struct _RhayaderWebBrowserScrollItemClass
{
	ClutterActorClass parent_class;
};

GType rhayader_webbrowser_scroll_item_get_type (void) G_GNUC_CONST;
void rhayader_webbrowser_scroll_item_configure_width(gfloat item_width);
RhayaderWebBrowserScrollItem *rhayader_webbrowser_scroll_item_new (void);
void rhayader_webbrowser_scroll_item_set_focus(RhayaderWebBrowserScrollItem* self,gboolean is_focus);

G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_SCROLL_ITEM_H */

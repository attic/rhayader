/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_SCROLL_ITEM_H
#define _RHAYADER_WEBBROWSER_SCROLL_ITEM_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

ClutterActor *create_and_position_text(const gchar* fontName,
									ClutterColor fontColor,
									gfloat X, gfloat Y,
									ClutterActor *parent);
ClutterActor *create_and_position_line(ClutterColor lineColor,
				gfloat X, gfloat Y,
				gfloat width, gfloat height,
				ClutterActor *parent);

G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_SCROLL_ITEM_H */

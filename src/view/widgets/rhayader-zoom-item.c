/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include "rhayader-zoom-item.h"
#include "rhayader-utils.h"

//dimensions
static gfloat RHAYADER_BROWSERZOOMBAR_WIDTH =65.0;
#define RHAYADER_BROWSERZOOMBAR_RECTANGLE_GROUP_HEIGHT         63.0
#define RHAYADER_BROWSERZOOMBAR_RECTANGLE_WIDTH                (RHAYADER_BROWSERZOOMBAR_WIDTH - 5)
#define RHAYADER_BROWSERZOOMBAR_RECTANGLE_HEIGHT                1.0
#define RHAYADER_BROWSERZOOMBAR_LINE_X                          2.0
#define RHAYADER_BROWSERZOOMBAR_LINE_Y                         63.0
#define RHAYADER_BROWSERZOOMBAR_LABEL_X                        5.0
#define RHAYADER_BROWSERZOOMBAR_LABEL_Y                        41.0



#define RHAYADER_BROWSERZOOMBAR_SIZINGBAR_FONT_HEIGHT 21


#define RHAYADER_WEBBROWSER_FONT(size) "DejaVuSansCondensed " #size"px"

G_DEFINE_TYPE (RhayaderWebBrowserZoomItem, rhayader_webbrowser_zoom_item, CLUTTER_TYPE_ACTOR)

enum
{
  PROP_0,
  PROP_FOCUSED,
  PROP_VALUE,
};

enum
{
  COLUMN_VALUE,
  COLUMN_LAST
};

#define WEBBROWSER_ZOOM_ITEM_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_ZOOM_ITEM, RhayaderWebBrowserZoomItemPrivate))

struct _RhayaderWebBrowserZoomItemPrivate
{
	ClutterText* m_zoomText;
	guint m_zoomval;
	gboolean m_is_highlighted;
};
static guint zoom_values[] = {60,80,100,120,150,200};

static void
rhayader_webbrowser_zoom_item_get_property (GObject    *object,
                                       guint       property_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
	RhayaderWebBrowserZoomItem *self = RHAYADER_WEBBROWSER_ZOOM_ITEM (object);
	RhayaderWebBrowserZoomItemPrivate* priv =  WEBBROWSER_ZOOM_ITEM_PRIVATE (self);

  switch (property_id)
    {
  	  case PROP_VALUE:
  		g_value_set_uint(value,priv->m_zoomval);
  		break;

     default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}


static void
rhayader_webbrowser_zoom_item_set_property (GObject      *object,
                                       guint         property_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
	RhayaderWebBrowserZoomItem *self = RHAYADER_WEBBROWSER_ZOOM_ITEM (object);
	RhayaderWebBrowserZoomItemPrivate* priv =  WEBBROWSER_ZOOM_ITEM_PRIVATE (self);

  switch (property_id)
    {
  	  case PROP_VALUE:
  	  {
  		gchar *textValue = NULL;

  		 priv->m_zoomval = g_value_get_uint (value);
  		textValue= g_strdup_printf("%d %%",priv->m_zoomval);
  		 clutter_text_set_text(priv->m_zoomText,textValue);
  		 if(textValue)
  		 {
  			 g_free(textValue);
  			textValue=NULL;
  		 }
  	  }
  	  break;

  	  case PROP_FOCUSED:
  	  {
  		rhayader_webbrowser_zoom_item_set_focus(self,g_value_get_boolean (value));
  	  }
       break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
rhayader_webbrowser_zoom_item_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_zoom_item_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_zoom_item_class_init (RhayaderWebBrowserZoomItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GParamSpec *pspec;

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserZoomItemPrivate));

  object_class->get_property = rhayader_webbrowser_zoom_item_get_property;
  object_class->set_property = rhayader_webbrowser_zoom_item_set_property;
  object_class->dispose = rhayader_webbrowser_zoom_item_dispose;

  pspec = g_param_spec_uint ("value",
                               "value",
                               "zoom value",
                               0,
                               200,
                               0,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_VALUE, pspec);

  pspec = g_param_spec_boolean ("focused",
                               "focused",
                               "is focused",
                               FALSE,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_FOCUSED, pspec);
}

static void
rhayader_webbrowser_zoom_item_init (RhayaderWebBrowserZoomItem *self)
{
  static ClutterColor labelcolor = {0xE4,0xE8,0xE9,0xFF};
  static ClutterColor rectcolor = {0x00,0x00,0x00,0x66};
  RhayaderWebBrowserZoomItemPrivate *priv = self->priv = WEBBROWSER_ZOOM_ITEM_PRIVATE (self);
  gchar *textValue = NULL;

   priv->m_zoomval = 0;

   	clutter_actor_set_height(CLUTTER_ACTOR(self), RHAYADER_BROWSERZOOMBAR_RECTANGLE_GROUP_HEIGHT);
   	clutter_actor_set_background_color(CLUTTER_ACTOR(self),&rectcolor);
   	clutter_actor_set_reactive(CLUTTER_ACTOR(self),TRUE);

   //	ClutterActor* xpercentLine =create_and_position_line(separatorColor,RHAYADER_BROWSERZOOMBAR_LINE_X, RHAYADER_BROWSERZOOMBAR_LINE_Y, RHAYADER_BROWSERZOOMBAR_RECTANGLE_WIDTH , RHAYADER_BROWSERZOOMBAR_RECTANGLE_HEIGHT,CLUTTER_ACTOR(self));
   	textValue=g_strdup_printf("%d %%",0);
   	priv->m_zoomText = (ClutterText*)create_and_position_text(RHAYADER_WEBBROWSER_FONT(21),labelcolor,
   												RHAYADER_BROWSERZOOMBAR_LABEL_X,
   												RHAYADER_BROWSERZOOMBAR_LABEL_Y-RHAYADER_BROWSERZOOMBAR_SIZINGBAR_FONT_HEIGHT,
   												CLUTTER_ACTOR(self));

}


void rhayader_webbrowser_zoom_item_configure_width(gfloat item_width)
{
	RHAYADER_BROWSERZOOMBAR_WIDTH = item_width;
}


RhayaderWebBrowserZoomItem *
rhayader_webbrowser_zoom_item_new (void)
{
  return g_object_new (RHAYADER_TYPE_WEBBROWSER_ZOOM_ITEM, NULL);
}

guint* rhayader_webbrowser_zoom_item_get_values(guint* num)
{
	*num = 6;
	return zoom_values;
}

void rhayader_webbrowser_zoom_item_set_focus(RhayaderWebBrowserZoomItem* self,gboolean is_focus)
{
	RhayaderWebBrowserZoomItemPrivate *priv = self->priv = WEBBROWSER_ZOOM_ITEM_PRIVATE (self);
	ClutterColor details_color = {0x98, 0xA9, 0xB2, 0xFF};

	priv->m_is_highlighted = is_focus;
	if(priv->m_is_highlighted){
		details_color.red = 0xFF;details_color.blue = 0xFF;details_color.green = 0xFF;
		details_color.alpha = 0xFF;
	}
	clutter_text_set_color(CLUTTER_TEXT(priv->m_zoomText),&details_color);

}

guint rhayader_webbrowser_zoom_item_get_displayed_index(gdouble zoom_val)
{
	guint index=0;
	//for(index=0;(index < 6) && (zoom_val > (gdouble)((gdouble)(zoom_values[index])/100.0));index++);
	//if(index == 6) index--;
	for(index=5;(index > 0) && (zoom_val < (gdouble)((gdouble)(zoom_values[index])/100.0));index--);
	return index;
}



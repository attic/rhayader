/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_LIST_ITEM_H
#define _RHAYADER_WEBBROWSER_LIST_ITEM_H

#include <glib-object.h>
#include <clutter/clutter.h>

#pragma GCC diagnostic push
/* FIXME: stop disabling these warnings when the libraries are fixed.
 * https://bugs.apertis.org/show_bug.cgi?id=569 */
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <liblightwood-glowshader.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_LIST_ITEM rhayader_webbrowser_list_item_get_type()

#define RHAYADER_WEBBROWSER_LIST_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_ITEM, RhayaderWebBrowserListItem))

#define RHAYADER_WEBBROWSER_LIST_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_LIST_ITEM, RhayaderWebBrowserListItemClass))

#define RHAYADER_IS_WEBBROWSER_LIST_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_ITEM))

#define RHAYADER_IS_WEBBROWSER_LIST_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_LIST_ITEM))

#define RHAYADER_WEBBROWSER_LIST_ITEM_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_ITEM, RhayaderWebBrowserListItemClass))

typedef struct _RhayaderWebBrowserListItem RhayaderWebBrowserListItem;
typedef struct _RhayaderWebBrowserListItemClass RhayaderWebBrowserListItemClass;
typedef struct _RhayaderWebBrowserListItemPrivate RhayaderWebBrowserListItemPrivate;

struct _RhayaderWebBrowserListItem
{
  ClutterGroup parent;

  RhayaderWebBrowserListItemPrivate *priv;
};

struct _RhayaderWebBrowserListItemClass
{
  ClutterGroupClass parent_class;
};

GType rhayader_webbrowser_list_item_get_type (void) G_GNUC_CONST;
void rhayader_webbrowser_list_item_configure_width(gfloat item_width);
RhayaderWebBrowserListItem *rhayader_webbrowser_list_item_new (void);

void rhayader_webbrowser_list_item_set_focus(RhayaderWebBrowserListItem* self,gboolean is_focus);
void rhayader_webbrowser_list_item_restore_focussed_tab(RhayaderWebBrowserListItem* self,ClutterActor* tab);


G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_LIST_ITEM_H */

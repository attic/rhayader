/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_NORMAL_VIEW_H
#define _RHAYADER_WEBBROWSER_NORMAL_VIEW_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include "libthornbury-model.h"
#include "widgets/rhayader-speller.h"

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_NORMAL_VIEW rhayader_webbrowser_normal_view_get_type()

#define RHAYADER_WEBBROWSER_NORMAL_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_NORMAL_VIEW, RhayaderWebBrowserNormalView))

#define RHAYADER_WEBBROWSER_NORMAL_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_NORMAL_VIEW, RhayaderWebBrowserNormalViewClass))

#define RHAYADER_IS_WEBBROWSER_NORMAL_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_NORMAL_VIEW))

#define RHAYADER_IS_WEBBROWSER_NORMAL_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_NORMAL_VIEW))

#define RHAYADER_WEBBROWSER_NORMAL_VIEW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_NORMAL_VIEW, RhayaderWebBrowserNormalViewClass))

typedef struct _RhayaderWebBrowserNormalView RhayaderWebBrowserNormalView;
typedef struct _RhayaderWebBrowserNormalViewClass RhayaderWebBrowserNormalViewClass;
typedef struct _RhayaderWebBrowserNormalViewPrivate RhayaderWebBrowserNormalViewPrivate;

struct _RhayaderWebBrowserNormalView
{
  ClutterActor parent;

  RhayaderWebBrowserNormalViewPrivate *priv;
};

struct _RhayaderWebBrowserNormalViewClass
{
  ClutterActorClass parent_class;
};

typedef enum
{
  FORWARD,
  BACK,
  RELOAD,
  SEARCH,
  TOBOOKMARKS,
  SHARE,
  SAVEASPDF
 }eContextMenuOption;
GType rhayader_webbrowser_normal_view_get_type (void) G_GNUC_CONST;

RhayaderWebBrowserNormalView *rhayader_webbrowser_normal_view_new (gfloat toolbar_width);
ClutterActor* rhayader_webbrowser_normal_view_get_tab(RhayaderWebBrowserNormalView* self);

#ifndef WEBRUNTIME
gboolean rhayader_webbrowser_normal_view_is_fullscreen(RhayaderWebBrowserNormalView * self);
void rhayader_webbrowser_normal_view_show_view(RhayaderWebBrowserNormalView* self,ClutterActor* webview_widget);
void rhayader_webbrowser_normal_view_set_bottom_area_visible(RhayaderWebBrowserNormalView* self,gboolean is_raise);
gboolean rhayader_webbrowser_normal_view_is_shown(RhayaderWebBrowserNormalView * self);
RhayaderWebbrowserSpeller* rhayader_webbrowser_normal_view_get_url_speller(RhayaderWebBrowserNormalView* self);
void rhayader_webbrowser_normal_view_refresh(RhayaderWebBrowserNormalView* self);
void rhayader_webbrowser_normal_view_hide_view(RhayaderWebBrowserNormalView * self);
void rhayader_webbrowser_normal_view_go_fullscreen(RhayaderWebBrowserNormalView * self,gboolean isFullscreen);

#endif

G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_NORMAL_VIEW_H */

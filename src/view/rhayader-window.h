/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_WINDOW_H
#define _RHAYADER_WEBBROWSER_WINDOW_H

#include <glib-object.h>
#include <clutter/clutter.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_WINDOW rhayader_webbrowser_window_get_type()

#define RHAYADER_WEBBROWSER_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_WINDOW, RhayaderWebbrowserWindow))

#define RHAYADER_WEBBROWSER_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_WINDOW, RhayaderWebbrowserWindowClass))

#define RHAYADER_IS_WEBBROWSER_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_WINDOW))

#define RHAYADER_IS_WEBBROWSER_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_WINDOW))

#define RHAYADER_WEBBROWSER_WINDOW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_WINDOW, RhayaderWebbrowserWindowClass))

typedef struct _RhayaderWebbrowserWindow RhayaderWebbrowserWindow;
typedef struct _RhayaderWebbrowserWindowClass RhayaderWebbrowserWindowClass;
typedef struct _RhayaderWebbrowserWindowPrivate RhayaderWebbrowserWindowPrivate;

struct _RhayaderWebbrowserWindow
{
  MxWindow parent;

  RhayaderWebbrowserWindowPrivate *priv;
};

struct _RhayaderWebbrowserWindowClass
{
  MxWindowClass parent_class;
};

typedef enum
{
	LIST_VIEW,
	NORMAL_VIEW,
	FULLSCREEN_VIEW
}eViews;

typedef enum
{
	WINDOW_SCROLLTO_TOP
}eWindowOption;

GType rhayader_webbrowser_window_get_type (void) G_GNUC_CONST;

MxWindow *rhayader_webbrowser_window_new (void);
void rhayader_webbrowser_window_initialize (MxWindow *self,gfloat toolbar_width);
ClutterActor* rhayader_webbrowser_window_get_normalview(MxWindow * self);

#ifndef WEBRUNTIME
ClutterActor* rhayader_webbrowser_window_get_listview(MxWindow * self);
eViews rhayader_webbrowser_window_get_currentview(MxWindow * self);
gboolean rhayader_webbrowser_window_switchto_listview(MxWindow * self);
gboolean rhayader_webbrowser_window_switchto_normalview(MxWindow * self);
gboolean rhayader_webbrowser_window_switchto_previousview(MxWindow * self);

#endif
G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_WINDOW_H */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-normal-view.h"
#ifndef WEBRUNTIME
#include "widgets/rhayader-list-roller.h"
#include "widgets/rhayader-zoom-item.h"
#include "mildenhall/mildenhall_context_drawer.h"
#endif
#include "mildenhall/mildenhall_webview_widget.h"
#include <libthornbury-ui-texture.h>

enum
{
  ACTION_REQUESTED,
  TRANSITION_STARTED,
  TRANSITION_COMPLETED,
  FULLSCREEN_TRANSITION_STARTED,
  FULLSCREEN_TRANSITION_COMPLETED,
  LAST_SIGNAL
};

enum
{
    DRAWER_COLUMN_NAME,
    DRAWER_COLUMN_ICON,
    DRAWER_COLUMN_TOOLTIP_TEXT,
    DRAWER_COLUMN_REACTIVE,
    DRAWER_COLUMN_LAST
};
G_DEFINE_TYPE (RhayaderWebBrowserNormalView, rhayader_webbrowser_normal_view, CLUTTER_TYPE_ACTOR)

#define WEBBROWSER_NORMAL_VIEW_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_NORMAL_VIEW, RhayaderWebBrowserNormalViewPrivate))

struct _RhayaderWebBrowserNormalViewPrivate
{

#ifndef WEBRUNTIME
	 ThornburyModel* 			m_model;
	 gfloat 				m_toolbarwidth ;

	/*normal view elements*/
	 ClutterActor* 			m_toolbar;
	RhayaderWebBrowserListRoller* m_zoomRoller;

	ClutterConstraint*       m_tabsize_constraint;

	ClutterActor* 			m_contextMenu;
	RhayaderWebbrowserSpeller* 	m_speller;
	gulong 					m_idle_resize_id;

	ClutterActor* m_topArea;
	ClutterActor* m_bottomArea;
	ClutterActor* m_overlay_actor;
#endif

	ClutterActor* 			m_webview_container;
	ClutterActor* 			m_viewport;
	ClutterActor*			 m_tab;
	gboolean 				m_isFullscreen;

};

static guint signals[LAST_SIGNAL] = { 0, };

//private functions
#ifndef WEBRUNTIME
static void
_rhayader_webbrowser_normal_view_item_activated_cb(RhayaderWebBrowserListRoller* zoom_roller,ClutterActor* item,RhayaderWebBrowserNormalView *self);
static void
_rhayader_webbrowser_normal_view_on_allocate_cb(ClutterActor* actor,const ClutterActorBox *allocation,
													ClutterAllocationFlags  flags,RhayaderWebBrowserNormalView* self);
static void _rhayader_webbrowser_normal_view_animate(RhayaderWebBrowserNormalView * self,gboolean is_show);
static void _rhayader_webbrowser_normal_view_tab_scale(RhayaderWebBrowserNormalView *self,gboolean is_scale,
													gdouble width,gdouble height, gdouble x, gdouble y, gboolean is_animate,
													ClutterGravity scale_gravity);

static void
_rhayader_webbrowser_normal_view_tab_transition_completed_cb (ClutterActor *actor,RhayaderWebBrowserNormalView *self);

static gboolean
_rhayader_webbrowser_normal_view_context_menu_option_cb(ClutterActor *button, gchar *name,RhayaderWebBrowserNormalView* self);

static gboolean _rhayader_webbrowser_normal_view_tab_is_maximized (RhayaderWebBrowserNormalView *self);
#endif

static void
rhayader_webbrowser_normal_view_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_normal_view_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_normal_view_class_init (RhayaderWebBrowserNormalViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
//  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserNormalViewPrivate));

  object_class->dispose = rhayader_webbrowser_normal_view_dispose;

signals[ACTION_REQUESTED] =
    g_signal_new ("action_requested",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__UINT,
                  G_TYPE_NONE, 1,G_TYPE_UINT);

  signals[TRANSITION_STARTED] =
    g_signal_new ("transition_started",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__BOOLEAN,
                  G_TYPE_NONE, 1,G_TYPE_BOOLEAN);

  signals[TRANSITION_COMPLETED] =
    g_signal_new ("transition_completed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__BOOLEAN,
                  G_TYPE_NONE, 1,G_TYPE_BOOLEAN);

  signals[FULLSCREEN_TRANSITION_STARTED] =
    g_signal_new ("fullscreen_transition_started",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__BOOLEAN,
                  G_TYPE_NONE, 1,G_TYPE_BOOLEAN);

  signals[FULLSCREEN_TRANSITION_COMPLETED] =
    g_signal_new ("fullscreen_transition_completed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__BOOLEAN,
                  G_TYPE_NONE, 1,G_TYPE_BOOLEAN);


}
#ifndef WEBRUNTIME

static void  _rhayader_webbrowser_normal_view_zoom_item_focussed_cb(RhayaderWebBrowserListRoller* m_zoomRoller,gboolean isnew,ClutterActor *actor,gpointer user_data)
{
	 RhayaderWebBrowserNormalViewPrivate* priv  = WEBBROWSER_NORMAL_VIEW_PRIVATE (user_data);
		guint zoomval=65;
		gdouble zoom_value;

		g_object_get(G_OBJECT(actor),"value",&zoomval,NULL);
		zoom_value = (gdouble)zoomval/100;
		if(priv->m_tab)
		g_object_set(priv->m_tab,"zoom_level",zoom_value,NULL);

}

static gboolean _rhayader_webbrowser_normal_view_set_zoom_item_focus(gpointer user_data)
{
	 RhayaderWebBrowserNormalViewPrivate* priv  = WEBBROWSER_NORMAL_VIEW_PRIVATE (user_data);
	rhayader_webbrowser_list_roller_set_focused_row(priv->m_zoomRoller,
						rhayader_webbrowser_zoom_item_get_displayed_index(0.665),TRUE,FALSE);
	return FALSE;
}



static gboolean
idle_on_allocate_cb (RhayaderWebBrowserNormalView* self)
{
	//ClutterActorClass *klass;
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);
	ClutterActorBox selfbox;
	ClutterActorBox bottombox;
	guint num_values = 0;
	guint *zoom_values = rhayader_webbrowser_zoom_item_get_values (&num_values);
	guint i = 0, j = 0;

	//place context menu and speller
	clutter_actor_get_content_box (CLUTTER_ACTOR (priv->m_bottomArea), &bottombox);
	clutter_actor_set_y(priv->m_contextMenu,bottombox.y2-85.0);
	clutter_actor_add_child(priv->m_bottomArea,priv->m_contextMenu);
	clutter_actor_add_child(priv->m_bottomArea,CLUTTER_ACTOR(priv->m_speller));

	//create zoom roller
	clutter_actor_get_content_box (CLUTTER_ACTOR (self), &selfbox);
	clutter_actor_set_width(priv->m_toolbar,priv->m_toolbarwidth);
	//clutter_actor_add_child(priv->m_toolbar,g_object_new(CLUTTER_TYPE_ACTOR,"width",priv->m_toolbarwidth,"height",85.0,"reactive",FALSE,NULL));
    priv->m_model = THORNBURY_MODEL(thornbury_list_model_new(1,G_TYPE_UINT,"value"));
  	for(i=0;i<2;i++)
  		for(j=0;j < num_values;j++)
  			thornbury_model_append(priv->m_model,0,zoom_values[j],-1);
    g_assert(G_IS_OBJECT(priv->m_model));
    rhayader_webbrowser_zoom_item_configure_width(priv->m_toolbarwidth);
	priv->m_zoomRoller = rhayader_webbrowser_list_roller_new(priv->m_toolbarwidth,selfbox.y2-(85),RHAYADER_TYPE_WEBBROWSER_ZOOM_ITEM,priv->m_model);
   g_signal_connect(priv->m_zoomRoller,"item-activated",
						 G_CALLBACK(_rhayader_webbrowser_normal_view_item_activated_cb),self);
   g_signal_connect(priv->m_zoomRoller,"item_focussed",
  						 G_CALLBACK(_rhayader_webbrowser_normal_view_zoom_item_focussed_cb),self);

   clutter_actor_add_child(priv->m_toolbar,CLUTTER_ACTOR(priv->m_zoomRoller));
   g_timeout_add(300,_rhayader_webbrowser_normal_view_set_zoom_item_focus,self);

  priv->m_idle_resize_id = 0;


     clutter_actor_show(priv->m_toolbar);
     clutter_actor_set_opacity(priv->m_toolbar,0xFF);
     clutter_actor_show(CLUTTER_ACTOR(priv->m_bottomArea));
     clutter_actor_set_opacity(CLUTTER_ACTOR(priv->m_bottomArea),0xFF);
     clutter_actor_show(CLUTTER_ACTOR(priv->m_overlay_actor));
     clutter_actor_set_opacity(CLUTTER_ACTOR(priv->m_overlay_actor),0xFF);


   /* remove the timeout */
   return G_SOURCE_REMOVE;

}


static void
_rhayader_webbrowser_normal_view_on_allocate_cb(ClutterActor* actor,const ClutterActorBox *allocation,
													ClutterAllocationFlags  flags,RhayaderWebBrowserNormalView* self)
{

	  if (!self->priv->m_idle_resize_id && (self->priv->m_zoomRoller == NULL))
		  self->priv->m_idle_resize_id = clutter_threads_add_timeout (100, (GSourceFunc)idle_on_allocate_cb, self);

}
#endif
/*creat the normal view elements*/


static void
rhayader_webbrowser_normal_view_init (RhayaderWebBrowserNormalView *self)
{
	RhayaderWebBrowserNormalViewPrivate* priv = self->priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);
#ifndef WEBRUNTIME
	ClutterBoxLayout *toolbar_vbox_layout;
	ThornburyModel *context_menu_model;
	ClutterBoxLayout *vbox_layout;
	ClutterActor *topAreaContainer;
	ClutterBoxLayout *hbox_layout;
	ClutterBoxLayout *bottom_hbox_layout;
#endif

	priv->m_isFullscreen = FALSE;
	priv->m_tab = NULL;

#ifdef WEBRUNTIME
	clutter_actor_set_layout_manager(CLUTTER_ACTOR(self),CLUTTER_LAYOUT_MANAGER(clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL)));
		priv->m_tab = mildenhall_webview_widget_new();
		g_object_set(priv->m_tab,"form-assistance",TRUE,NULL);
		clutter_actor_show(priv->m_tab);
		g_object_set(priv->m_tab,
					"x-expand",TRUE,
					"y-expand",TRUE,
					"x-align",CLUTTER_ACTOR_ALIGN_FILL,
					"y-align",CLUTTER_ACTOR_ALIGN_FILL,
					NULL);

		clutter_actor_add_child(CLUTTER_ACTOR(self),priv->m_tab);
#else
	priv->m_idle_resize_id = 0;
	priv->m_zoomRoller = NULL;
	priv->m_tabsize_constraint = clutter_bind_constraint_new(priv->m_viewport,CLUTTER_BIND_SIZE,0);
	g_object_ref(priv->m_tabsize_constraint);

	priv->m_tab = mildenhall_webview_widget_new();
			g_object_set(priv->m_tab,"form-assistance",TRUE,NULL);
			clutter_actor_show(priv->m_tab);
			g_object_set(priv->m_tab,
						"x-expand",TRUE,
						"y-expand",TRUE,
						"x-align",CLUTTER_ACTOR_ALIGN_FILL,
						"y-align",CLUTTER_ACTOR_ALIGN_FILL,
						NULL);
	priv->m_toolbar = clutter_actor_new();
	toolbar_vbox_layout = CLUTTER_BOX_LAYOUT (clutter_box_layout_new ());
	clutter_box_layout_set_orientation (toolbar_vbox_layout,CLUTTER_ORIENTATION_VERTICAL);
	clutter_box_layout_set_pack_start (toolbar_vbox_layout,FALSE);
	clutter_actor_set_layout_manager(priv->m_toolbar,CLUTTER_LAYOUT_MANAGER(toolbar_vbox_layout));



	g_object_set(priv->m_toolbar,
				"x-expand",FALSE,
				"y-expand",FALSE,
				"x-align",CLUTTER_ACTOR_ALIGN_END,
				"y-align",CLUTTER_ACTOR_ALIGN_START,
				NULL);
	//clutter_actor_add_child(CLUTTER_ACTOR(topArea),priv->m_toolbar);
	clutter_actor_set_reactive(priv->m_toolbar,TRUE);

	priv->m_viewport = clutter_actor_new();
	clutter_actor_show(priv->m_viewport);
	clutter_actor_set_layout_manager(CLUTTER_ACTOR(priv->m_viewport),clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL));
	g_object_set(priv->m_viewport,
			"x-expand",TRUE,
			"y-expand",TRUE,
			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
			"y-align",CLUTTER_ACTOR_ALIGN_FILL,
			NULL);
//	clutter_actor_add_child(topArea,priv->m_viewport);


	priv->m_speller = rhayader_webbrowser_speller_new();
	//clutter_actor_add_child(bottomArea,CLUTTER_ACTOR(priv->m_speller));
	//clutter_actor_hide(CLUTTER_ACTOR(priv->m_speller));
	//clutter_actor_set_height(bottomArea,clutter_actor_get_height(priv->m_speller));


		  //create context menu
				context_menu_model = THORNBURY_MODEL (thornbury_list_model_new (
							DRAWER_COLUMN_LAST, G_TYPE_STRING, NULL, G_TYPE_STRING, NULL,
							G_TYPE_STRING, NULL, G_TYPE_BOOLEAN, NULL, -1));

				 thornbury_model_append(context_menu_model,
								  DRAWER_COLUMN_NAME,"BACK",
								  DRAWER_COLUMN_ICON,ICONDIR"/icon_browser_back.png",
									 DRAWER_COLUMN_TOOLTIP_TEXT, _("BACK"),
									 DRAWER_COLUMN_REACTIVE,TRUE,
									-1);
				 thornbury_model_append(context_menu_model,
								  DRAWER_COLUMN_NAME,"RELOAD",
								  DRAWER_COLUMN_ICON,ICONDIR"/icon_reload.png",
								  DRAWER_COLUMN_TOOLTIP_TEXT, _("RELOAD"),
								  DRAWER_COLUMN_REACTIVE,TRUE,
									-1);
				 thornbury_model_append(context_menu_model,
								  DRAWER_COLUMN_NAME,"FORWARD",
								  DRAWER_COLUMN_ICON,ICONDIR"/icon_browser_forward.png",
								  DRAWER_COLUMN_TOOLTIP_TEXT, _("FORWARD"),
								  DRAWER_COLUMN_REACTIVE,TRUE,
									-1);
				 thornbury_model_append(context_menu_model,
								  DRAWER_COLUMN_NAME,"STORE PAGE",
								  DRAWER_COLUMN_ICON,ICONDIR"/icon_internet_saveaspdf_AC.png",
								  DRAWER_COLUMN_TOOLTIP_TEXT, _("STORE PAGE"),
								  DRAWER_COLUMN_REACTIVE,TRUE,-1);
		 priv->m_contextMenu = g_object_new(MILDENHALL_TYPE_CONTEXT_DRAWER,"model",context_menu_model,NULL);
		 g_object_set(priv->m_contextMenu,
					"x-expand",TRUE,
					"y-expand",FALSE,
					"x-align",CLUTTER_ACTOR_ALIGN_END,
					NULL);

				g_signal_connect(priv->m_contextMenu,
								 "drawer-button-released",
								 (GCallback) _rhayader_webbrowser_normal_view_context_menu_option_cb, self);

		priv->m_overlay_actor = thornbury_ui_texture_create_new (ICONDIR"/browser_normalview_overlay.png", 0,0,FALSE,FALSE);
		   g_object_set(priv->m_overlay_actor,
					// "y",49.0,
					 "x-expand",TRUE,
					 "y-expand",FALSE,
					 "x-align",CLUTTER_ACTOR_ALIGN_FILL,
					 "y-align",CLUTTER_ACTOR_ALIGN_START,
					 NULL);

	vbox_layout = CLUTTER_BOX_LAYOUT (clutter_box_layout_new ());
	clutter_box_layout_set_orientation (vbox_layout,CLUTTER_ORIENTATION_VERTICAL);
	clutter_box_layout_set_pack_start (vbox_layout,FALSE);
	clutter_box_layout_set_spacing (vbox_layout,1);
	clutter_actor_set_layout_manager(CLUTTER_ACTOR(self),CLUTTER_LAYOUT_MANAGER(vbox_layout));

    topAreaContainer = clutter_actor_new ();
    clutter_actor_set_layout_manager(topAreaContainer,clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL));
    priv->m_topArea = clutter_actor_new();
	hbox_layout = CLUTTER_BOX_LAYOUT (clutter_box_layout_new ());
	clutter_box_layout_set_orientation (hbox_layout,CLUTTER_ORIENTATION_HORIZONTAL);
	clutter_box_layout_set_pack_start (hbox_layout,TRUE);
	clutter_box_layout_set_spacing (hbox_layout,2);
    clutter_actor_set_layout_manager(CLUTTER_ACTOR( priv->m_topArea),CLUTTER_LAYOUT_MANAGER(hbox_layout));
    clutter_actor_add_child(topAreaContainer,priv->m_topArea);
    clutter_actor_add_child(topAreaContainer,priv->m_overlay_actor );

    priv->m_bottomArea = clutter_actor_new();
    	g_object_set(priv->m_bottomArea,
    			"x-expand",TRUE,
    			"y-expand",TRUE,
    			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
    			"y-align",CLUTTER_ACTOR_ALIGN_END,
    			NULL);
    	bottom_hbox_layout = CLUTTER_BOX_LAYOUT (clutter_box_layout_new ());
    	clutter_box_layout_set_orientation (bottom_hbox_layout,CLUTTER_ORIENTATION_HORIZONTAL);
    	clutter_box_layout_set_pack_start (bottom_hbox_layout,TRUE);
    	clutter_box_layout_set_spacing (bottom_hbox_layout,1);
    	clutter_actor_set_layout_manager(CLUTTER_ACTOR(priv->m_bottomArea),CLUTTER_LAYOUT_MANAGER(bottom_hbox_layout));
    	clutter_actor_set_height(priv->m_bottomArea,60.0);

    	clutter_actor_add_child(CLUTTER_ACTOR(self),topAreaContainer);
    	clutter_actor_add_child(CLUTTER_ACTOR(priv->m_topArea),priv->m_toolbar);
    	clutter_actor_add_child(priv->m_topArea,priv->m_viewport);

    	clutter_actor_add_child(CLUTTER_ACTOR(self),priv->m_bottomArea);
    	clutter_actor_set_position(priv->m_tab,0.0,0.0);
    	clutter_actor_add_child(priv->m_viewport,priv->m_tab);
    	//clutter_actor_set_position(priv->m_contextMenu,0.0,200.0);
    	//clutter_actor_add_child(priv->m_bottomArea,contextmenu_container);
    	//clutter_actor_add_child(priv->m_bottomArea,CLUTTER_ACTOR(priv->m_speller));

    	clutter_actor_hide(CLUTTER_ACTOR(self));
    	clutter_actor_set_opacity(CLUTTER_ACTOR(self),0x00);
		g_signal_connect_after (CLUTTER_ACTOR(self), "allocation-changed", G_CALLBACK (_rhayader_webbrowser_normal_view_on_allocate_cb), self);



#endif
}





RhayaderWebBrowserNormalView *
rhayader_webbrowser_normal_view_new (gfloat toolbar_width)
{
	RhayaderWebBrowserNormalView *self =  g_object_new (RHAYADER_TYPE_WEBBROWSER_NORMAL_VIEW,NULL);
#ifndef WEBRUNTIME
	self->priv->m_toolbarwidth = toolbar_width;
#endif
	return self;
}

#ifndef WEBRUNTIME
gboolean rhayader_webbrowser_normal_view_is_fullscreen(RhayaderWebBrowserNormalView * self)
{
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);
	return (priv->m_tab && priv->m_isFullscreen);
}
#endif

ClutterActor* rhayader_webbrowser_normal_view_get_tab(RhayaderWebBrowserNormalView* self)
{
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);
	//g_assert(rhayader_webbrowser_normal_view_is_shown(self));
	return priv->m_tab;
}


#ifndef WEBRUNTIME
void rhayader_webbrowser_normal_view_go_fullscreen(RhayaderWebBrowserNormalView * self,gboolean isFullscreen)
{
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);
	if(!priv->m_tab ||
		(priv->m_isFullscreen == isFullscreen))
		return;
	priv->m_isFullscreen = isFullscreen;
	//ClutterActor* view_parent = clutter_actor_get_parent(CLUTTER_ACTOR(self));
	if(priv->m_isFullscreen)
	{
		ClutterActorBox box;

		clutter_actor_set_size(priv->m_tab,clutter_actor_get_width(priv->m_viewport),clutter_actor_get_height(priv->m_viewport));
		priv->m_tab=g_object_ref(priv->m_tab);
		clutter_actor_remove_child(priv->m_viewport,priv->m_tab);
		clutter_actor_add_child(clutter_actor_get_parent(CLUTTER_ACTOR(self)),priv->m_tab);
		clutter_actor_get_content_box (CLUTTER_ACTOR (self), &box);
		_rhayader_webbrowser_normal_view_tab_scale(self,FALSE,box.x2,box.y2-box.y1,0,0,TRUE,CLUTTER_GRAVITY_NORTH_EAST);
	}
	else
	{
		clutter_actor_set_opacity(priv->m_toolbar,0xff);
		clutter_actor_set_opacity(CLUTTER_ACTOR(priv->m_bottomArea),0xff);
		//_rhayader_webbrowser_normal_view_tab_scale(self,FALSE,clutter_actor_get_x(self->priv->m_toolbar),clutter_actor_get_height(CLUTTER_ACTOR(priv->m_toolbar))),0,0,TRUE,CLUTTER_GRAVITY_NORTH_EAST);
		_rhayader_webbrowser_normal_view_tab_scale(self,FALSE,clutter_actor_get_width(self->priv->m_viewport),clutter_actor_get_height(CLUTTER_ACTOR(priv->m_viewport)),0,0,TRUE,CLUTTER_GRAVITY_NORTH_EAST);

	}
	g_signal_emit(self,signals[FULLSCREEN_TRANSITION_STARTED],0,priv->m_isFullscreen);
}

RhayaderWebbrowserSpeller* rhayader_webbrowser_normal_view_get_url_speller(RhayaderWebBrowserNormalView* self)
{
	return self->priv->m_speller;
}


static gboolean _rhayader_webbrowser_normal_view_show_view_cb(RhayaderWebBrowserNormalView* self)
{
	g_print("_rhayader_webbrowser_normal_view_show_view_cb ");
	//RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);

	_rhayader_webbrowser_normal_view_animate(self,TRUE);
		return G_SOURCE_REMOVE;
}

void rhayader_webbrowser_normal_view_show_view(RhayaderWebBrowserNormalView* self,ClutterActor* webview_widget)
{
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);

	if(rhayader_webbrowser_normal_view_is_shown(self))
		return;

	g_signal_emit(self,signals[TRANSITION_STARTED],0,TRUE);
	clutter_actor_show(priv->m_toolbar);
	clutter_actor_show(CLUTTER_ACTOR(priv->m_bottomArea));
	clutter_actor_show(CLUTTER_ACTOR(priv->m_overlay_actor));

	g_timeout_add(200,(GSourceFunc)_rhayader_webbrowser_normal_view_show_view_cb,self);

    g_object_set(priv->m_tab,"form-assistance",TRUE,NULL);
	return ;
}

void rhayader_webbrowser_normal_view_set_bottom_area_visible(RhayaderWebBrowserNormalView* self,gboolean is_raise)
{
	return;
	//rhayader_webbrowser_speller_set_visible(self->priv->m_speller,is_raise);
	 clutter_actor_save_easing_state (CLUTTER_ACTOR(self->priv->m_bottomArea));
	 clutter_actor_set_easing_mode (CLUTTER_ACTOR(self->priv->m_bottomArea), CLUTTER_LINEAR);
	 clutter_actor_set_easing_duration (CLUTTER_ACTOR(self->priv->m_bottomArea), 500);
	 clutter_actor_set_height(CLUTTER_ACTOR(self->priv->m_bottomArea),(is_raise == TRUE)?66:0);
	 clutter_actor_restore_easing_state (CLUTTER_ACTOR(self->priv->m_bottomArea));


}

void rhayader_webbrowser_normal_view_refresh(RhayaderWebBrowserNormalView* self)
{
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);
	gdouble zoom_level = 0.665;

	if(!priv->m_tab)
		return;

	g_object_get(priv->m_tab,"zoom_level",&zoom_level,NULL);

	if(rhayader_webbrowser_zoom_item_get_displayed_index(zoom_level) == rhayader_webbrowser_list_roller_get_focused_row(priv->m_zoomRoller))
		 return;
	rhayader_webbrowser_list_roller_set_focused_row(priv->m_zoomRoller,
					rhayader_webbrowser_zoom_item_get_displayed_index(zoom_level),TRUE,FALSE);

}


static gboolean _rhayader_webbrowser_normal_view_hide_view_cb(RhayaderWebBrowserNormalView * self)
{
	_rhayader_webbrowser_normal_view_animate(self,FALSE);
	return FALSE;
}
void rhayader_webbrowser_normal_view_hide_view(RhayaderWebBrowserNormalView * self)
{
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);
	if(!rhayader_webbrowser_normal_view_is_shown(self))
		return ;

	rhayader_webbrowser_speller_hide(priv->m_speller,FALSE);
	g_signal_emit(self,signals[TRANSITION_STARTED],0,FALSE);
	g_timeout_add(200,(GSourceFunc)_rhayader_webbrowser_normal_view_hide_view_cb,self);

	//_rhayader_webbrowser_normal_view_tab_scale(self,FALSE,width,height,x,y,TRUE,CLUTTER_GRAVITY_NORTH_EAST);

}

gboolean rhayader_webbrowser_normal_view_is_shown(RhayaderWebBrowserNormalView * self)
{
	return (CLUTTER_ACTOR_IS_VISIBLE(CLUTTER_ACTOR(self)) &&
					(clutter_actor_get_opacity(CLUTTER_ACTOR(self)) == 0xFF));

}
//private methods

static void _rhayader_webbrowser_normal_view_tab_scale(RhayaderWebBrowserNormalView *self,gboolean is_scale, gdouble width,
		gdouble height, gdouble x, gdouble y, gboolean is_animate,
		ClutterGravity scale_gravity)
{
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);
	ClutterActorBox tabContent;
	gfloat scale_x;
	gfloat scale_y;

	clutter_actor_remove_constraint(priv->m_tab,priv->m_tabsize_constraint);

	g_object_set(priv->m_tab,
			"min-width-set",FALSE,
			"x-expand",FALSE,
			"y-expand",FALSE,
			"x-align",CLUTTER_ACTOR_ALIGN_START,
			"y-align",CLUTTER_ACTOR_ALIGN_START,
			NULL);

	clutter_actor_get_content_box(priv->m_tab,&tabContent);
	scale_x = width/(tabContent.x2 - tabContent.x1);
	scale_y = height*1.75/(tabContent.y2 - tabContent.y1);
	 g_print(" \n scaling tab from %f,%f to %f,%f by %f,%f ",tabContent.x2 - tabContent.x1,tabContent.y2 - tabContent.y1,width,height,scale_x,scale_y);

	if (is_animate)
	{
		g_assert(CLUTTER_IS_ACTOR(priv->m_tab));

		 clutter_actor_save_easing_state (priv->m_tab);
		 clutter_actor_set_easing_duration (priv->m_tab, 600);
		 clutter_actor_set_easing_mode (priv->m_tab, CLUTTER_LINEAR);
		 clutter_actor_set_position(priv->m_tab,x,y);
		 if(!is_scale)
			 clutter_actor_set_size(priv->m_tab,width,height);
		 else
			 clutter_actor_set_scale(priv->m_tab,scale_x,scale_y);
		 clutter_actor_restore_easing_state (priv->m_tab);
		  g_signal_connect (G_OBJECT(priv->m_tab),
							"transitions-completed",
							G_CALLBACK (_rhayader_webbrowser_normal_view_tab_transition_completed_cb),
							self);

	}
	else
	{
		g_print("\n size=%f,%f pos=%f,%f",width,height,x,y);
		g_object_set(G_OBJECT(priv->m_tab),
					"scale-x",scale_x,
					"scale-y",scale_y,
					"x",x,
					"y",y,
					NULL);

	}
}



typedef void (*TabCallback)(gpointer user_data);
static void
_rhayader_webbrowser_normal_view_tab_transition_completed_cb (ClutterActor *actor,RhayaderWebBrowserNormalView *self)
{
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);
	g_print("\n _rhayader_webbrowser_tab_transition_completed_cb %d",_rhayader_webbrowser_normal_view_tab_is_maximized(self));

  /* disconnect so we don't get multiple notifications */
  g_signal_handlers_disconnect_by_func (actor,
		  G_CALLBACK(_rhayader_webbrowser_normal_view_tab_transition_completed_cb), self);

	if(_rhayader_webbrowser_normal_view_tab_is_maximized(self))
	{
		if(!priv->m_isFullscreen)
		{
			clutter_actor_remove_child(clutter_actor_get_parent(priv->m_tab),priv->m_tab);
			clutter_actor_add_child(priv->m_viewport,priv->m_tab);
			clutter_actor_add_constraint(actor,priv->m_tabsize_constraint);
		}
		else
		{
			clutter_actor_set_opacity(priv->m_toolbar,0x00);
			//clutter_actor_set_opacity(CLUTTER_ACTOR(priv->m_bottomArea),0x00);
		}



		g_object_set(priv->m_tab,
				"min-width-set",TRUE,
				"min-width",0.0,
				"x-expand",TRUE,
				"y-expand",TRUE,
				"x-align",CLUTTER_ACTOR_ALIGN_FILL,
				"y-align",CLUTTER_ACTOR_ALIGN_FILL,
				NULL);
		//g_timeout_add_seconds(1,_rhayader_webbrowser_normal_view_tab_size_reset_cb,self);

	}
		else
		{
			g_object_set(actor,
					"x-expand",FALSE,
					"y-expand",FALSE,
					"x-align",CLUTTER_ACTOR_ALIGN_START,
					"y-align",CLUTTER_ACTOR_ALIGN_START,
					NULL);
		}
	if(priv->m_isFullscreen)
	{
		g_signal_emit(self,signals[FULLSCREEN_TRANSITION_COMPLETED],0,TRUE);
	}
}

static gboolean
_rhayader_webbrowser_normal_view_context_menu_option_cb(ClutterActor *button, gchar *name,RhayaderWebBrowserNormalView* self)
{

	eContextMenuOption option = BACK;

	if (!g_strcmp0(name, "FORWARD"))
		option =FORWARD;
	else if (!g_strcmp0(name, "RELOAD"))
		option =RELOAD;
	else if (!g_strcmp0(name, "STORE PAGE"))
		option =SAVEASPDF;

	g_signal_emit(self,signals[ACTION_REQUESTED],0,option);
	return TRUE;

}

static void
_rhayader_webbrowser_normal_view_item_activated_cb(RhayaderWebBrowserListRoller* zoom_roller,ClutterActor* item,RhayaderWebBrowserNormalView *self)
{
	guint zoomval=0;
	gdouble zoom_value;

	g_object_get(G_OBJECT(item),"value",&zoomval,NULL);
	zoom_value = ((gdouble) zoomval) / 100;
	if(self->priv->m_tab)
		g_object_set(self->priv->m_tab,"zoom_level",zoom_value,NULL);

}

static gboolean _normal_view_refresh(gpointer data)
{
	RhayaderWebBrowserNormalView * self=(RhayaderWebBrowserNormalView *)data;
	rhayader_webbrowser_normal_view_refresh(self);
return  FALSE;
}

static void
_rhayader_webbrowser_normal_view_transition_completed_cb(ClutterActor* toolbar,RhayaderWebBrowserNormalView * self)
{
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);
	gboolean is_shown = (clutter_actor_get_opacity (toolbar) > 50);

	g_print("\n _rhayader_webbrowser_normal_view_transition_completed_cb");

  /* disconnect so we don't get multiple notifications */
  g_signal_handlers_disconnect_by_func (toolbar,
		  G_CALLBACK(_rhayader_webbrowser_normal_view_transition_completed_cb), self);

  if (!is_shown)
  {
	  clutter_actor_hide(CLUTTER_ACTOR(self));
	  /*clutter_actor_hide(toolbar);
	  clutter_actor_hide(CLUTTER_ACTOR(priv->m_bottomArea));
	  clutter_actor_hide(CLUTTER_ACTOR(priv->m_overlay_actor));*/
	  clutter_actor_set_opacity(CLUTTER_ACTOR(self),0x00);
	  rhayader_webbrowser_speller_set_title(priv->m_speller,"");

  }
  if(is_shown)
  {
	  g_timeout_add(500,_normal_view_refresh,self);
	  clutter_actor_show(CLUTTER_ACTOR(self));
	  clutter_actor_set_opacity(CLUTTER_ACTOR(self),0xFF);
	  clutter_actor_show(toolbar);
	  clutter_actor_show(CLUTTER_ACTOR(priv->m_bottomArea));
	  clutter_actor_show(CLUTTER_ACTOR(priv->m_overlay_actor));
  }

  g_signal_emit(self,signals[TRANSITION_COMPLETED],0,is_shown);
	//g_signal_emit_by_name(self,"transitions-completed",NULL);
}

static void _rhayader_webbrowser_normal_view_animate(RhayaderWebBrowserNormalView * self,gboolean is_show)
{
	RhayaderWebBrowserNormalViewPrivate* priv = WEBBROWSER_NORMAL_VIEW_PRIVATE (self);


	clutter_actor_show(CLUTTER_ACTOR(self));
	     clutter_actor_save_easing_state (CLUTTER_ACTOR(self));
		 clutter_actor_set_easing_mode (CLUTTER_ACTOR(self), CLUTTER_LINEAR);
		 clutter_actor_set_easing_duration (CLUTTER_ACTOR(self), 700);
		 clutter_actor_set_opacity(CLUTTER_ACTOR(self),(is_show == TRUE)?0xFF:0x00);
		 clutter_actor_restore_easing_state (CLUTTER_ACTOR(self));
		g_signal_connect_after (G_OBJECT(self),
								"transitions-completed",
								G_CALLBACK (_rhayader_webbrowser_normal_view_transition_completed_cb),
								self);



	clutter_actor_show(CLUTTER_ACTOR(priv->m_bottomArea));
	 clutter_actor_save_easing_state (CLUTTER_ACTOR(priv->m_bottomArea));
	 clutter_actor_set_easing_mode (CLUTTER_ACTOR(priv->m_bottomArea), CLUTTER_LINEAR);
	 clutter_actor_set_easing_duration (CLUTTER_ACTOR(priv->m_bottomArea), 700);
	 clutter_actor_set_opacity(CLUTTER_ACTOR(priv->m_bottomArea),(is_show == TRUE)?0xFF:0x00);
	 clutter_actor_restore_easing_state (CLUTTER_ACTOR(priv->m_bottomArea));

		clutter_actor_show(CLUTTER_ACTOR(priv->m_overlay_actor));
		 clutter_actor_save_easing_state (CLUTTER_ACTOR(priv->m_overlay_actor));
		 clutter_actor_set_easing_mode (CLUTTER_ACTOR(priv->m_overlay_actor), CLUTTER_LINEAR);
		 clutter_actor_set_easing_duration (CLUTTER_ACTOR(priv->m_overlay_actor), 700);
		 clutter_actor_set_opacity(CLUTTER_ACTOR(priv->m_overlay_actor),(is_show == TRUE)?0xFF:0x00);
		 clutter_actor_restore_easing_state (CLUTTER_ACTOR(priv->m_overlay_actor));


	clutter_actor_show(priv->m_toolbar);
	 clutter_actor_save_easing_state (priv->m_toolbar);
	 clutter_actor_set_easing_mode (priv->m_toolbar, CLUTTER_LINEAR);
	 clutter_actor_set_easing_duration (priv->m_toolbar, 700);
	 clutter_actor_set_opacity(priv->m_toolbar,(is_show == TRUE)?0xFF:0x00);
	 clutter_actor_restore_easing_state (priv->m_toolbar);
	  g_signal_connect_after (G_OBJECT(priv->m_toolbar),
						"transitions-completed",
						G_CALLBACK (_rhayader_webbrowser_normal_view_transition_completed_cb),
						self);
}

static gboolean _rhayader_webbrowser_normal_view_tab_is_maximized(RhayaderWebBrowserNormalView * self)
{
	return clutter_actor_get_width(self->priv->m_tab) > 250;
}
#endif /* !WEBRUNTIME */

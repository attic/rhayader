/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-window.h"
#include "rhayader-normal-view.h"
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "mildenhall/mildenhall_ui_utility.h"
#include <libthornbury-ui-texture.h>
#ifndef WEBRUNTIME
#include "../model/rhayader-list-model.h"
#include "rhayader-list-view.h"
#include "mildenhall/mildenhall_views_drawer.h"
#endif

#include <clutter/gdk/clutter-gdk.h>
enum
{
	ACTION_REQUESTED,
	TRANSITION_REQUESTED,
	LAST_SIGNAL
};

enum
{
	VIEWS_DRAWER_COLUMN_NAME,
	VIEWS_DRAWER_COLUMN_ICON,
	VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,
	VIEWS_DRAWER_COLUMN_REACTIVE,
	VIEWS_DRAWER_COLUMN_LAST
};

static guint signals[LAST_SIGNAL] = { 0, };

G_DEFINE_TYPE (RhayaderWebbrowserWindow, rhayader_webbrowser_window, MX_TYPE_WINDOW)

#define WEBBROWSER_WINDOW_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_WINDOW, RhayaderWebbrowserWindowPrivate))

struct _RhayaderWebbrowserWindowPrivate
{
	RhayaderWebBrowserNormalView* 				m_normalview;
	ClutterActor* 							m_curTab;
	ClutterActor* 							m_viewContainer;
#ifndef WEBRUNTIME
	ClutterActor*							m_viewMenu;
	ClutterActor*							m_fullscreenRestore;
	ClutterActor*							m_scrollTotop;
	RhayaderWebBrowserListView* 					m_listview;
#endif

};

#ifndef WEBRUNTIME
static gboolean
_rhayader_webbrowser_window_view_option_cb(ClutterActor *button, gchar *name,RhayaderWebbrowserWindow *self);
static void
_rhayader_webbrowser_window_listview_transition_completed_cb(RhayaderWebBrowserListView* listview,gboolean is_shown,RhayaderWebbrowserWindow *self);
static gboolean
_rhayader_webbrowser_window_fullscreen_restore_cb(ClutterActor *actor,ClutterEvent* event,RhayaderWebbrowserWindow *self);
static gboolean
_rhayader_webbrowser_window_fullscreen_view_cb(RhayaderWebbrowserWindow *self);
static gboolean
_rhayader_webbrowser_window_scrolltoTop_cb(ClutterActor *actor,ClutterEvent* event,RhayaderWebbrowserWindow *self);
#endif
#ifndef WEBRUNTIME
static gboolean touch_event(ClutterActor *actor, ClutterEvent *event, gpointer userData);
static gboolean _rhayader_webbrowser_window_scrolltoTop_touch_cb(ClutterActor *actor, ClutterEvent *event, gpointer userData);
#endif
static void rhayader_webbrowser_window_constructed (GObject *object);

static void
rhayader_webbrowser_window_dispose (GObject *object)
{
	G_OBJECT_CLASS (rhayader_webbrowser_window_parent_class)->dispose (object);
}


static void
rhayader_webbrowser_window_class_init (RhayaderWebbrowserWindowClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (klass, sizeof (RhayaderWebbrowserWindowPrivate));
	object_class->constructed = rhayader_webbrowser_window_constructed;
	object_class->dispose = rhayader_webbrowser_window_dispose;

	signals[TRANSITION_REQUESTED] =
			g_signal_new ("transition_requested",
					G_TYPE_FROM_CLASS (klass),
					G_SIGNAL_RUN_LAST,
					0,
					NULL, NULL,
					g_cclosure_marshal_VOID__UINT,
					G_TYPE_NONE, 1,G_TYPE_UINT);

	signals[ACTION_REQUESTED] =
			g_signal_new ("action_requested",
					G_TYPE_FROM_CLASS (klass),
					G_SIGNAL_RUN_LAST,
					0,
					NULL, NULL,
					g_cclosure_marshal_VOID__UINT,
					G_TYPE_NONE, 1,G_TYPE_UINT);


}

static gboolean _rhayader_webbrowser_window_blockButtonPressEvent_cb(ClutterActor* actor, ClutterButtonEvent* event, gpointer data)
{
	// Prevent clicks to propagate to the stage as they're used to scroll and
	// should not reach the default MxWindow handler (which otherwise moves
	// the window if it has a toolbar)
	return TRUE;
}



static void
rhayader_webbrowser_window_init (RhayaderWebbrowserWindow *self)
{
	self->priv = WEBBROWSER_WINDOW_PRIVATE (self);

}

MxWindow *
rhayader_webbrowser_window_new (void)
{
  return g_object_new (RHAYADER_TYPE_WEBBROWSER_WINDOW,
      NULL);
}

#ifndef WEBRUNTIME
/********************************************************
 * Function : touch_event
 * Description: Get the touch events and redirects it to the
 * 				button press and release.
 * Parameter :  VrnsProgressBar object.
 * Return value: void
 ********************************************************/
static gboolean touch_event(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	switch(event->type)
	{
	case CLUTTER_BUTTON_PRESS:
	case CLUTTER_TOUCH_BEGIN:
		break;

	case CLUTTER_BUTTON_RELEASE:
	case CLUTTER_TOUCH_END:

		_rhayader_webbrowser_window_fullscreen_restore_cb(actor,event,userData);

		break;
	default:
		return FALSE;
		break;
	}
	return TRUE;
}
#endif

static void
rhayader_webbrowser_window_constructed (GObject *object)
{
	RhayaderWebbrowserWindow *self = RHAYADER_WEBBROWSER_WINDOW (object);
	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
	ClutterStage *stage;
	ClutterActor* frame = clutter_actor_new();
	ClutterActor *bg;
	GdkWindow *gdk_window;
#ifdef WEBRUNTIME
	ClutterActor *overlay;
#else
	GdkPixbuf *pixbuf;
	ClutterContent *bg_image;
	ClutterActor *bg_actor;
	ClutterContent *zoom_image;
	ClutterActor *zoom_actor;
	ClutterContent *scroll_image;
	ClutterActor *scroll_actor;
#endif

	G_OBJECT_CLASS (rhayader_webbrowser_window_parent_class)->constructed (object);

	mx_window_set_has_toolbar (MX_WINDOW (self), FALSE);
  stage = mx_window_get_clutter_stage (MX_WINDOW (self));
  clutter_stage_set_throttle_motion_events (CLUTTER_STAGE (stage), FALSE);
  clutter_actor_set_size (stage, 728, 480);
  clutter_stage_set_user_resizable (stage, FALSE);
  clutter_actor_show (stage);
  gdk_window = clutter_gdk_get_stage_window (stage);
  gdk_window_set_decorations (gdk_window, 0);


	clutter_actor_set_layout_manager(frame,
			clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL)
	);
	mx_window_set_child(MX_WINDOW(self), frame);

	bg = mildenhall_ui_utils_create_application_background ();
	g_object_set(bg,
			"x-expand",TRUE,
			"y-expand",TRUE,
			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
			"y-align",CLUTTER_ACTOR_ALIGN_FILL,
			NULL);
	clutter_actor_add_child (frame,bg);


	priv->m_viewContainer = clutter_actor_new();
	clutter_actor_set_layout_manager(priv->m_viewContainer,
			clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL)
	);
	clutter_actor_add_child(frame,priv->m_viewContainer);

	clutter_actor_set_y(priv->m_viewContainer,50.0);
	g_object_set(priv->m_viewContainer,
			"x-expand",TRUE,
			// "y-expand",TRUE,
			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
			//	 "y-align",CLUTTER_ACTOR_ALIGN_FILL,
			NULL);
	clutter_actor_add_constraint(priv->m_viewContainer,clutter_bind_constraint_new(frame,CLUTTER_BIND_HEIGHT,-70.0));

	g_signal_connect(priv->m_viewContainer, "button-press-event",
			G_CALLBACK(_rhayader_webbrowser_window_blockButtonPressEvent_cb), 0);


#ifndef  WEBRUNTIME
	//fullscreen restore button

	pixbuf = gdk_pixbuf_new_from_file (ICONDIR"/bt_bg_single_normal.png", NULL);
	bg_image = clutter_image_new ();
	clutter_image_set_data (CLUTTER_IMAGE (bg_image),
			gdk_pixbuf_get_pixels (pixbuf),
			gdk_pixbuf_get_has_alpha (pixbuf)
			? COGL_PIXEL_FORMAT_RGBA_8888
					: COGL_PIXEL_FORMAT_RGB_888,
					  gdk_pixbuf_get_width (pixbuf),
					  gdk_pixbuf_get_height (pixbuf),
					  gdk_pixbuf_get_rowstride (pixbuf),
					  NULL);

	bg_actor = g_object_new (CLUTTER_TYPE_ACTOR,
		"width", 64.0,
		"height", 64.0,
		NULL);
	clutter_actor_set_content (bg_actor, bg_image);
	pixbuf = gdk_pixbuf_new_from_file (ICONDIR"/icon_zoomback.png", NULL);
	zoom_image = clutter_image_new ();
	clutter_image_set_data (CLUTTER_IMAGE (zoom_image),
			gdk_pixbuf_get_pixels (pixbuf),
			gdk_pixbuf_get_has_alpha (pixbuf)
			? COGL_PIXEL_FORMAT_RGBA_8888
					: COGL_PIXEL_FORMAT_RGB_888,
					  gdk_pixbuf_get_width (pixbuf),
					  gdk_pixbuf_get_height (pixbuf),
					  gdk_pixbuf_get_rowstride (pixbuf),
					  NULL);

	zoom_actor = g_object_new (CLUTTER_TYPE_ACTOR,
		"width", 36.0,
		"height", 36.0,
		NULL);
	clutter_actor_set_content (zoom_actor, zoom_image);

	priv->m_fullscreenRestore = g_object_new(CLUTTER_TYPE_ACTOR,"width",64.0,"height",64.0,NULL);
	clutter_actor_add_child (priv->m_fullscreenRestore, bg_actor);
	clutter_actor_add_child (priv->m_fullscreenRestore, zoom_actor);
	clutter_actor_set_position(zoom_actor,14.0,14.0);
	clutter_actor_add_child(frame,priv->m_fullscreenRestore);
	clutter_actor_set_reactive(priv->m_fullscreenRestore,TRUE);
	g_object_set(priv->m_fullscreenRestore,
			"x-expand",FALSE,
			"y-expand",FALSE,
			"x-align",CLUTTER_ACTOR_ALIGN_START,
			"y-align",CLUTTER_ACTOR_ALIGN_END,
			NULL);
	clutter_actor_set_height(priv->m_fullscreenRestore,0);
	clutter_actor_add_constraint(priv->m_fullscreenRestore,clutter_bind_constraint_new(frame,CLUTTER_BIND_HEIGHT,-18.0));
	g_signal_connect(priv->m_fullscreenRestore,
			"button-release-event",G_CALLBACK(_rhayader_webbrowser_window_fullscreen_restore_cb),self);
	g_signal_connect(priv->m_fullscreenRestore,"touch-event",G_CALLBACK(touch_event),self);



	bg_actor = g_object_new(CLUTTER_TYPE_ACTOR,"width",64.0,"height",64.0,NULL);
	clutter_actor_set_content (bg_actor, bg_image);

	pixbuf = gdk_pixbuf_new_from_file (ICONDIR"/icon_browser_backtotop_AC.png", NULL);
	scroll_image = clutter_image_new ();
	clutter_image_set_data (CLUTTER_IMAGE (scroll_image),
			gdk_pixbuf_get_pixels (pixbuf),
			gdk_pixbuf_get_has_alpha (pixbuf)
			? COGL_PIXEL_FORMAT_RGBA_8888
					: COGL_PIXEL_FORMAT_RGB_888,
					  gdk_pixbuf_get_width (pixbuf),
					  gdk_pixbuf_get_height (pixbuf),
					  gdk_pixbuf_get_rowstride (pixbuf),
					  NULL);
	scroll_actor = g_object_new (CLUTTER_TYPE_ACTOR,
		"width", 64.0,
		"height", 64.0,
		NULL);
	clutter_actor_set_content (scroll_actor, scroll_image);

	priv->m_scrollTotop = g_object_new(CLUTTER_TYPE_ACTOR,"width",64.0,"height",64.0,NULL);
	clutter_actor_add_child (priv->m_scrollTotop, bg_actor);
	clutter_actor_add_child (priv->m_scrollTotop, scroll_actor);
	clutter_actor_set_position(scroll_actor,14.0,14.0);
	clutter_actor_add_child(frame,priv->m_scrollTotop);
	clutter_actor_set_reactive(priv->m_scrollTotop,TRUE);
	g_object_set(priv->m_scrollTotop,
			"x-expand",FALSE,
			"y-expand",FALSE,
			"x-align",CLUTTER_ACTOR_ALIGN_END,
			"y-align",CLUTTER_ACTOR_ALIGN_END,
			NULL);
	clutter_actor_set_height(priv->m_scrollTotop,0);
	clutter_actor_add_constraint(priv->m_scrollTotop,clutter_bind_constraint_new(frame,CLUTTER_BIND_HEIGHT,-18.0));
	g_signal_connect(priv->m_scrollTotop,
			"button-release-event",G_CALLBACK(_rhayader_webbrowser_window_scrolltoTop_cb),self);
	g_signal_connect(priv->m_scrollTotop,"touch-event",G_CALLBACK(_rhayader_webbrowser_window_scrolltoTop_touch_cb),self);


	/*pixbuf = gdk_pixbuf_new_from_file (ICONDIR"/browser_content_overlay.png", NULL);
  bg_image = clutter_image_new ();
   clutter_image_set_data (CLUTTER_IMAGE (bg_image),
						   gdk_pixbuf_get_pixels (pixbuf),
						   gdk_pixbuf_get_has_alpha (pixbuf)
							 ? COGL_PIXEL_FORMAT_RGBA_8888
							 : COGL_PIXEL_FORMAT_RGB_888,
						   gdk_pixbuf_get_width (pixbuf),
						   gdk_pixbuf_get_height (pixbuf),
						   gdk_pixbuf_get_rowstride (pixbuf),
						   NULL);

   ClutterActor* overlay_actor = g_object_new(CLUTTER_TYPE_ACTOR,NULL);
   clutter_actor_set_content (overlay_actor, bg_image);*/


#else
	//overlay
	overlay = mildenhall_ui_utils_create_application_overlay ();
	g_object_set(overlay,
			// "y",-54.0,
			//"height",clutter_actor_get_height(frame)-80.0,
			"x-expand",TRUE,
			"y-expand",FALSE,
			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
			"y-align",CLUTTER_ACTOR_ALIGN_START,
			NULL);
	clutter_actor_add_child (frame,overlay);
#endif
}
#ifndef WEBRUNTIME
/********************************************************
 * Function : _rhayader_webbrowser_window_scrolltoTop_touch_cb
 * Description: Get the touch events and redirects it to the
 * 				button press and release.
 * Parameter :  VrnsProgressBar object.
 * Return value: void
 ********************************************************/
static gboolean _rhayader_webbrowser_window_scrolltoTop_touch_cb(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	switch(event->type)
	{
	case CLUTTER_BUTTON_PRESS:
	case CLUTTER_TOUCH_BEGIN:
		break;

	case CLUTTER_BUTTON_RELEASE:
	case CLUTTER_TOUCH_END:

		_rhayader_webbrowser_window_scrolltoTop_cb(actor,event,userData);

		break;
	default:
		return FALSE;
		break;
	}
	return TRUE;
}
#endif


#ifndef WEBRUNTIME
static gboolean
_rhayader_webbrowser_window_on_initialize_cb(RhayaderWebbrowserWindow *self)
{
	rhayader_webbrowser_window_switchto_listview(MX_WINDOW(self));

	return G_SOURCE_REMOVE;
}
#endif


void rhayader_webbrowser_window_initialize (MxWindow *win,gfloat toolbar_width)
{
	RhayaderWebbrowserWindow* self = RHAYADER_WEBBROWSER_WINDOW(win);
	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
#ifndef WEBRUNTIME
	RhayaderWebBrowserListModel *list_model;
	ThornburyModel *view_menu_model;
	ClutterActor *viewMenuContainer;
	ClutterActor *overlay_actor;
#endif

	/*	priv->m_isStandalone = isStandalone;
	 * if(priv->m_isStandalone)
	{
		mx_window_set_small_screen(win,TRUE);
		mx_window_set_fullscreen(win,FALSE);
	}
	 */



#ifndef WEBRUNTIME
	//create list view
	list_model = rhayader_webbrowser_list_model_get_singleton ();
	priv->m_listview = rhayader_webbrowser_list_view_new (
		THORNBURY_MODEL (list_model),
		toolbar_width);
	g_object_unref (list_model);
	clutter_actor_add_child(priv->m_viewContainer,CLUTTER_ACTOR(priv->m_listview));
	g_object_set(priv->m_listview,
			"x-expand",TRUE,
			"y-expand",TRUE,
			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
			"y-align",CLUTTER_ACTOR_ALIGN_FILL,
			NULL);
	g_signal_connect(priv->m_listview, "transition_completed",
			G_CALLBACK(_rhayader_webbrowser_window_listview_transition_completed_cb),self);
#endif

	//create normal view
	priv->m_normalview = rhayader_webbrowser_normal_view_new(toolbar_width);
	clutter_actor_add_child(priv->m_viewContainer,CLUTTER_ACTOR(priv->m_normalview));
	g_object_set(priv->m_normalview,
			"x-expand",TRUE,
			"y-expand",TRUE,
			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
			"y-align",CLUTTER_ACTOR_ALIGN_FILL,
			NULL);


#ifndef WEBRUNTIME
	//create view drawer
	view_menu_model = THORNBURY_MODEL (thornbury_list_model_new (VIEWS_DRAWER_COLUMN_LAST,
			G_TYPE_STRING, NULL, G_TYPE_STRING, NULL, G_TYPE_STRING, NULL,
			G_TYPE_BOOLEAN, NULL, -1));
	thornbury_model_append(view_menu_model, VIEWS_DRAWER_COLUMN_NAME, "BROWSER-VIEW",
			VIEWS_DRAWER_COLUMN_ICON, ICONDIR"/icon_view_browser.png",
			VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT, _("BROWSER"),
			VIEWS_DRAWER_COLUMN_REACTIVE, TRUE, -1);
	thornbury_model_append(view_menu_model, VIEWS_DRAWER_COLUMN_NAME, "TABS-VIEW",
			VIEWS_DRAWER_COLUMN_ICON, ICONDIR"/icon_view_listwiththumbs_AC.png",
			VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT, _("TABS"),
			VIEWS_DRAWER_COLUMN_REACTIVE, TRUE, -1);
	thornbury_model_append(view_menu_model, VIEWS_DRAWER_COLUMN_NAME, "FULLSCREEN-VIEW",
			VIEWS_DRAWER_COLUMN_ICON, ICONDIR"/icon_view_fullscreen.png",
			VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT, _("FULLSCREEN"),
			VIEWS_DRAWER_COLUMN_REACTIVE, TRUE, -1);

	viewMenuContainer = clutter_actor_new ();
	g_object_set(viewMenuContainer,
			"x-expand",TRUE,
			"y-expand",TRUE,
			"x-align",CLUTTER_ACTOR_ALIGN_END,
			"y-align",CLUTTER_ACTOR_ALIGN_FILL,
			NULL);
	priv->m_viewMenu = g_object_new(MILDENHALL_TYPE_VIEWS_DRAWER,"model",view_menu_model,NULL);
	clutter_actor_set_position(priv->m_viewMenu,0.0,0.0);
	clutter_actor_add_child(viewMenuContainer,priv->m_viewMenu);
	clutter_actor_set_width(priv->m_viewMenu,toolbar_width);
	g_object_set(priv->m_viewMenu,
			"x-expand",TRUE,
			"y-expand",TRUE,
			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
			"y-align",CLUTTER_ACTOR_ALIGN_FILL,
			NULL);
	//	clutter_actor_insert_child_below(priv->m_viewContainer,viewMenuContainer,NULL);
	clutter_actor_add_child(priv->m_viewContainer,viewMenuContainer);
	clutter_actor_set_reactive(priv->m_viewContainer,TRUE);
	g_signal_connect(priv->m_viewMenu,
			"drawer-button-released",
			(GCallback) _rhayader_webbrowser_window_view_option_cb, self);

	overlay_actor = thornbury_ui_texture_create_new (ICONDIR "/browser_window_overlay.png", 0, 0, FALSE, FALSE);
	g_object_set(overlay_actor,
			//"y",49.0,
			"x-expand",TRUE,
			"y-expand",FALSE,
			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
			"y-align",CLUTTER_ACTOR_ALIGN_START,
			NULL);
	clutter_actor_add_child (viewMenuContainer,overlay_actor);

	g_timeout_add_full(G_PRIORITY_HIGH_IDLE,2000,(GSourceFunc)_rhayader_webbrowser_window_on_initialize_cb,self,NULL);

#endif


}



ClutterActor* rhayader_webbrowser_window_get_normalview(MxWindow * self)
{
	return (ClutterActor*)(WEBBROWSER_WINDOW_PRIVATE (RHAYADER_WEBBROWSER_WINDOW(self))->m_normalview);
}

#ifndef WEBRUNTIME
ClutterActor* rhayader_webbrowser_window_get_listview(MxWindow * self)
{
	return (ClutterActor*)(WEBBROWSER_WINDOW_PRIVATE (RHAYADER_WEBBROWSER_WINDOW(self))->m_listview);
}

eViews rhayader_webbrowser_window_get_currentview(MxWindow * self)
{
	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
	eViews cur_view=LIST_VIEW;
	if(!rhayader_webbrowser_list_view_is_shown(priv->m_listview))
	{
		cur_view=NORMAL_VIEW;
		if(rhayader_webbrowser_normal_view_is_fullscreen(priv->m_normalview))
			cur_view=FULLSCREEN_VIEW;
	}
	return cur_view;

}




static gboolean listview_requested = FALSE;
gboolean rhayader_webbrowser_window_switchto_listview(MxWindow * self)
{
	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
	ThornburyModel *pViewsDrawerModel;
	GValue pColName = { 0, };
	GValue pColIcon = { 0, };
	GValue pColToolTip = { 0, };

	g_object_get(priv->m_viewMenu,"model",&pViewsDrawerModel,NULL);
	g_value_init(&pColName, G_TYPE_STRING);
	g_value_init(&pColIcon, G_TYPE_STRING);
	g_value_init(&pColToolTip, G_TYPE_STRING);

	g_value_set_string(&pColName, "BROWSER-VIEW");
	g_value_set_string(&pColIcon, ICONDIR"/icon_view_browser.png");
	g_value_set_string(&pColToolTip,  _("BROWSER"));
	thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_NAME,&pColName);
	thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
	thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);

	g_value_set_string(&pColName, "FULLSCREEN-VIEW");
	g_value_set_string(&pColIcon, ICONDIR"/icon_view_fullscreen.png");
	g_value_set_string(&pColToolTip, _("FULLSCREEN"));
	thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_NAME,&pColName);
	thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
	thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);


	g_value_set_string(&pColName, "TABS-VIEW");
	g_value_set_string(&pColIcon, ICONDIR"/icon_view_listwiththumbs_AC.png");
	g_value_set_string(&pColToolTip, _("TABS"));
	thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_NAME,&pColName);
	thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
	thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);









	if(!rhayader_webbrowser_list_view_is_shown(priv->m_listview))
	{
		g_signal_emit(self,signals[TRANSITION_REQUESTED],0,LIST_VIEW);

		if(rhayader_webbrowser_normal_view_is_fullscreen(priv->m_normalview))
		{
			listview_requested = TRUE;
			_rhayader_webbrowser_window_fullscreen_restore_cb(NULL,NULL,RHAYADER_WEBBROWSER_WINDOW(self));
		}
		else
		{
			rhayader_webbrowser_normal_view_hide_view(priv->m_normalview);
			rhayader_webbrowser_list_view_show(priv->m_listview);

		}

		return FALSE;
	}
	return TRUE;
}

gboolean rhayader_webbrowser_window_switchto_normalview(MxWindow * self)
{
	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
	ThornburyModel *pViewsDrawerModel;
	GValue pColName = { 0, };
	GValue pColIcon = { 0, };
	GValue pColToolTip = { 0, };
	ClutterActor *tab;

	g_object_get(priv->m_viewMenu,"model",&pViewsDrawerModel,NULL);
	g_value_init(&pColName, G_TYPE_STRING);
	g_value_init(&pColIcon, G_TYPE_STRING);
	g_value_init(&pColToolTip, G_TYPE_STRING);


	g_value_set_string(&pColName, "BROWSER-VIEW");
	g_value_set_string(&pColIcon, ICONDIR"/icon_view_browser.png");
	g_value_set_string(&pColToolTip,  _("BROWSER"));
	thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_NAME,&pColName);
	thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
	thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);

	g_value_set_string(&pColName, "FULLSCREEN-VIEW");
	g_value_set_string(&pColIcon, ICONDIR"/icon_view_fullscreen.png");
	g_value_set_string(&pColToolTip, _("FULLSCREEN"));
	thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_NAME,&pColName);
	thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
	thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);


	g_value_set_string(&pColName, "TABS-VIEW");
	g_value_set_string(&pColIcon, ICONDIR"/icon_view_listwiththumbs_AC.png");
	g_value_set_string(&pColToolTip, _("TABS"));
	thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_NAME,&pColName);
	thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
	thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);

	tab = rhayader_webbrowser_list_view_get_focused_tab (priv->m_listview);
	if(tab)
	{
		g_signal_emit(self,signals[TRANSITION_REQUESTED],0,NORMAL_VIEW);
		priv->m_curTab = tab;
		g_object_set_data(G_OBJECT(priv->m_curTab),"row",(gpointer)rhayader_webbrowser_list_view_get_focused_row(priv->m_listview));
		//_rhayader_webbrowser_tab_activated_cb(priv->m_listview,tab,self);
		g_assert(CLUTTER_IS_ACTOR(priv->m_curTab));
		rhayader_webbrowser_normal_view_show_view(priv->m_normalview,priv->m_curTab);
		rhayader_webbrowser_list_view_hide(priv->m_listview);
		return FALSE;
	}
	return TRUE;
}

gboolean rhayader_webbrowser_window_switchto_previousview(MxWindow * self)
{
#ifndef WEBRUNTIME
	return !rhayader_webbrowser_window_switchto_listview(self);
#else
	return rhayader_webbrowser_normal_view_go_back(self);
	#endif

}

static void
_rhayader_webbrowser_window_switchto_fullscreen(MxWindow *self)
{
	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
	if(!rhayader_webbrowser_normal_view_is_shown(priv->m_normalview))
		return;

	g_signal_emit(self,signals[TRANSITION_REQUESTED],0,FULLSCREEN_VIEW);
	//mx_window_set_fullscreen(self,TRUE);
	_rhayader_webbrowser_window_fullscreen_view_cb(RHAYADER_WEBBROWSER_WINDOW(self));
	return;

	/*ClutterActor* stage = (ClutterActor*)mx_window_get_clutter_stage(MX_WINDOW(self));
	clutter_actor_save_easing_state (stage);
	clutter_actor_set_easing_mode(stage,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration(stage,400);
	clutter_actor_set_width(stage,RHAYADER_BROWSER_FULLSCREEN_WINDOW_WIDTH);
	 clutter_actor_restore_easing_state (stage);
	g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE,600,(GSourceFunc)_rhayader_webbrowser_window_fullscreen_view_cb,self,NULL);*/
}

/*
static gboolean _rhayader_webbrowser_window_listview_restore_focussed_tab(RhayaderWebbrowserWindow *self)
{
	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
	rhayader_webbrowser_list_view_restore_focused_tab(priv->m_listview,priv->m_curTab);
	priv->m_curTab = NULL;
	return G_SOURCE_REMOVE;
}
*/

static void _rhayader_webbrowser_window_listview_transition_completed_cb(
		RhayaderWebBrowserListView *listview,
		gboolean is_shown,
		RhayaderWebbrowserWindow *self)

{
/*	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);ROHAN
	if(is_shown)
	{
		g_print("\n _rhayader_webbrowser_window_listview_transition_cb");
		if(CLUTTER_IS_ACTOR(priv->m_curTab))
		{
		//	g_idle_add((GSourceFunc)_rhayader_webbrowser_window_listview_restore_focussed_tab,self);
			_rhayader_webbrowser_window_listview_restore_focussed_tab(self);
		}
	}
	*/
}

static gboolean
_rhayader_webbrowser_window_view_option_cb(ClutterActor *button, gchar *name,RhayaderWebbrowserWindow *self)
{
	//RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
	if (!g_strcmp0(name, "TABS-VIEW"))
	{
		//g_object_set(button,"current-state",(gint64)0,NULL);
		rhayader_webbrowser_window_switchto_listview(MX_WINDOW(self));
		//Browser VIew and FUllScreen and Tab view
#if 0
		g_value_set_string(&pColName, "BROWSER-VIEW");
		g_value_set_string(&pColIcon, ICONDIR"/icon_view_browser.png");
		g_value_set_string(&pColToolTip,  _("BROWSER"));
		thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);

		g_value_set_string(&pColName, "FULLSCREEN-VIEW");
		g_value_set_string(&pColIcon, ICONDIR"/icon_view_fullscreen.png");
		g_value_set_string(&pColToolTip, _("FULLSCREEN"));
		thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);


		g_value_set_string(&pColName, "TABS-VIEW");
		g_value_set_string(&pColIcon, ICONDIR"/icon_view_listwiththumbs_AC.png");
		g_value_set_string(&pColToolTip, _("TABS"));
		thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
#endif
	}
	else if (!g_strcmp0(name, "BROWSER-VIEW"))
	{
		rhayader_webbrowser_window_switchto_normalview(MX_WINDOW(self));
		//g_object_set(button,"current-state",(gint64)1,NULL);
		//FullScreen,Tab,Browser
#if 0
		g_value_set_string(&pColName, "BROWSER-VIEW");
		g_value_set_string(&pColIcon, ICONDIR"/icon_view_browser.png");
		g_value_set_string(&pColToolTip,  _("BROWSER"));
		thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(pViewsDrawerModel,2,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);

		g_value_set_string(&pColName, "FULLSCREEN-VIEW");
		g_value_set_string(&pColIcon, ICONDIR"/icon_view_fullscreen.png");
		g_value_set_string(&pColToolTip, _("FULLSCREEN"));
		thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(pViewsDrawerModel,0,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);


		g_value_set_string(&pColName, "TABS-VIEW");
		g_value_set_string(&pColIcon, ICONDIR"/icon_view_listwiththumbs_AC.png");
		g_value_set_string(&pColToolTip, _("TABS"));
		thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_NAME,&pColName);
		thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_ICON,&pColIcon);
		thornbury_model_insert_value(pViewsDrawerModel,1,VIEWS_DRAWER_COLUMN_TOOLTIP_TEXT,&pColToolTip);
#endif
	}
	else if (!g_strcmp0(name, "FULLSCREEN-VIEW"))
	{
		_rhayader_webbrowser_window_switchto_fullscreen(MX_WINDOW(self));

	}
	return TRUE;
}

static gboolean _rhayader_webbrowser_window_switchto_lisview_cb(RhayaderWebbrowserWindow *self)
{
	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
	rhayader_webbrowser_normal_view_hide_view(priv->m_normalview);
	rhayader_webbrowser_list_view_show(priv->m_listview);
	return G_SOURCE_REMOVE;
}

static gboolean
_rhayader_webbrowser_window_fullscreen_view_cb(RhayaderWebbrowserWindow *self)
{
	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);

	//rhayader_webbrowser_normalview_controller_go_fullscreen(priv->m_normalview_controller,TRUE);
	rhayader_webbrowser_normal_view_go_fullscreen(self->priv->m_normalview,TRUE);
	//clutter_actor_set_child_above_sibling(priv->m_viewContainer,priv->m_fullscreenRestore,NULL);

	clutter_actor_save_easing_state (priv->m_fullscreenRestore);
	clutter_actor_set_easing_mode(priv->m_fullscreenRestore,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration(priv->m_fullscreenRestore,500);
	clutter_actor_set_height(priv->m_fullscreenRestore,64.0);
	clutter_actor_restore_easing_state (priv->m_fullscreenRestore);

	clutter_actor_save_easing_state (priv->m_scrollTotop);
	clutter_actor_set_easing_mode(priv->m_scrollTotop,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration(priv->m_scrollTotop,500);
	clutter_actor_set_height(priv->m_scrollTotop,64.0);
	clutter_actor_restore_easing_state (priv->m_scrollTotop);

	return G_SOURCE_REMOVE;
}

static gboolean
_rhayader_webbrowser_window_on_fullscreen_restore_view_cb(RhayaderWebbrowserWindow *self)
{
//	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);

	rhayader_webbrowser_normal_view_go_fullscreen(self->priv->m_normalview,FALSE);
	if(listview_requested)
		g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE,600,(GSourceFunc)_rhayader_webbrowser_window_switchto_lisview_cb,self,NULL);
	listview_requested = FALSE;

	return G_SOURCE_REMOVE;
}

static gboolean
_rhayader_webbrowser_window_on_fullscreen_restore_window_cb(RhayaderWebbrowserWindow *self)
{
//	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
	//mx_window_set_fullscreen(self,FALSE);
	_rhayader_webbrowser_window_on_fullscreen_restore_view_cb(self);
	return FALSE;

	/*
	ClutterActor* stage = (ClutterActor*)mx_window_get_clutter_stage(MX_WINDOW(self));
	clutter_actor_save_easing_state (stage);
	clutter_actor_set_easing_mode(stage,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration(stage,400);
	clutter_actor_set_width(stage,RHAYADER_BROWSER_NORMAL_WINDOW_WIDTH);
    clutter_actor_restore_easing_state (stage);

	g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE,600,(GSourceFunc)_rhayader_webbrowser_window_on_fullscreen_restore_view_cb,self,NULL);
	return G_SOURCE_REMOVE;
	 */
}

static gboolean
_rhayader_webbrowser_window_fullscreen_restore_cb(ClutterActor *actor,ClutterEvent* event,RhayaderWebbrowserWindow *self)
{
	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);

	g_print("\n _rhayader_webbrowser_on_fullscreen_restore_cb");

	clutter_actor_save_easing_state (priv->m_fullscreenRestore);
	clutter_actor_set_easing_mode(priv->m_fullscreenRestore,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration(priv->m_fullscreenRestore,500);
	clutter_actor_set_height(priv->m_fullscreenRestore,0);
	clutter_actor_restore_easing_state (priv->m_fullscreenRestore);
	clutter_actor_save_easing_state (priv->m_scrollTotop);
	clutter_actor_set_easing_mode(priv->m_scrollTotop,CLUTTER_LINEAR);
	clutter_actor_set_easing_duration(priv->m_scrollTotop,500);
	clutter_actor_set_height(priv->m_scrollTotop,0);
	clutter_actor_restore_easing_state (priv->m_scrollTotop);

	g_timeout_add_full(G_PRIORITY_HIGH,500,(GSourceFunc)_rhayader_webbrowser_window_on_fullscreen_restore_window_cb,self,NULL);


	return TRUE;
}

static gboolean
_rhayader_webbrowser_window_scrolltoTop_cb(ClutterActor *actor,ClutterEvent* event,RhayaderWebbrowserWindow *self)
{
//	RhayaderWebbrowserWindowPrivate* priv = WEBBROWSER_WINDOW_PRIVATE (self);
	g_signal_emit(self,signals[ACTION_REQUESTED],0,WINDOW_SCROLLTO_TOP);
	return TRUE;
}
#endif




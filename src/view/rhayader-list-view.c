/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-list-view.h"
#include "libthornbury-ui-texture.h"
#include "widgets/rhayader-list-roller.h"
#include "widgets/rhayader-list-item.h"
#include "widgets/rhayader-scroll-item.h"
#include "widgets/rhayader-zoom-item.h"
#include "mildenhall/mildenhall_context_drawer.h"





enum
{
  TAB_ACTIVATED,
  ACTION_REQUESTED,
  TRANSITION_STARTED,
  TRANSITION_COMPLETED,
  LAST_SIGNAL
};

G_DEFINE_TYPE (RhayaderWebBrowserListView, rhayader_webbrowser_list_view, CLUTTER_TYPE_ACTOR)

#define WEBBROWSER_LIST_VIEW_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_LIST_VIEW, RhayaderWebBrowserListViewPrivate))

struct _RhayaderWebBrowserListViewPrivate
{
	/*list view elements*/
	RhayaderWebBrowserListRoller* m_listRoller;

	ClutterActor* m_toolbar;
	ClutterActor* m_viewport;
	RhayaderWebBrowserListRoller* m_scrollRoller;
	gulong m_idle_resize_id;
	ThornburyModel* m_model;
	gfloat m_toolbarwidth;
	ClutterActor* m_contextMenu;

};
enum
{
    DRAWER_COLUMN_NAME,
    DRAWER_COLUMN_ICON,
    DRAWER_COLUMN_TOOLTIP_TEXT,
    DRAWER_COLUMN_REACTIVE,
    DRAWER_COLUMN_LAST
};
static guint signals[LAST_SIGNAL] = { 0, };

//private functions
static gboolean
_rhayader_webbrowser_list_view_context_menu_option_cb(ClutterActor *button, gchar *name,RhayaderWebBrowserListView* self);
static void
_rhayader_webbrowser_list_view_scrollitem_focussed_cb(RhayaderWebBrowserListRoller* listroller,guint isNew,ClutterActor*  focussed_item,RhayaderWebBrowserListView *self);
static void
_rhayader_webbrowser_list_view_item_focussed_cb(RhayaderWebBrowserListRoller* listRoller,guint isNew,ClutterActor* item,RhayaderWebBrowserListView* self);
static void
_rhayader_webbrowser_list_view_item_activated_cb(RhayaderWebBrowserListRoller* roller,ClutterActor* item,RhayaderWebBrowserListView *self);

static void
_rhayader_webbrowser_list_view_on_allocate_cb(ClutterActor* actor,const ClutterActorBox *allocation,
													ClutterAllocationFlags  flags,RhayaderWebBrowserListView* self);
static void _rhayader_webbrowser_list_view_animate(RhayaderWebBrowserListView * self,gboolean is_show);
static void _rhayader_webbrowser_list_view_transition_completed_cb (ClutterActor *actor,RhayaderWebBrowserListView * self);

static void
rhayader_webbrowser_list_view_dispose (GObject *object)
{
  RhayaderWebBrowserListView *self = RHAYADER_WEBBROWSER_LIST_VIEW (object);

  g_clear_object (&self->priv->m_model);

  G_OBJECT_CLASS (rhayader_webbrowser_list_view_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_list_view_class_init (RhayaderWebBrowserListViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
 // ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserListViewPrivate));

  object_class->dispose = rhayader_webbrowser_list_view_dispose;
  //actor_class->allocate = rhayader_webbrowser_list_view_allocate;


  signals[TAB_ACTIVATED] =
    g_signal_new ("tab_activated",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__POINTER,
                  G_TYPE_NONE, 1,G_TYPE_POINTER);

  signals[ACTION_REQUESTED] =
    g_signal_new ("action_requested",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__UINT,
                  G_TYPE_NONE, 1,G_TYPE_UINT);

  signals[TRANSITION_STARTED] =
    g_signal_new ("transition_started",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__BOOLEAN,
                  G_TYPE_NONE, 1,G_TYPE_BOOLEAN);

  signals[TRANSITION_COMPLETED] =
    g_signal_new ("transition_completed",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__BOOLEAN,
                  G_TYPE_NONE, 1,G_TYPE_BOOLEAN);


}

/*creat the list view elements*/
static void
rhayader_webbrowser_list_view_init (RhayaderWebBrowserListView *self)
{
	RhayaderWebBrowserListViewPrivate* priv = self->priv = WEBBROWSER_LIST_VIEW_PRIVATE (self);
	ClutterActor *listview_container;
	ClutterBoxLayout *box_layout;
	ClutterActor *overlay_actor;

	clutter_actor_set_layout_manager(CLUTTER_ACTOR(self),clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL));
    listview_container = clutter_actor_new ();
    box_layout = CLUTTER_BOX_LAYOUT (clutter_box_layout_new ());
    clutter_box_layout_set_orientation (box_layout,CLUTTER_ORIENTATION_HORIZONTAL);
    clutter_box_layout_set_pack_start (box_layout,TRUE);
    clutter_box_layout_set_spacing (box_layout,2);
    clutter_actor_set_layout_manager(CLUTTER_ACTOR(listview_container),CLUTTER_LAYOUT_MANAGER(box_layout));
    clutter_actor_add_child(CLUTTER_ACTOR(self),listview_container);

	self->priv->m_idle_resize_id = 0;
	self->priv->m_listRoller = NULL;
	self->priv->m_model = NULL;
	self->priv->m_scrollRoller = NULL;
    g_signal_connect (CLUTTER_ACTOR(self),
    		"allocation-changed", G_CALLBACK (_rhayader_webbrowser_list_view_on_allocate_cb), self);

	priv->m_toolbar = clutter_actor_new();
		clutter_actor_set_layout_manager(priv->m_toolbar,clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL));

		g_object_set(priv->m_toolbar,
					"x-expand",FALSE,
					"y-expand",TRUE,
					"x-align",CLUTTER_ACTOR_ALIGN_END,
					"y-align",CLUTTER_ACTOR_ALIGN_FILL,
					NULL);
	clutter_actor_add_child(CLUTTER_ACTOR(listview_container),CLUTTER_ACTOR(priv->m_toolbar));
	  clutter_actor_set_reactive(priv->m_toolbar,TRUE);

	  priv->m_viewport = clutter_actor_new();
		   clutter_actor_set_layout_manager(priv->m_viewport,clutter_bin_layout_new(CLUTTER_BIN_ALIGNMENT_FILL,CLUTTER_BIN_ALIGNMENT_FILL));
		   //clutter_actor_set_width(priv->m_viewport,box_width - RHAYADER_BROWSER_VIEW_TOOLBAR_WIDTH);
		   clutter_actor_show(priv->m_viewport);
		   g_object_set(priv->m_viewport,
		   			"x-expand",FALSE,
		   			"y-expand",TRUE,
		   			"x-align",CLUTTER_ACTOR_ALIGN_FILL,
		   			"y-align",CLUTTER_ACTOR_ALIGN_FILL,
		   			NULL);
	 clutter_actor_add_child(CLUTTER_ACTOR(listview_container),priv->m_viewport);

		overlay_actor = thornbury_ui_texture_create_new (ICONDIR "/browser_listview_overlay.png", 0, 0, FALSE, FALSE);
		   g_object_set(overlay_actor,
					// "y",49.0,
					 "x-expand",TRUE,
					 "y-expand",FALSE,
					 "x-align",CLUTTER_ACTOR_ALIGN_FILL,
					 "y-align",CLUTTER_ACTOR_ALIGN_START,
					 NULL);
		   clutter_actor_add_child(CLUTTER_ACTOR(self),overlay_actor);

     clutter_actor_show(CLUTTER_ACTOR(self));
	 clutter_actor_set_opacity(CLUTTER_ACTOR(self),0x00);

}



static gboolean
idle_on_allocate_cb (RhayaderWebBrowserListView* self)
{
	RhayaderWebBrowserListViewPrivate* priv = WEBBROWSER_LIST_VIEW_PRIVATE (self);
	gfloat box_width, box_height;
	gfloat list_roller_width;
	ThornburyModel *scrollModel;
	guint index;
	ThornburyModel *context_menu_model;
	ClutterActorBox box;

	  clutter_actor_get_size(CLUTTER_ACTOR(self),&box_width,&box_height);
	   g_print("\n !!!list view size %f,%f",box_width,box_height);


   //create list roller
		  g_assert(G_IS_OBJECT(priv->m_model));
		  list_roller_width = box_width - priv->m_toolbarwidth;
		  rhayader_webbrowser_list_item_configure_width(list_roller_width);
		   priv->m_listRoller = rhayader_webbrowser_list_roller_new(list_roller_width,box_height,RHAYADER_TYPE_WEBBROWSER_LIST_ITEM,priv->m_model);
		   clutter_actor_set_reactive(CLUTTER_ACTOR(priv->m_listRoller),TRUE);
			g_object_set(G_OBJECT(priv->m_listRoller),
						 "x-expand",TRUE,
						 "x-align",CLUTTER_ACTOR_ALIGN_FILL,
						 "y-expand",TRUE,
						 "y-align",CLUTTER_ACTOR_ALIGN_FILL,
						 NULL);
			clutter_actor_add_child(priv->m_viewport,CLUTTER_ACTOR(priv->m_listRoller));
			g_signal_connect_after(priv->m_listRoller,"item_focussed",
							G_CALLBACK(_rhayader_webbrowser_list_view_item_focussed_cb),self);
			 g_signal_connect_after(priv->m_listRoller,"item-activated",
								 G_CALLBACK(_rhayader_webbrowser_list_view_item_activated_cb),self);

//create scroll bar
		clutter_actor_set_width(priv->m_toolbar,priv->m_toolbarwidth);
		rhayader_webbrowser_scroll_item_configure_width(priv->m_toolbarwidth);
		scrollModel = THORNBURY_MODEL (thornbury_list_model_new (1, G_TYPE_UINT,"value"));
		for (index =1;index < 10;index++)
			thornbury_model_append(scrollModel,0,index,-1);
		g_assert(G_IS_OBJECT(scrollModel));
	  priv->m_scrollRoller = rhayader_webbrowser_list_roller_new(priv->m_toolbarwidth,box_height,RHAYADER_TYPE_WEBBROWSER_SCROLL_ITEM,scrollModel);
	  g_object_set(priv->m_scrollRoller,
				"x-expand",FALSE,
				"y-expand",TRUE,
				"x-align",CLUTTER_ACTOR_ALIGN_END,
				"y-align",CLUTTER_ACTOR_ALIGN_FILL,
				NULL);
	  clutter_actor_add_child(priv->m_toolbar,CLUTTER_ACTOR(priv->m_scrollRoller));
		g_signal_connect_after(priv->m_scrollRoller,"item_focussed",
						G_CALLBACK(_rhayader_webbrowser_list_view_scrollitem_focussed_cb),self);

//create context menu
    context_menu_model = THORNBURY_MODEL (thornbury_list_model_new (
             DRAWER_COLUMN_LAST, G_TYPE_STRING, NULL, G_TYPE_STRING, NULL,
             G_TYPE_STRING, NULL, G_TYPE_BOOLEAN, NULL, -1));


  thornbury_model_append(context_menu_model,
		  	  	  	  DRAWER_COLUMN_NAME,"NEW VIEW",
		  	  	  	  DRAWER_COLUMN_ICON,ICONDIR"/icon_view_new_AC.png",
                      DRAWER_COLUMN_TOOLTIP_TEXT, _("NEW VIEW"),
                      DRAWER_COLUMN_REACTIVE,TRUE,
                     -1);
  thornbury_model_append(context_menu_model,
		  	  	  	  DRAWER_COLUMN_NAME,"CLOSE VIEW",
		  	  	  	  DRAWER_COLUMN_ICON,ICONDIR"/icon_view_close_AC.png",
                      DRAWER_COLUMN_TOOLTIP_TEXT, _("CLOSE VIEW"),
                      DRAWER_COLUMN_REACTIVE,TRUE,
                     -1);
  clutter_actor_get_content_box (CLUTTER_ACTOR (priv->m_toolbar), &box);
  priv->m_contextMenu = g_object_new(MILDENHALL_TYPE_CONTEXT_DRAWER,"model",context_menu_model,NULL);
  //clutter_actor_set_position(priv->m_contextMenu,box.x1,box.y2-66.0);
  g_object_set(priv->m_contextMenu,
			"x-expand",FALSE,
  			//"y-expand",TRUE,
  			"x-align",CLUTTER_ACTOR_ALIGN_END,
  		//	"y-align",CLUTTER_ACTOR_ALIGN_END,
  			"y",box.y2-66.0,
  			NULL);
 // clutter_actor_add_constraint(priv->m_contextMenu,clutter_bind_constraint_new(frame,CLUTTER_BIND_HEIGHT,-40.0));
  clutter_actor_add_child(CLUTTER_ACTOR(self),priv->m_contextMenu);
  g_signal_connect(priv->m_contextMenu,
                   "drawer-button-released",
                   (GCallback) _rhayader_webbrowser_list_view_context_menu_option_cb, self);




  /* unset the guard */
  self->priv->m_idle_resize_id = 0;

  /* remove the timeout */
  return G_SOURCE_REMOVE;
}

static void
_rhayader_webbrowser_list_view_on_allocate_cb(ClutterActor* actor,const ClutterActorBox *allocation,
													ClutterAllocationFlags  flags,RhayaderWebBrowserListView* self)
{
	  /* throttle multiple actor allocations to one canvas resize; we use a guard
	   * variable to avoid queueing multiple resize operations
	  */
	if(!self->priv->m_idle_resize_id && !self->priv->m_listRoller)
		  self->priv->m_idle_resize_id = clutter_threads_add_timeout (1000, (GSourceFunc)idle_on_allocate_cb, self);
}


RhayaderWebBrowserListView *
rhayader_webbrowser_list_view_new (ThornburyModel* model,gfloat toolbar_width)
{
	RhayaderWebBrowserListView *self;

	g_return_val_if_fail (THORNBURY_IS_MODEL (model), NULL);

	self = g_object_new (RHAYADER_TYPE_WEBBROWSER_LIST_VIEW,
		NULL);

	self->priv->m_model = g_object_ref (model);
	self->priv->m_toolbarwidth = toolbar_width;
	return self;
}

gint rhayader_webbrowser_list_view_get_focused_row(RhayaderWebBrowserListView * self)
{
	if(self->priv->m_listRoller)
		return rhayader_webbrowser_list_roller_get_focused_row(self->priv->m_listRoller);
	else
		return -1;
}

void rhayader_webbrowser_list_view_set_focused_row(RhayaderWebBrowserListView * self,guint row_id,gboolean is_activate)
{
	if(self->priv->m_listRoller)
	{
		rhayader_webbrowser_list_roller_set_focused_row(self->priv->m_listRoller,row_id,TRUE,is_activate);
	}
}

ClutterActor* rhayader_webbrowser_list_view_get_focused_tab(RhayaderWebBrowserListView * self)
{
	RhayaderWebBrowserListViewPrivate* priv = WEBBROWSER_LIST_VIEW_PRIVATE (self);
	ClutterActor* item;
	gpointer tab = NULL;

	g_print("\n scroll state...%d",rhayader_webbrowser_list_view_in_motion(self));
	if(rhayader_webbrowser_list_view_in_motion(self) == TRUE)
		return NULL;

	item = rhayader_webbrowser_list_roller_get_item (priv->m_listRoller,
		rhayader_webbrowser_list_roller_get_focused_row (priv->m_listRoller));
	g_object_get(item,"tab",&tab,NULL);
	g_assert((tab != NULL) && CLUTTER_IS_ACTOR(tab));
	return tab;
}

ThornburyModel * rhayader_webbrowser_list_view_get_model(RhayaderWebBrowserListView * self)
{
	return self->priv->m_model;
}

void rhayader_webbrowser_list_view_restore_focused_tab(RhayaderWebBrowserListView * self,ClutterActor* tab)
{
	//RhayaderWebBrowserListViewPrivate* priv = WEBBROWSER_LIST_VIEW_PRIVATE (self);

	gint tab_row = (gint)g_object_steal_data(G_OBJECT(tab),"row");
	ClutterActor* item =rhayader_webbrowser_list_roller_get_item(self->priv->m_listRoller,tab_row);
	rhayader_webbrowser_list_item_restore_focussed_tab(RHAYADER_WEBBROWSER_LIST_ITEM(item),tab);
}

gboolean rhayader_webbrowser_list_view_in_motion(RhayaderWebBrowserListView * self)
{
	return rhayader_webbrowser_list_roller_is_moving(self->priv->m_listRoller);
}
void rhayader_webbrowser_list_view_show (RhayaderWebBrowserListView *self)
{
	if(rhayader_webbrowser_list_view_is_shown(self))
		return;
	g_print("\n rhayader_webbrowser_list_view_show");
	clutter_actor_show(CLUTTER_ACTOR(self));
	g_signal_emit(self,signals[TRANSITION_STARTED],0,TRUE);
	_rhayader_webbrowser_list_view_animate(self,TRUE);
}
gboolean rhayader_webbrowser_list_view_is_shown (RhayaderWebBrowserListView *self)
{
	return (CLUTTER_ACTOR_IS_VISIBLE(CLUTTER_ACTOR(self)) &&
			(clutter_actor_get_opacity(CLUTTER_ACTOR(self)) == 0xFF));
}

void rhayader_webbrowser_list_view_hide (RhayaderWebBrowserListView *self)
{
	if(!rhayader_webbrowser_list_view_is_shown(self))
		return;
	g_signal_emit(self,signals[TRANSITION_STARTED],0,FALSE);
         g_object_set(G_OBJECT(self->priv->m_contextMenu),"close",TRUE,NULL); 
	_rhayader_webbrowser_list_view_animate(self,FALSE);
}


static void _rhayader_webbrowser_list_view_animate(RhayaderWebBrowserListView * self,gboolean is_show)
{
	 clutter_actor_save_easing_state (CLUTTER_ACTOR(self));
	 clutter_actor_set_easing_mode (CLUTTER_ACTOR(self), CLUTTER_LINEAR);
	 clutter_actor_set_easing_duration (CLUTTER_ACTOR(self), 500);
	 clutter_actor_set_opacity(CLUTTER_ACTOR(self),(is_show == TRUE)?0xFF:0x00);
	 clutter_actor_restore_easing_state (CLUTTER_ACTOR(self));
	  g_signal_connect_after (G_OBJECT(self),
						"transitions-completed",
						G_CALLBACK (_rhayader_webbrowser_list_view_transition_completed_cb),
						self);

}

static gboolean
_rhayader_webbrowser_list_view_context_menu_option_cb(ClutterActor *button, gchar *name,RhayaderWebBrowserListView* self)
{

	eTabsMenuOption option = NEWVIEW;

	if (!g_strcmp0(name, "CLOSE VIEW"))
		option =CLOSEVIEW;
	g_signal_emit(self,signals[ACTION_REQUESTED],0,option);
	return TRUE;

}


/*static gboolean rhayader_webbrowser_list_view_focussed_item_notify_cb(RhayaderWebBrowserListView * self)
{
	ClutterActor* focussed_item = rhayader_webbrowser_list_roller_get_item(self->priv->m_listRoller,
										rhayader_webbrowser_list_roller_get_focused_row(self->priv->m_listRoller));
	_rhayader_webbrowser_list_view_item_activated_cb(self->priv->m_listRoller,focussed_item,self);

	return G_SOURCE_REMOVE;
}*/
static void _rhayader_webbrowser_list_view_item_focussed_cb(RhayaderWebBrowserListRoller* listRoller,guint isNew,ClutterActor* item,RhayaderWebBrowserListView* self)
{
	//rhayader_webbrowser_list_roller_set_focused_row(self->priv->m_scrollRoller,rhayader_webbrowser_list_roller_get_focused_row(listRoller));

	if(isNew)
	_rhayader_webbrowser_list_view_item_activated_cb(listRoller,item,self);
		//g_timeout_add(1000,rhayader_webbrowser_list_view_focussed_item_notify_cb,self);
}

static void
_rhayader_webbrowser_list_view_transition_completed_cb (ClutterActor *actor,RhayaderWebBrowserListView * self)
{
  gboolean is_shown = (clutter_actor_get_opacity (CLUTTER_ACTOR (self)) > 50);

	g_print("\n _rhayader_webbrowser_list_view_transition_completed_cb");
  /* disconnect so we don't get multiple notifications */
  g_signal_handlers_disconnect_by_func (actor,
		  G_CALLBACK(_rhayader_webbrowser_list_view_transition_completed_cb), self);

  if(!is_shown)
	  clutter_actor_hide(CLUTTER_ACTOR(self));
  //else
	//  clutter_actor_queue_relayout(CLUTTER_ACTOR(self->priv->m_listRoller));

  g_signal_emit(self,signals[TRANSITION_COMPLETED],0,is_shown);
}

static void
_rhayader_webbrowser_list_view_item_activated_cb(RhayaderWebBrowserListRoller* listroller,ClutterActor*  focussed_item,RhayaderWebBrowserListView *self)
{

	gpointer focussed_tab=NULL;
	g_object_get(focussed_item,"tab",&focussed_tab,NULL);
	g_assert((focussed_tab != NULL) && CLUTTER_IS_ACTOR(focussed_tab));
	if(focussed_tab)
	{
		g_print("\n tab activated");
		g_object_set_data(G_OBJECT(focussed_tab),"row",(gpointer)g_object_get_data(G_OBJECT(focussed_item),"row"));
		//g_object_set_data(G_OBJECT(focussed_tab),"row",(gpointer)rhayader_webbrowser_list_roller_get_focused_row(listroller));
		g_signal_emit(self,signals[TAB_ACTIVATED],0,focussed_tab);
	}
}
static void
_rhayader_webbrowser_list_view_scrollitem_focussed_cb(RhayaderWebBrowserListRoller* listroller,guint isNew,ClutterActor*  focussed_item,RhayaderWebBrowserListView *self)
{
	guint scrollval = 0;
	g_object_get(focussed_item,"value",&scrollval,NULL);

	rhayader_webbrowser_list_roller_set_focused_row(self->priv->m_listRoller,MIN(thornbury_model_get_n_rows(self->priv->m_model),scrollval),TRUE,FALSE);
}




/*typedef void  (*ViewCallback)(gpointer user_data);
typedef struct
{
	RhayaderWebBrowserListView*	self;
	RhayaderWebBrowserTab* current_tab;
	GCallback view_shown_cb;
	gpointer user_data;
}ViewCBData;

static void
rhayader_webbrowser_list_view_tab_shown_cb(ViewCBData* cb_data)
{
	//RhayaderWebBrowserListViewPrivate* priv = WEBBROWSER_LIST_VIEW_PRIVATE (cb_data->self);
	rhayader_webbrowser_list_view_add_tab(cb_data->self,cb_data->current_tab);
	if(cb_data->view_shown_cb)
		((ViewCallback)cb_data->view_shown_cb)(cb_data->user_data);
	g_free(cb_data);
}*/


/*called when user selects the list view .show the tabs roller and the menu*/
/*void rhayader_webbrowser_list_view_show (RhayaderWebBrowserListView *self,RhayaderWebBrowserTab* current_tab,GCallback view_shown_cb,gpointer user_data)
{
	ENTRY_TRACE
	RhayaderWebBrowserListViewPrivate* priv = WEBBROWSER_LIST_VIEW_PRIVATE (self);

	clutter_actor_show(CLUTTER_ACTOR(self->priv->m_tabs));
	clutter_actor_show(CLUTTER_ACTOR(self));
	clutter_actor_grab_key_focus(CLUTTER_ACTOR(self));
	//clutter_actor_set_opacity(CLUTTER_ACTOR(self),0xff);
	clutter_actor_animate(
			CLUTTER_ACTOR(self),BROWSER_ANIMATION,
			"opacity",0xff,
			NULL);

	clutter_actor_show(CLUTTER_ACTOR(priv->m_contextMenu));
	//clutter_actor_set_opacity(CLUTTER_ACTOR(priv->m_contextMenu),0xff);
	clutter_actor_animate(
			CLUTTER_ACTOR(self->priv->m_contextMenu),BROWSER_ANIMATION,
			"opacity",0xff,
			NULL);

	rhayader_webbrowser_tabs_update_focussed_tab(priv->m_tabs,TRUE);
	if(current_tab)
	{
		ViewCBData* cb_data = g_new(ViewCBData,1);
		cb_data->self = self;
		cb_data->current_tab = current_tab;
		cb_data->view_shown_cb = view_shown_cb;cb_data->user_data = user_data;
		clutter_actor_raise_top(CLUTTER_ACTOR(current_tab));
		rhayader_webbrowser_tab_minimize(current_tab,TRUE,(GCallback)rhayader_webbrowser_list_view_tab_shown_cb,cb_data);
	}
	else if(view_shown_cb){
		((ViewCallback)view_shown_cb)(user_data);
	}
}*/

/*void rhayader_webbrowser_list_view_hide (RhayaderWebBrowserListView *self)
{
	clutter_actor_hide(CLUTTER_ACTOR(self->priv->m_tabs));
	clutter_actor_animate(
			CLUTTER_ACTOR(self),BROWSER_ANIMATION,
			"opacity",0x00,
			NULL);
	clutter_actor_animate(
			CLUTTER_ACTOR(self->priv->m_contextMenu),BROWSER_ANIMATION,
			"opacity",0x00,
			NULL);
}*/







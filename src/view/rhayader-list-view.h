/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_LIST_VIEW_H
#define _RHAYADER_WEBBROWSER_LIST_VIEW_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include "libthornbury-model.h"

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_LIST_VIEW rhayader_webbrowser_list_view_get_type()

#define RHAYADER_WEBBROWSER_LIST_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_VIEW, RhayaderWebBrowserListView))

#define RHAYADER_WEBBROWSER_LIST_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_LIST_VIEW, RhayaderWebBrowserListViewClass))

#define RHAYADER_IS_WEBBROWSER_LIST_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_VIEW))

#define RHAYADER_IS_WEBBROWSER_LIST_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_LIST_VIEW))

#define RHAYADER_WEBBROWSER_LIST_VIEW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_VIEW, RhayaderWebBrowserListViewClass))

typedef struct _RhayaderWebBrowserListView RhayaderWebBrowserListView;
typedef struct _RhayaderWebBrowserListViewClass RhayaderWebBrowserListViewClass;
typedef struct _RhayaderWebBrowserListViewPrivate RhayaderWebBrowserListViewPrivate;

struct _RhayaderWebBrowserListView
{
  ClutterActor parent;

  RhayaderWebBrowserListViewPrivate *priv;
};

struct _RhayaderWebBrowserListViewClass
{
  ClutterActorClass parent_class;
};

typedef enum
{
	NEWVIEW,
	CLOSEVIEW
 }eTabsMenuOption;
GType rhayader_webbrowser_list_view_get_type (void) G_GNUC_CONST;

RhayaderWebBrowserListView *rhayader_webbrowser_list_view_new (ThornburyModel* model,gfloat toolbar_width);
void rhayader_webbrowser_list_view_show (RhayaderWebBrowserListView *self);
gboolean rhayader_webbrowser_list_view_is_shown (RhayaderWebBrowserListView *self);
void rhayader_webbrowser_list_view_hide (RhayaderWebBrowserListView *self);

ThornburyModel * rhayader_webbrowser_list_view_get_model(RhayaderWebBrowserListView * self);
gint rhayader_webbrowser_list_view_get_focused_row(RhayaderWebBrowserListView * self);
void rhayader_webbrowser_list_view_set_focused_row(RhayaderWebBrowserListView * self,guint row,gboolean is_activate);
ClutterActor* rhayader_webbrowser_list_view_get_focused_tab(RhayaderWebBrowserListView * self);

void rhayader_webbrowser_list_view_restore_focused_tab(RhayaderWebBrowserListView * self,ClutterActor* tab);
gboolean rhayader_webbrowser_list_view_in_motion(RhayaderWebBrowserListView * self);

//void rhayader_webbrowser_list_view_initialize (RhayaderWebBrowserListView *self);


G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_LIST_VIEW_H */

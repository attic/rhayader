/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-tab.h"
#include "rhayader-app.h"
#include "rhayader-mimehandler.h"
#include <errno.h>
#include <string.h>

#include <canterbury.h>

#pragma GCC diagnostic push
/* FIXME: stop disabling these warnings when the libraries are fixed.
 * https://bugs.apertis.org/show_bug.cgi?id=565 (WebKit-Clutter)
 * https://bugs.apertis.org/show_bug.cgi?id=567 (Mx)
 * https://bugs.apertis.org/show_bug.cgi?id=569 (Lightwood) */
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <liblightwood-webview-widget.h>
#include <mx/mx.h>
#include <webkit/webkit.h>
#include <webkit/webkitiwebview.h>
#include <webkit/mx/webkit/mx-web-view.h>
#include <webkit/webkitgwebframe.h>
#include <webkit/webkitwebpolicydecision.h>
#include <webkit/webkitwebwindowfeatures.h>
#include <webkit/webkitnetworkresponse.h>
#pragma GCC diagnostic pop

#define WEBKIT_IWEB_VIEW_GET_PAGE(webActor) webkit_iweb_view_get_page(WEBKIT_IWEB_VIEW(webActor))

G_DEFINE_TYPE (RhayaderWebBrowserTab, rhayader_webbrowser_tab, G_TYPE_OBJECT)

#define WEBBROWSER_TAB_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_TAB, RhayaderWebBrowserTabPrivate))

struct _RhayaderWebBrowserTabPrivate
{
	ClutterActor* 			m_webview_widget;
	ClutterActor*			m_webviewActor;
	ClutterActor* 			m_viewContainer;
	gfloat					m_focus_x;
	gfloat					m_focus_y;
	gfloat					m_thumbnail_width;
	gfloat					m_thumbnail_height;

	RhayaderWebBrowserMimeHandler* 	m_mime_handler;
	CanterburyAudioMgr* 	m_audio_manager_proxy;

	gboolean m_isStandalone;
	GList *m_tabs_backforwardlist;
	GList *m_recentlist;
#ifndef WEBRUNTIME
	GHashTable *m_recenthistorylist;
#endif
};

enum
{
	URL_PROGRESS,
	TITLE_CHANGE,
	MIME_DETECTED,
	FEED_DETECTED,
	NEW_TAB_REQUESTED,
	ZOOM_LEVEL_CHANGED,
	TAB_RESIZE,
	WEBVIEW_SPELLER_ALERT,
	BUSY_STATE_CHANGED,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, };

//private methods
static void _rhayader_webbrowser_tab_notify_title_cb(WebKitWebPage* web_view,
												GParamSpec* pspec, gpointer data);
static void _rhayader_webbrowser_tab_notify_load_status_cb(WebKitWebPage* webPage,
												GParamSpec* pspec, gpointer data);
static void _rhayader_webbrowser_tab_notify_progress_cb(WebKitWebPage* web_view,
												GParamSpec* pspec, gpointer data);
static void _rhayader_webbrowser_tab_new_view_requested_cb(LightwoodWebViewWidget* web_view,
												 gchar* url, RhayaderWebBrowserTab *self);
static void _rhayader_webbrowser_tab_mime_alert_cb(LightwoodWebViewWidget* web_view,
												WebKitDownload* download, RhayaderWebBrowserTab *self);
static void _rhayader_webbrowser_tab_rss_feed_alert_cb(LightwoodWebViewWidget* web_view,
												gboolean is_podcast,RhayaderWebBrowserTab *self);
static void _rhayader_webbrowser_tab_save_image_requested_cb(LightwoodWebViewWidget* web_view,
													gchar* url, RhayaderWebBrowserTab *self);
static void _rhayader_webbrowser_tab_notify_zoom_level_changed_cb(LightwoodWebViewWidget* web_view,
													GParamSpec* pspec, RhayaderWebBrowserTab *self);
static void _rhayader_webbrowser_tab_notify_speller_state_cb(LightwoodWebViewWidget* web_view,gboolean is_shown,RhayaderWebBrowserTab *self);

static void _rhayader_webbrowser_tab_audio_status_change_cb(LightwoodWebViewWidget* web_view,CanterburyAudioMgr *audio_manager_proxy);
static void _rhayader_webbrowser_tab_audio_response_cb(CanterburyAudioMgr *object, const gchar *arg_app_name, guint arg_audio_result,LightwoodWebViewWidget* webview_widget);

#ifndef WEBRUNTIME
static void _tab_free_data(gpointer data);
#endif

static void
rhayader_webbrowser_tab_get_property (GObject    *object,
                                 guint       property_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
rhayader_webbrowser_tab_set_property (GObject      *object,
                                 guint         property_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
rhayader_webbrowser_tab_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_tab_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_tab_class_init (RhayaderWebBrowserTabClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserTabPrivate));

  object_class->get_property = rhayader_webbrowser_tab_get_property;
  object_class->set_property = rhayader_webbrowser_tab_set_property;
  object_class->dispose = rhayader_webbrowser_tab_dispose;

	signals[URL_PROGRESS] = g_signal_new("url_progress",
			G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
			g_cclosure_marshal_VOID__DOUBLE, G_TYPE_NONE, 1, G_TYPE_DOUBLE);

	signals[ZOOM_LEVEL_CHANGED] = g_signal_new("zoom_level_changed",
			G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
			g_cclosure_marshal_VOID__DOUBLE, G_TYPE_NONE, 1, G_TYPE_DOUBLE);

	signals[TITLE_CHANGE] = g_signal_new("title_change",
			G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
			g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1, G_TYPE_STRING);

	signals[NEW_TAB_REQUESTED] = g_signal_new("new_tab_requested",
			G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
			g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1, G_TYPE_POINTER);

	signals[MIME_DETECTED] = g_signal_new("mime_detected",
			G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
			g_cclosure_marshal_VOID__UINT_POINTER, G_TYPE_NONE, 2,G_TYPE_UINT,G_TYPE_POINTER);

	signals[FEED_DETECTED] = g_signal_new("feed_detected",
			G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
			g_cclosure_marshal_VOID__UINT, G_TYPE_NONE, 1,G_TYPE_UINT);

	  signals[WEBVIEW_SPELLER_ALERT] =
	      g_signal_new ("webview_speller_alert",
	                    G_TYPE_FROM_CLASS (klass),
	                    G_SIGNAL_RUN_LAST,
	                    0,
	                    NULL, NULL,
	                    g_cclosure_marshal_VOID__BOOLEAN,
	                    G_TYPE_NONE, 1,G_TYPE_BOOLEAN);

	/**
	 * RhayaderWebBrowserTab::busy-state-changed:
	 * @busy: %TRUE if the tab is busy, for example loading a new page
	 *
	 * Emitted when the tab becomes busy (for example because it has
	 * been asked to load a page) or non-busy (for example because
	 * page loading has completed).
	 */
	signals[BUSY_STATE_CHANGED] = g_signal_new ("busy-state-changed",
		G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
		g_cclosure_marshal_VOID__BOOLEAN,
		G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
}

static void
rhayader_webbrowser_tab_init (RhayaderWebBrowserTab *self)
{
  RhayaderWebBrowserTabPrivate* priv = self->priv = WEBBROWSER_TAB_PRIVATE (self);
  priv->m_webview_widget = NULL;
  priv->m_webviewActor = NULL;
  priv->m_mime_handler = NULL;
  priv->m_audio_manager_proxy = NULL;
  priv->m_isStandalone=FALSE;
  priv->m_tabs_backforwardlist = NULL;
  priv->m_recentlist = NULL;
#ifndef WEBRUNTIME
	priv->m_recenthistorylist = g_hash_table_new_full(g_str_hash,g_str_equal,_tab_free_data,_tab_free_data);
#endif
}

RhayaderWebBrowserTab *
rhayader_webbrowser_tab_new (gboolean is_standalone)
{
	RhayaderWebBrowserTab * self =  g_object_new (RHAYADER_TYPE_WEBBROWSER_TAB, NULL);
	self->priv->m_isStandalone = is_standalone;
	return self;
}


static gboolean _rhayader_webbrowser_blockButtonPressEvent_cb(ClutterActor* actor, ClutterButtonEvent* event, gpointer data)
{
    // Prevent clicks to propagate to the stage as they're used to scroll and
    // should not reach the default MxWindow handler (which otherwise moves
    // the window if it has a toolbar)
    return TRUE;
}
void rhayader_webbrowser_tab_uninitialize(RhayaderWebBrowserTab * self)
{
	RhayaderWebBrowserTabPrivate* priv = WEBBROWSER_TAB_PRIVATE (self);

	if(CLUTTER_IS_ACTOR(priv->m_webview_widget))
	{
//		g_print("\n un-initialize %d",priv->m_webview_widget);
		WebKitWebPage* webPage = WEBKIT_IWEB_VIEW_GET_PAGE(priv->m_webviewActor);

		rhayader_webbrowser_tab_pause_audio (self);
		g_signal_handlers_disconnect_by_data(priv->m_webview_widget,self);
		g_signal_handlers_disconnect_by_data(webPage,self);
	}

}



void rhayader_webbrowser_tab_initialize(RhayaderWebBrowserTab * self,ClutterActor* webview)
{
	RhayaderWebBrowserTabPrivate* priv = WEBBROWSER_TAB_PRIVATE (self);
	WebKitWebPage *webPage;

	rhayader_webbrowser_tab_uninitialize(self);

	priv->m_webview_widget = webview;
	lightwood_webview_widget_set_form_assistance(priv->m_webview_widget, TRUE);
	//g_print("\n initialize %d",priv->m_webview_widget);
	priv->m_webviewActor = lightwood_webview_widget_get_webview(priv->m_webview_widget);
	webPage = WEBKIT_IWEB_VIEW_GET_PAGE (priv->m_webviewActor);
#ifndef WEBRUNTIME
	webkit_web_page_set_zoom_level(webPage, 0.665);
#endif
	//webkit_web_page_set_zoom_level(WEBKIT_IWEB_VIEW_GET_PAGE(webkitActor),1.0);
	clutter_actor_set_reactive(priv->m_webview_widget,TRUE);
  	g_signal_connect_after((priv->m_webview_widget), "button-press-event",
  	                     G_CALLBACK(_rhayader_webbrowser_blockButtonPressEvent_cb), 0);


	g_object_connect(priv->m_webview_widget,
			"signal::button-press-event", G_CALLBACK(_rhayader_webbrowser_blockButtonPressEvent_cb), self,
			NULL);

	g_object_connect(webPage,
			"signal::notify::title",G_CALLBACK (_rhayader_webbrowser_tab_notify_title_cb), self,
			"signal::notify::load-status",G_CALLBACK (_rhayader_webbrowser_tab_notify_load_status_cb), self,
			"signal::notify::progress",G_CALLBACK (_rhayader_webbrowser_tab_notify_progress_cb), self,
			//"signal::notify::zoom-level",G_CALLBACK(_rhayader_webbrowser_tab_notify_zoom_cb), self,
			NULL);

	g_object_connect(priv->m_webview_widget,
			//"signal::double_tap_alert",	G_CALLBACK (_rhayader_webbrowser_tab_double_tap_alert_cb), self,
			"signal_after::new_view_requested",G_CALLBACK (_rhayader_webbrowser_tab_new_view_requested_cb),self,
			"signal_after::mime_alert",G_CALLBACK (_rhayader_webbrowser_tab_mime_alert_cb), self,
			"signal_after::rss_feed_alert",G_CALLBACK (_rhayader_webbrowser_tab_rss_feed_alert_cb), self,
			"signal_after::save_image_requested",G_CALLBACK (_rhayader_webbrowser_tab_save_image_requested_cb), self,
			"signal_after::notify::zoom_level",G_CALLBACK (_rhayader_webbrowser_tab_notify_zoom_level_changed_cb), self,
			"signal::webview_speller_alert",G_CALLBACK (_rhayader_webbrowser_tab_notify_speller_state_cb), self,
			NULL);

	priv->m_audio_manager_proxy = NULL;
	if(priv->m_isStandalone == FALSE)
	{
		priv->m_audio_manager_proxy = canterbury_audio_mgr_proxy_new_for_bus_sync(
			 	 	 	 	 	 	 	 G_BUS_TYPE_SESSION,G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			 	 	 	 	 	 	 	 "org.apertis.Canterbury","/org/apertis/Canterbury/AudioMgr",NULL,NULL);
		g_assert(priv->m_audio_manager_proxy != NULL);
		g_signal_connect(priv->m_audio_manager_proxy, "audio-response", G_CALLBACK (_rhayader_webbrowser_tab_audio_response_cb), priv->m_webview_widget);
		g_signal_connect(priv->m_webview_widget,"audio_status_change",G_CALLBACK(_rhayader_webbrowser_tab_audio_status_change_cb),priv->m_audio_manager_proxy);
	}
	if(!priv->m_mime_handler)
		priv->m_mime_handler = rhayader_webbrowser_mimehandler_new(G_OBJECT(self),clutter_actor_get_stage(webview));

	//rhayader_webbrowser_tab_play_audio(self);

	_rhayader_webbrowser_tab_notify_title_cb(webPage,NULL,self);
	_rhayader_webbrowser_tab_notify_progress_cb(webPage,NULL,self);


         #ifdef WEBRUNTIME
	lightwood_webview_widget_set_gesture_handling(priv->m_webview_widget,FALSE);
        #endif

}

/*static void _rhayader_webbrowser_tab_dbw_audio_request_granted_cb(gboolean is_play,RhayaderWebBrowserTab *self)
{
	g_print("\n _rhayader_webbrowser_tab_dbw_audio_request_granted_cb");
	if(is_play)
		rhayader_web_view_widget_play_audio(self->priv->m_scroll_view);
	else
		rhayader_web_view_widget_pause_audio(self->priv->m_scroll_view);
}*/

static void
_rhayader_webbrowser_tab_audio_response_cb(CanterburyAudioMgr *object, const gchar *arg_app_name, guint arg_audio_result,LightwoodWebViewWidget* webview_widget)
{
	if( !(arg_app_name && !g_strcmp0(arg_app_name,lightwood_webview_widget_get_app_name(webview_widget))) )
			return;

	switch(arg_audio_result)
	{
		case CANTERBURY_AUDIO_PLAY:
			{
				lightwood_webview_widget_play_audio(CLUTTER_ACTOR(webview_widget));
			}
			break;

		case CANTERBURY_AUDIO_PAUSE:
			{
				//lightwood_webview_widget_pause_audio(CLUTTER_ACTOR(webview_widget));
			}
			break;

		case CANTERBURY_AUDIO_STOP:
			break;

		default:
			break;
	}
}

static void _rhayader_webbrowser_tab_audio_status_change_cb(LightwoodWebViewWidget* web_view,CanterburyAudioMgr *audio_manager_proxy)
{
	gint audio_status = LIGHTWOOD_WEBVIEW_AUDIO_STATUS_ENDED;
	gchar *app_name = lightwood_webview_widget_get_app_name (web_view);

	g_print("\n _rhayader_webbrowser_tab_audio_status_change_cb %d",__LINE__);
	//g_print("\n _rhayader_webbrowser_tab_audio_status_change_cb %d",__LINE__);
	g_object_get(web_view, "audio_status", &audio_status, NULL);
	//g_print("\n _rhayader_webbrowser_tab_audio_status_change_cb %d",__LINE__);
	if(audio_manager_proxy && CANTERBURY_IS_AUDIO_MGR(audio_manager_proxy))
	{
		//g_print("\n _rhayader_webbrowser_tab_audio_status_change_cb %d",__LINE__);

		if(audio_status == LIGHTWOOD_WEBVIEW_AUDIO_STATUS_PLAYING)
			{
		//	g_print("\n _rhayader_webbrowser_tab_audio_status_change_cb %d %s ",__LINE__,app_name);

			canterbury_audio_mgr_call_audio_request(audio_manager_proxy, app_name, NULL, NULL, NULL);
			}

		else

			{
		//	g_print("\n _rhayader_webbrowser_tab_audio_status_change_cb %d",__LINE__);
			canterbury_audio_mgr_call_audio_release(audio_manager_proxy,app_name, NULL, NULL, NULL);
			}
	}
}

void
rhayader_webbrowser_tab_load_url (RhayaderWebBrowserTab *self,
    const gchar *url)
{
	RhayaderWebBrowserTabPrivate* priv = WEBBROWSER_TAB_PRIVATE (self);

	g_signal_emit (self, signals[BUSY_STATE_CHANGED], 0, TRUE);

	  if(CLUTTER_IS_ACTOR(priv->m_webviewActor) && priv->m_webview_widget && url )
		  lightwood_webview_widget_load_url(priv->m_webview_widget,url);
}
gchar* rhayader_webbrowser_tab_get_url(RhayaderWebBrowserTab *self)
{
	return (gchar*) webkit_web_page_get_uri(
			WEBKIT_IWEB_VIEW_GET_PAGE(self->priv->m_webviewActor));

}
gchar* rhayader_webbrowser_tab_get_title(RhayaderWebBrowserTab *self)
{
	return (gchar*) webkit_web_page_get_title(
				WEBKIT_IWEB_VIEW_GET_PAGE(self->priv->m_webviewActor));
}
gboolean rhayader_webbrowser_tab_go_back(RhayaderWebBrowserTab * self)
{
	if(webkit_web_page_can_go_back(WEBKIT_IWEB_VIEW_GET_PAGE(self->priv->m_webviewActor)))
	{
		webkit_web_page_go_back(
				WEBKIT_IWEB_VIEW_GET_PAGE(self->priv->m_webviewActor));
		return TRUE;
	}
	return FALSE;
}

void rhayader_webbrowser_tab_go_fwd(RhayaderWebBrowserTab * self)
{
	webkit_web_page_go_forward(
				WEBKIT_IWEB_VIEW_GET_PAGE(self->priv->m_webviewActor));

}

void rhayader_webbrowser_tab_reload(RhayaderWebBrowserTab * self)
{
	g_signal_emit (self, signals[BUSY_STATE_CHANGED], 0, TRUE);
	webkit_web_page_reload(
				WEBKIT_IWEB_VIEW_GET_PAGE(self->priv->m_webviewActor));

}

void rhayader_webbrowser_tab_stop(RhayaderWebBrowserTab *self)
{
	if(self->priv->m_webviewActor)
	webkit_web_frame_stop_loading(webkit_web_page_get_main_frame(
			WEBKIT_IWEB_VIEW_GET_PAGE(self->priv->m_webviewActor)));
//	rhayader_webbrowser_tab_stop_audio(self);

}

void rhayader_webbrowser_tab_saveaspdf(RhayaderWebBrowserTab * self)
{
	 gchar* DownloadDir = (gchar *)g_build_filename ( g_get_home_dir(),DOCDIR , NULL);
	 gchar *title = NULL;
	 gchar *filename;

	 if(!g_file_test (DownloadDir, G_FILE_TEST_IS_DIR))
	 {
		 int err = g_mkdir_with_parents(DownloadDir, 0755);
		 if(err == -1)g_print("error = %s",strerror(errno));
	 }
	 g_object_get(self->priv->m_webview_widget,"title",&title,NULL);
	 filename = g_strdup_printf("%s/%s.pdf",DownloadDir,title);
	 g_print("\n saving as pdf to %s",filename);
	 lightwood_webview_widget_save_as_pdf(self->priv->m_webview_widget,filename);
}

void rhayader_webbrowser_tab_scrolltoTop(RhayaderWebBrowserTab * self)
{
	lightwood_webview_widget_scroll_to(self->priv->m_webview_widget,0,0);
}

void rhayader_webbrowser_tab_pause_audio(RhayaderWebBrowserTab *self)
{
	lightwood_webview_widget_pause_audio(self->priv->m_webview_widget);
}

void rhayader_webbrowser_tab_play_audio(RhayaderWebBrowserTab *self)
{
	lightwood_webview_widget_play_audio(self->priv->m_webview_widget);
}

void rhayader_webbrowser_tab_stop_audio(RhayaderWebBrowserTab *self)
{
	lightwood_webview_widget_stop_audio(self->priv->m_webview_widget);
}

void rhayader_webbrowser_tab_save(RhayaderWebBrowserTab *self)
{
	rhayader_webbrowser_tab_stop_audio(self);
}


static gboolean
draw_canvas (ClutterCanvas *canvas,
            cairo_t       *cr,
            int            width,
            int            height,gpointer data)
{

	cairo_surface_t *img_surface= (cairo_surface_t *)data;
	cairo_set_source_surface(cr, img_surface, 0,0);
	cairo_rectangle(cr,0,0,400,300);
	cairo_fill(cr);
	g_print("\n DRAW CANVAS OF WEBVIEW \n");

	 return TRUE;
}


static ClutterContent *rhayader_webbrowser_tab_get_content_from_cairo_surface(cairo_surface_t *surface)
{

	ClutterContent *canvas=NULL;
	canvas = clutter_canvas_new ();
	clutter_canvas_set_size (CLUTTER_CANVAS (canvas), 400,300);
	g_signal_connect (canvas, "draw", G_CALLBACK (draw_canvas), surface);
	clutter_content_invalidate (canvas);
	return canvas;
}

ClutterActor  *rhayader_webbrowser_tab_get_get_snapshot(RhayaderWebBrowserTab *self)
{
	ClutterActor *snapshot=clutter_actor_new();
	if(self->priv->m_webviewActor)
	{
		cairo_surface_t *img_surface;
		ClutterContent *content;

		g_print("\n CREATE A SNAPSHOT OF CURRENT WEBVIEW \n");
		img_surface = webkit_web_page_get_snapshot (
			WEBKIT_IWEB_VIEW_GET_PAGE(self->priv->m_webviewActor));
	g_object_set_data(G_OBJECT(self),"cairo-surface",(gpointer)img_surface);
	content = rhayader_webbrowser_tab_get_content_from_cairo_surface (img_surface);
	clutter_actor_set_content (snapshot, CLUTTER_CONTENT(content));
	g_object_unref(content);
	content=NULL;
	}
	return snapshot;
}


#ifndef WEBRUNTIME
static void _tab_free_data(gpointer data)
{
	if(data)
	{
		g_free(data);
		data=NULL;
	}
}
#endif

void rhayader_webbrowser_tab_get_tabs_backforwardlist(RhayaderWebBrowserTab *self,
		gint index)
{

	if (self->priv->m_webviewActor)
	{
		gint count = 0;
		WebKitWebHistoryItem *current_item;
		WebKitWebBackForwardList *backforwardlist =
				webkit_web_page_get_back_forward_list(
						WEBKIT_IWEB_VIEW_GET_PAGE(self->priv->m_webviewActor));
		GList *b_list =
				(GList *) webkit_web_back_forward_list_get_back_list_with_limit(
						backforwardlist,
						webkit_web_back_forward_list_get_back_length(
								backforwardlist));
		GList *f_list =
				(GList *) webkit_web_back_forward_list_get_forward_list_with_limit(
						backforwardlist,
						webkit_web_back_forward_list_get_forward_length(
								backforwardlist));

		GPtrArray *m_historylist = g_ptr_array_new();
		GList *plist;

#ifndef WEBRUNTIME
		if(NULL == self->priv->m_recenthistorylist)
		self->priv->m_recenthistorylist = g_hash_table_new_full(g_str_hash,g_str_equal,_tab_free_data,_tab_free_data);
#endif

		while (count
				< webkit_web_back_forward_list_get_back_length(backforwardlist))
		{
			WebKitWebHistoryItem *history_item =
					(WebKitWebHistoryItem *) g_list_nth_data(b_list, count);
			if (history_item)
			{
				WebKitWebHistoryItem *history_item_copy =
						(WebKitWebHistoryItem *) webkit_web_history_item_copy(
								history_item);
				gchar *uri;

				g_ptr_array_add(m_historylist, (gpointer) history_item_copy);

				uri = g_strdup (webkit_web_history_item_get_uri (history_item_copy));
			    if(g_strcmp0(uri,"about:blank"))
				{
                #ifndef WEBRUNTIME
				 gchar *title=g_strdup((gchar *)webkit_web_history_item_get_title(history_item_copy));
				g_hash_table_insert(self->priv->m_recenthistorylist,uri,title);
                #endif
				}

			}
			count++;
		}
		current_item = webkit_web_back_forward_list_get_current_item (
						backforwardlist);
		if (current_item)
		{
			WebKitWebHistoryItem *current_item_copy =
					(WebKitWebHistoryItem *) webkit_web_history_item_copy(
							current_item);
			gchar *uri;

			g_ptr_array_add(m_historylist, (gpointer) current_item_copy);
			uri = g_strdup (webkit_web_history_item_get_uri (current_item_copy));
	         if(g_strcmp0(uri,"about:blank"))
			{
             #ifndef WEBRUNTIME
			gchar *title=g_strdup((gchar *)webkit_web_history_item_get_title(current_item_copy));

				g_hash_table_insert(self->priv->m_recenthistorylist,uri,title);
               #endif
			}
		}

		count = 0;

		while (count
				< webkit_web_back_forward_list_get_forward_length(
						backforwardlist))
		{
			WebKitWebHistoryItem *history_item =
					(WebKitWebHistoryItem *) g_list_nth_data(f_list, count);
			if (history_item)
			{
				gchar *uri;
				WebKitWebHistoryItem *history_item_copy =
						(WebKitWebHistoryItem *) webkit_web_history_item_copy(
								history_item);
				g_ptr_array_add(m_historylist, (gpointer) history_item_copy);
				uri = g_strdup (webkit_web_history_item_get_uri (history_item_copy));
			    if(g_strcmp0(uri,"about:blank"))
			    {
                 #ifndef WEBRUNTIME
			    	gchar *title=g_strdup((gchar *)webkit_web_history_item_get_title(history_item_copy));
				  g_hash_table_insert(self->priv->m_recenthistorylist,uri,title);
                  #endif
			    }
			}
			count++;
		}

		plist = g_list_nth(self->priv->m_tabs_backforwardlist, index);
		if (plist && plist->data)
		{
			g_ptr_array_free(plist->data, TRUE);
			plist->data = NULL;
			self->priv->m_tabs_backforwardlist = g_list_remove_link(
					self->priv->m_tabs_backforwardlist, plist);
			plist = NULL;
			self->priv->m_tabs_backforwardlist = g_list_insert(
					self->priv->m_tabs_backforwardlist, m_historylist, index);

		}
		else
			self->priv->m_tabs_backforwardlist = g_list_append(
					self->priv->m_tabs_backforwardlist, m_historylist);
              #ifndef WEBRUNTIME
		           if(0 == g_list_length(self->priv->m_recentlist))
					self->priv->m_recentlist = g_list_append(
							self->priv->m_recentlist, self->priv->m_recenthistorylist);
             #endif

		webkit_web_back_forward_list_clear(backforwardlist);
	}

}

void rhayader_webbrowser_tab_set_tabs_backforwardlist(RhayaderWebBrowserTab *self,
		gint index)
{

	if (self->priv->m_webviewActor)
	{
		WebKitWebBackForwardList *backforwardlist =
				webkit_web_page_get_back_forward_list(
						WEBKIT_IWEB_VIEW_GET_PAGE(self->priv->m_webviewActor));
		GPtrArray *history_list;

		webkit_web_back_forward_list_clear(backforwardlist);
		history_list = g_list_nth_data (self->priv->m_tabs_backforwardlist, index);
		if (history_list)
		{
			gint count = 0;
			while (count < history_list->len)
			{
				WebKitWebHistoryItem *history_item =
						(WebKitWebHistoryItem *) g_ptr_array_index(history_list,
								count);
				webkit_web_back_forward_list_add_item(backforwardlist,
						history_item);
				count++;
			}
		}

	}
}

void rhayader_webbrowser_tab_clear_tabs_backforwardlist(RhayaderWebBrowserTab *self,
		gint index)
{
	if (self && self->priv->m_tabs_backforwardlist)
	{
		GList *rlist = g_list_nth(self->priv->m_tabs_backforwardlist, index);
		self->priv->m_tabs_backforwardlist = g_list_delete_link(
				self->priv->m_tabs_backforwardlist, rlist);
	}
}

void rhayader_webbrowser_tab_create_default_backforwardlist(RhayaderWebBrowserTab *self,
		gint rows)
{
	if (self)
	{
		gint count = 0;
		while (count < rows)
		{
			GPtrArray *m_historylist = g_ptr_array_new();
			self->priv->m_tabs_backforwardlist = g_list_append(
					self->priv->m_tabs_backforwardlist, m_historylist);
#ifndef WEBRUNTIME
			self->priv->m_recentlist = g_list_append(
								self->priv->m_recentlist, self->priv->m_recenthistorylist);
#endif
			count++;
		}
	}
}

void rhayader_webbrowser_tab_set_focus(RhayaderWebBrowserTab *self, gboolean is_focus)
{
	if (self && self->priv->m_webview_widget)
		lightwood_webview_widget_set_focus(self->priv->m_webview_widget, is_focus);
}


//---------------webview signal handlers------------------------
/*callback with the webpage title when a new page is loaded*/
static void _rhayader_webbrowser_tab_notify_title_cb(WebKitWebPage* web_view,
												GParamSpec* pspec, gpointer data)
{
	//ENTRY_TRACE
	RhayaderWebBrowserTab* self = RHAYADER_WEBBROWSER_TAB(data);
	//rhayader_webbrowser_idle_animation_stop();
	g_signal_emit(self, signals[TITLE_CHANGE], 0, webkit_web_page_get_title(web_view));

}

static void _rhayader_webbrowser_tab_notify_load_status_cb(WebKitWebPage* webPage,
		GParamSpec* pspec, gpointer data)
{
	RhayaderWebBrowserTab *self = RHAYADER_WEBBROWSER_TAB (data);
	WebKitLoadStatus load_status = webkit_web_page_get_load_status(webPage);
	if( (load_status == WEBKIT_LOAD_FINISHED) ||
		(load_status == WEBKIT_LOAD_FAILED))
	{
		g_signal_emit (self, signals[BUSY_STATE_CHANGED], 0, FALSE);
	}

	/*if ( webkit_web_page_get_load_status(webPage) == WEBKIT_LOAD_FINISHED)
	{

		//g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE,200,(GSourceFunc)_rhayader_webbrowser_tab_invalidate,self,NULL);
		 WebKitWebFrame* frame = webkit_web_page_get_main_frame(webPage);

		 WebKitNetworkResponse* response = webkit_web_frame_get_network_response(frame);
		 if(response == NULL) return;
		 SoupMessage* message = webkit_network_response_get_message(response);
		 if(message == NULL) return;
		 const char* mime_type =
		 soup_message_headers_get_content_type(message->response_headers,NULL);
		 g_print("\n mime_type=%s",mime_type);
		 if( g_strrstr(mime_type,"/xml") != NULL)
		 {
			 WebKitWebDataSource* src = webkit_web_frame_get_data_source(frame);
			 gchar* page_src = webkit_web_data_source_get_data(src)->str;
			// g_print("\n ------------PAGE SOURCES--------\n%.300s",page_src);
			 gboolean rss_detected = (g_strstr_len(page_src,500,"<rss") != NULL);
			 if(rss_detected)
			 {
				 g_printf("\n *********rss detected......*************");
				 gboolean is_podcast = (g_strstr_len(webkit_network_response_get_uri(response),100,"podcast") != NULL);
				g_signal_emit(self,signals[FEED_DETECTED],0,is_podcast);
			 }
		 }

	}*/


}

/*callback for url load progress.propogate the signal to let the browser view handle the notification*/
static void _rhayader_webbrowser_tab_notify_progress_cb(WebKitWebPage* web_view,
													GParamSpec* pspec, gpointer data)
{
	RhayaderWebBrowserTab* self = RHAYADER_WEBBROWSER_TAB(data);
	gdouble load_progress = webkit_web_page_get_progress(web_view) * 100;
	g_signal_emit(self, signals[URL_PROGRESS], 0, load_progress);

	/*if(load_progress == 100.0)
	{
		g_print("\n load complete..");
		rhayader_webbrowser_idle_animation_stop();
	}*/
}

static void _rhayader_webbrowser_tab_new_view_requested_cb(
		LightwoodWebViewWidget* web_view, gchar* url, RhayaderWebBrowserTab *self)
{
	//if(!self->priv->m_zoom_in_progress)
		g_signal_emit(self, signals[NEW_TAB_REQUESTED], 0, url);
}

static void _rhayader_webbrowser_tab_mime_alert_cb(LightwoodWebViewWidget* web_view,
		WebKitDownload* download, RhayaderWebBrowserTab *self)
{
	//gboolean is_save = g_strstr_len(webkit_download_get_uri(download),strlen(PDF_CONVERTER)+10,PDF_CONVERTER) != NULL;
	//gboolean is_save = g_strstr_len(webkit_download_get_uri(download),50,"pdfcrowd") != NULL;
	g_signal_emit(self, signals[MIME_DETECTED], 0,0, download);
}

/*static gboolean _rhayader_webbrowser_tab_double_tap_alert_cb(
		LightwoodWebViewWidget* web_view, ClutterButtonEvent* event,
		RhayaderWebBrowserTab *self)
{
		return _rhayader_webbrowser_tab_context_zoom_requested_cb(self, event);

}*/

static void
_rhayader_webbrowser_tab_rss_feed_alert_cb(LightwoodWebViewWidget* web_view,gboolean is_podcast,RhayaderWebBrowserTab *self)
{
	g_signal_emit(self,signals[FEED_DETECTED],0,is_podcast);
}

static void _rhayader_webbrowser_tab_save_image_requested_cb(
		LightwoodWebViewWidget* web_view, gchar* url, RhayaderWebBrowserTab *self)
{
	//LightwoodWebBrowserTabPrivate* priv = WEBBROWSER_TAB_PRIVATE (self);
	//WebKitWebPage* webPage = WEBLKIT_IWEB_VIEW_GET_PAGE(priv->m_webkitActor);
	//WebKitWebFrame* frame = webkit_web_page_get_main_frame(webPage);

	WebKitDownload * img_save = webkit_download_new (webkit_network_request_new(g_strdup(url)));
	g_object_set_data(G_OBJECT(img_save),"mime-type","image/*");
	g_signal_emit(self, signals[MIME_DETECTED], 0,1, img_save);

/*	WebKitWebDataSource* data_source = webkit_web_frame_get_data_source(web_frame);
    GList* data_src_list = webkit_web_data_source_get_subresources(data_source);
    g_assert(data_src_list);
    do
    {
    	 WebKitWebResource*  data_src = data_src_list->data;
    	if(g_str_equal(webkit_web_resource_get_uri(data_src),url))
    	{
    		GString* raw_data = webkit_web_resource_get_data(data_src);
    		//g_signal_emit(self, signals[SAVE_IMAGE_REQUESTED], 0, url);
    		break;
    	}

    }while(g_list_next(data_src_list));*/

}

static void _rhayader_webbrowser_tab_notify_zoom_level_changed_cb(LightwoodWebViewWidget* web_view,
													GParamSpec* pspec, RhayaderWebBrowserTab *self)
{
	g_print("\n !!!!!!!!!!!recived zoom level notificaton!!!!!!!!!!!");
	g_signal_emit(CLUTTER_ACTOR(self), signals[ZOOM_LEVEL_CHANGED], 0, lightwood_webview_widget_get_zoom_level(web_view));
}

static void
_rhayader_webbrowser_tab_notify_speller_state_cb(LightwoodWebViewWidget* web_view,gboolean is_shown,RhayaderWebBrowserTab *self)
{
   g_signal_emit(self,signals[WEBVIEW_SPELLER_ALERT],0,is_shown);
}

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_LISTVIEW_CONTROLLER_H
#define _RHAYADER_WEBBROWSER_LISTVIEW_CONTROLLER_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include "view/rhayader-list-view.h"
#include "libthornbury-ui-texture.h"

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_LISTVIEW_CONTROLLER rhayader_webbrowser_listview_controller_get_type()

#define RHAYADER_WEBBROWSER_LISTVIEW_CONTROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LISTVIEW_CONTROLLER, RhayaderWebBrowserListViewController))

#define RHAYADER_WEBBROWSER_LISTVIEW_CONTROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_LISTVIEW_CONTROLLER, RhayaderWebBrowserListViewControllerClass))

#define RHAYADER_IS_WEBBROWSER_LISTVIEW_CONTROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LISTVIEW_CONTROLLER))

#define RHAYADER_IS_WEBBROWSER_LISTVIEW_CONTROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_LISTVIEW_CONTROLLER))

#define RHAYADER_WEBBROWSER_LISTVIEW_CONTROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LISTVIEW_CONTROLLER, RhayaderWebBrowserListViewControllerClass))

typedef struct _RhayaderWebBrowserListViewController RhayaderWebBrowserListViewController;
typedef struct _RhayaderWebBrowserListViewControllerClass RhayaderWebBrowserListViewControllerClass;
typedef struct _RhayaderWebBrowserListViewControllerPrivate RhayaderWebBrowserListViewControllerPrivate;

struct _RhayaderWebBrowserListViewController
{
  GObject parent;
  RhayaderWebBrowserListView*  m_listView;
  RhayaderWebBrowserListViewControllerPrivate *priv;
};

struct _RhayaderWebBrowserListViewControllerClass
{
  GObjectClass parent_class;
};

GType rhayader_webbrowser_listview_controller_get_type (void) G_GNUC_CONST;

RhayaderWebBrowserListViewController *rhayader_webbrowser_listview_controller_new (ClutterActor* listView);
void rhayader_webbrowser_listview_controller_new_tab (RhayaderWebBrowserListViewController *self,
    const gchar *url);
//void rhayader_webbrowser_listview_controller_save(RhayaderWebBrowserListViewController * self);

G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_LISTVIEW_CONTROLLER_H */

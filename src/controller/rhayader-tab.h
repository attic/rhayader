/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_TAB_H
#define _RHAYADER_WEBBROWSER_TAB_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_TAB rhayader_webbrowser_tab_get_type()

#define RHAYADER_WEBBROWSER_TAB(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_TAB, RhayaderWebBrowserTab))

#define RHAYADER_WEBBROWSER_TAB_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_TAB, RhayaderWebBrowserTabClass))

#define RHAYADER_IS_WEBBROWSER_TAB(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_TAB))

#define RHAYADER_IS_WEBBROWSER_TAB_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_TAB))

#define RHAYADER_WEBBROWSER_TAB_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_TAB, RhayaderWebBrowserTabClass))

typedef struct _RhayaderWebBrowserTab RhayaderWebBrowserTab;
typedef struct _RhayaderWebBrowserTabClass RhayaderWebBrowserTabClass;
typedef struct _RhayaderWebBrowserTabPrivate RhayaderWebBrowserTabPrivate;

struct _RhayaderWebBrowserTab
{
  GObject parent;

  RhayaderWebBrowserTabPrivate *priv;
};

struct _RhayaderWebBrowserTabClass
{
  GObjectClass parent_class;
};

GType rhayader_webbrowser_tab_get_type (void) G_GNUC_CONST;

RhayaderWebBrowserTab *rhayader_webbrowser_tab_new (gboolean is_standalone);
void rhayader_webbrowser_tab_initialize(RhayaderWebBrowserTab * self,ClutterActor* web_view);
void rhayader_webbrowser_tab_uninitialize(RhayaderWebBrowserTab * self);

void rhayader_webbrowser_tab_load_url(RhayaderWebBrowserTab *self,
    const gchar *url);
gchar* rhayader_webbrowser_tab_get_url(RhayaderWebBrowserTab *self);
gchar* rhayader_webbrowser_tab_get_title(RhayaderWebBrowserTab *self);
void rhayader_webbrowser_tab_saveaspdf(RhayaderWebBrowserTab * self);

gboolean rhayader_webbrowser_tab_go_back(RhayaderWebBrowserTab * self);
void rhayader_webbrowser_tab_go_fwd(RhayaderWebBrowserTab * self);
void rhayader_webbrowser_tab_stop(RhayaderWebBrowserTab * self);
void rhayader_webbrowser_tab_reload(RhayaderWebBrowserTab * self);
void rhayader_webbrowser_tab_scrolltoTop(RhayaderWebBrowserTab * self);

void rhayader_webbrowser_tab_pause_audio(RhayaderWebBrowserTab *self);
void rhayader_webbrowser_tab_play_audio(RhayaderWebBrowserTab *self);
void rhayader_webbrowser_tab_stop_audio(RhayaderWebBrowserTab *self);

void rhayader_webbrowser_tab_save(RhayaderWebBrowserTab *self);

ClutterActor *rhayader_webbrowser_tab_get_get_snapshot(RhayaderWebBrowserTab *self);
void rhayader_webbrowser_tab_get_tabs_backforwardlist(RhayaderWebBrowserTab *self,
		gint index);
void rhayader_webbrowser_tab_set_tabs_backforwardlist(RhayaderWebBrowserTab *self,
		gint index);
void rhayader_webbrowser_tab_clear_tabs_backforwardlist(RhayaderWebBrowserTab *self,
		gint index);
void rhayader_webbrowser_tab_create_default_backforwardlist(RhayaderWebBrowserTab *self,
		gint rows);
void rhayader_webbrowser_tab_set_focus(RhayaderWebBrowserTab *self, gboolean is_focus);

G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_TAB_H */

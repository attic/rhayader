/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_NORMALVIEW_CONTROLLER_H
#define _RHAYADER_WEBBROWSER_NORMALVIEW_CONTROLLER_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include "rhayader-tab.h"
#include "view/rhayader-normal-view.h"


G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_NORMALVIEW_CONTROLLER rhayader_webbrowser_normalview_controller_get_type()

#define RHAYADER_WEBBROWSER_NORMALVIEW_CONTROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_NORMALVIEW_CONTROLLER, RhayaderWebBrowserNormalViewController))

#define RHAYADER_WEBBROWSER_NORMALVIEW_CONTROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_NORMALVIEW_CONTROLLER, RhayaderWebBrowserNormalViewControllerClass))

#define RHAYADER_IS_WEBBROWSER_NORMALVIEW_CONTROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_NORMALVIEW_CONTROLLER))

#define RHAYADER_IS_WEBBROWSER_NORMALVIEW_CONTROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_NORMALVIEW_CONTROLLER))

#define RHAYADER_WEBBROWSER_NORMALVIEW_CONTROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_NORMALVIEW_CONTROLLER, RhayaderWebBrowserNormalViewControllerClass))

typedef struct _RhayaderWebBrowserNormalViewController RhayaderWebBrowserNormalViewController;
typedef struct _RhayaderWebBrowserNormalViewControllerClass RhayaderWebBrowserNormalViewControllerClass;
typedef struct _RhayaderWebBrowserNormalViewControllerPrivate RhayaderWebBrowserNormalViewControllerPrivate;

struct _RhayaderWebBrowserNormalViewController
{
  GObject parent;

  RhayaderWebBrowserTab*		m_tab;
  RhayaderWebBrowserNormalViewControllerPrivate *priv;
};

struct _RhayaderWebBrowserNormalViewControllerClass
{
  GObjectClass parent_class;
};

GType rhayader_webbrowser_normalview_controller_get_type (void) G_GNUC_CONST;

RhayaderWebBrowserNormalViewController *rhayader_webbrowser_normalview_controller_new (ClutterActor* normalView,gboolean is_standalone);
void rhayader_webbrowser_normalview_controller_load_url(RhayaderWebBrowserNormalViewController * self,gchar* request_url);
void rhayader_webbrowser_normalview_controller_hide_view(RhayaderWebBrowserNormalViewController * self);
void rhayader_webbrowser_normalview_controller_scrolltoTop(RhayaderWebBrowserNormalViewController * self);
void rhayader_webbrowser_normalview_controller_go_fullscreen(RhayaderWebBrowserNormalViewController * self,gboolean isFullscreen);
gboolean rhayader_webbrowser_normalview_controller_is_fullscreen(RhayaderWebBrowserNormalViewController * self);
gboolean rhayader_webbrowser_normalview_controller_go_back(RhayaderWebBrowserNormalViewController * self);
//void rhayader_webbrowser_normalview_controller_save(RhayaderWebBrowserNormalViewController * self);
G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_NORMALVIEW_CONTROLLER_H */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-listview-controller.h"
#include "view/rhayader-list-view.h"
#include "model/rhayader-list-model.h"
#include "mildenhall/mildenhall_webview_widget.h"



#define DEFAULT_HOMEPAGE 	"http://www.google.com"

G_DEFINE_TYPE (RhayaderWebBrowserListViewController, rhayader_webbrowser_listview_controller, G_TYPE_OBJECT)

#define WEBBROWSER_LISTVIEW_CONTROLLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_LISTVIEW_CONTROLLER, RhayaderWebBrowserListViewControllerPrivate))

struct _RhayaderWebBrowserListViewControllerPrivate
{
	ClutterActor* m_viewContainer;

	RhayaderWebBrowserListModel* m_listModel;
//	gchar* m_request_url;

};


//private methods
static void
_rhayader_webbrowser_listview_controller_action_requested_cb(ClutterActor *normal_view,eTabsMenuOption action,RhayaderWebBrowserListViewController * self);
static ClutterActor *rhayader_webbrowser_listview_controller_create_tab (void);
//static void
//_rhayader_webbrowser_listview_controller_transition_completed_cb(RhayaderWebBrowserListView *listview,gboolean is_shown,RhayaderWebBrowserListViewController *self);
static gboolean _rhayader_webbrowser_listview_controller_new_tab_cb(RhayaderWebBrowserListViewController *self);
static void _rhayader_webbrowser_listview_controller_add_tab (RhayaderWebBrowserListViewController *self, const gchar *request_url);
static void
rhayader_webbrowser_listview_controller_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_listview_controller_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_listview_controller_class_init (RhayaderWebBrowserListViewControllerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserListViewControllerPrivate));

  object_class->dispose = rhayader_webbrowser_listview_controller_dispose;

}

static void
rhayader_webbrowser_listview_controller_init (RhayaderWebBrowserListViewController *self)
{
  self->priv = WEBBROWSER_LISTVIEW_CONTROLLER_PRIVATE (self);

}


static void
_rhayader_webbrowser_listview_controller_initialize (RhayaderWebBrowserListViewController *self,ClutterActor* listView)
{
	RhayaderWebBrowserListViewControllerPrivate* priv = WEBBROWSER_LISTVIEW_CONTROLLER_PRIVATE (self);
	//priv->m_request_url = NULL;
	priv->m_viewContainer = clutter_actor_get_parent(listView);
	self->m_listView =RHAYADER_WEBBROWSER_LIST_VIEW(listView);

	 /* g_signal_connect_after (G_OBJECT(self->m_listView),
						"action_requested",
						G_CALLBACK (_rhayader_webbrowser_listview_controller_action_requested_cb),
						self);*/
	  //g_signal_connect_after(self->m_listView, "transition_completed",
	    //	               G_CALLBACK(_rhayader_webbrowser_listview_controller_transition_completed_cb),self);

	priv->m_listModel = (RhayaderWebBrowserListModel*)rhayader_webbrowser_list_view_get_model(self->m_listView);
	rhayader_webbrowser_list_model_restore_last_session(priv->m_listModel,rhayader_webbrowser_listview_controller_create_tab);

}

RhayaderWebBrowserListViewController *
rhayader_webbrowser_listview_controller_new (ClutterActor* listView)
{
	RhayaderWebBrowserListViewController * self =  g_object_new (RHAYADER_TYPE_WEBBROWSER_LISTVIEW_CONTROLLER, NULL);
	_rhayader_webbrowser_listview_controller_initialize(self,listView);

	return self;
}


void
rhayader_webbrowser_listview_controller_new_tab (RhayaderWebBrowserListViewController *self,
    const gchar *request_url)
{
	//RhayaderWebBrowserListViewControllerPrivate* priv = WEBBROWSER_LISTVIEW_CONTROLLER_PRIVATE (self);

	g_assert( rhayader_webbrowser_list_view_is_shown(self->m_listView));
		_rhayader_webbrowser_listview_controller_add_tab(self,request_url);
	//else
		//priv->m_request_url = g_strdup(request_url);
}


//void rhayader_webbrowser_listview_controller_save(RhayaderWebBrowserListViewController * self)
//{
//	rhayader_webbrowser_list_model_save(self->priv->m_listModel);
//}


static ClutterActor *
rhayader_webbrowser_listview_controller_create_tab (void)
{
	/*ClutterActor* webview = rhayader_webview_widget_new(); ROHAN
	clutter_actor_show(webview);
	g_object_set(webview,
				"opacity",0xFF,
				"x-expand",TRUE,
				"y-expand",TRUE,
				"x-align",CLUTTER_ACTOR_ALIGN_FILL,
				"y-align",CLUTTER_ACTOR_ALIGN_FILL,
				NULL);*/
	ClutterActor* texture = thornbury_ui_texture_create_new(ICONDIR"/default_tab.png",0,0,FALSE,FALSE);
	return texture;
}

static gboolean
_rhayader_webbrowser_listview_controller_set_new_tab_to_focus (gpointer data)
{
	RhayaderWebBrowserListViewController *self = data;
	RhayaderWebBrowserListViewControllerPrivate* priv = WEBBROWSER_LISTVIEW_CONTROLLER_PRIVATE (self);
	rhayader_webbrowser_list_view_set_focused_row (self->m_listView,
		thornbury_model_get_n_rows (THORNBURY_MODEL (priv->m_listModel)) - 1,
		TRUE);
	return G_SOURCE_REMOVE;
}

static void
_rhayader_webbrowser_listview_controller_add_tab (RhayaderWebBrowserListViewController *self,
    const gchar *request_url)
{
	RhayaderWebBrowserListViewControllerPrivate* priv = WEBBROWSER_LISTVIEW_CONTROLLER_PRIVATE (self);

	ClutterActor* webview = rhayader_webbrowser_listview_controller_create_tab();
	if(request_url)
		g_object_set_data(G_OBJECT(webview),"request_url",g_strdup(request_url));
	rhayader_webbrowser_list_model_add_entry(priv->m_listModel,webview,rhayader_webbrowser_list_view_get_focused_row(self->m_listView)+1);
 g_timeout_add(250,_rhayader_webbrowser_listview_controller_set_new_tab_to_focus,self);



}

//signal handlers
static gboolean _rhayader_webbrowser_listview_controller_new_tab_cb(RhayaderWebBrowserListViewController *self)
{
//	RhayaderWebBrowserListViewControllerPrivate* priv = WEBBROWSER_LISTVIEW_CONTROLLER_PRIVATE (self);
	_rhayader_webbrowser_listview_controller_add_tab(self,NULL);
	return G_SOURCE_REMOVE;
}

/*
static void _rhayader_webbrowser_listview_controller_action_requested_cb(ClutterActor *list_view,eTabsMenuOption action,RhayaderWebBrowserListViewController * self)
{
	RhayaderWebBrowserListViewControllerPrivate* priv = WEBBROWSER_LISTVIEW_CONTROLLER_PRIVATE (self);
	switch(action)
	{
		case NEWVIEW:
                        _rhayader_webbrowser_listview_controller_new_tab_cb(self);
		       //g_idle_add((GSourceFunc)_rhayader_webbrowser_listview_controller_new_tab_cb,self);
			break;

		case CLOSEVIEW:
		{
			if( thornbury_model_get_n_rows(THORNBURY_MODEL(priv->m_listModel)) > 1)
			{
				gint row = rhayader_webbrowser_list_view_get_focused_row(self->m_listView);
				rhayader_webbrowser_list_model_remove_entry(priv->m_listModel,row);
			}
		}
			break;

		default:
			g_assert(0);
			break;
	}
}*/


/*static void
_rhayader_webbrowser_listview_controller_transition_completed_cb(RhayaderWebBrowserListView *listview,gboolean is_shown,RhayaderWebBrowserListViewController *self)

{
	RhayaderWebBrowserListViewControllerPrivate* priv = WEBBROWSER_LISTVIEW_CONTROLLER_PRIVATE (self);
	if(is_shown)
	{
		if(priv->m_request_url)
		{
			g_print("\n loading url %s",priv->m_request_url);
			g_timeout_add(1000,(GSourceFunc)_rhayader_webbrowser_listview_controller_new_tab_cb,self);
			//rhayader_webbrowser_listview_controller_new_tab(priv->m_listview_controller);
		}
	}
}*/






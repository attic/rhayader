/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-normalview-controller.h"
#include "view/rhayader-normal-view.h"

G_DEFINE_TYPE (RhayaderWebBrowserNormalViewController, rhayader_webbrowser_normalview_controller, G_TYPE_OBJECT)

#define WEBBROWSER_NORMALVIEW_CONTROLLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_NORMALVIEW_CONTROLLER, RhayaderWebBrowserNormalViewControllerPrivate))

struct _RhayaderWebBrowserNormalViewControllerPrivate
{
	ClutterActor* 		    m_normalView;
	RhayaderWebbrowserSpeller* 	m_url_speller;

};


//private methods
#ifndef WEBRUNTIME
static void
_rhayader_webbrowser_normalview_controller_urlrequest_cb(ClutterActor* urlspeller,gchar* request_url,RhayaderWebBrowserNormalViewController * self);
static void
_rhayader_webbrowser_normalview_controller_titlechange_cb(RhayaderWebBrowserTab* tab,gchar* title,RhayaderWebBrowserNormalViewController * self);
static void
_rhayader_webbrowser_normalview_controller_urlprogress_cb(RhayaderWebBrowserTab* tab,gdouble load_percent,RhayaderWebBrowserNormalViewController * self);
static void
_rhayader_webbrowser_normalview_controller_zoomlevel_change_cb(RhayaderWebBrowserTab* tab,gdouble zoom_level,RhayaderWebBrowserNormalViewController * self);

static void
_rhayader_webbrowser_normalview_controller_view_transition_started_cb(ClutterActor *normal_view,gboolean is_shown,RhayaderWebBrowserNormalViewController * self);
static void
_rhayader_webbrowser_normalview_controller_view_transition_completed_cb(ClutterActor *normal_view,gboolean is_shown,RhayaderWebBrowserNormalViewController * self);
static void
_rhayader_webbrowser_normalview_controller_view_fullscreen_transition_completed_cb(ClutterActor *normal_view,gboolean is_shown,RhayaderWebBrowserNormalViewController * self);
static void
_rhayader_webbrowser_normalview_controller_action_requested_cb(ClutterActor *normal_view,eContextMenuOption action,RhayaderWebBrowserNormalViewController * self);
static void
_rhayader_webbrowser_normalview_controller_pause_requested_cb(ClutterActor* urlspeller,RhayaderWebBrowserNormalViewController * self);
static void
_rhayader_webbrowser_normalview_controller_scrolltotop_requested_cb(ClutterActor* urlspeller,RhayaderWebBrowserNormalViewController * self);
static void
_rhayader_webbrowser_normalview_controller_speller_alert_cb(RhayaderWebBrowserTab* tab,gboolean is_shown,RhayaderWebBrowserNormalViewController * self);
#endif

static void
rhayader_webbrowser_normalview_controller_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_normalview_controller_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_normalview_controller_class_init (RhayaderWebBrowserNormalViewControllerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserNormalViewControllerPrivate));

  object_class->dispose = rhayader_webbrowser_normalview_controller_dispose;

}

static void
rhayader_webbrowser_normalview_controller_init (RhayaderWebBrowserNormalViewController *self)
{
	self->priv = WEBBROWSER_NORMALVIEW_CONTROLLER_PRIVATE (self);

}

static void
_rhayader_webbrowser_normalview_controller_initialize (RhayaderWebBrowserNormalViewController *self, ClutterActor* normalView,gboolean is_standalone)
{
	RhayaderWebBrowserNormalViewControllerPrivate* priv = WEBBROWSER_NORMALVIEW_CONTROLLER_PRIVATE (self);
	priv->m_normalView = normalView;

#ifndef  WEBRUNTIME
	  g_signal_connect (G_OBJECT(priv->m_normalView),
						"transition_started",
						G_CALLBACK (_rhayader_webbrowser_normalview_controller_view_transition_started_cb),
						self);
	  g_signal_connect (G_OBJECT(priv->m_normalView),
						"transition_completed",
						G_CALLBACK (_rhayader_webbrowser_normalview_controller_view_transition_completed_cb),
						self);

	  g_signal_connect (G_OBJECT(priv->m_normalView),
						"fullscreen_transition_completed",
						G_CALLBACK (_rhayader_webbrowser_normalview_controller_view_fullscreen_transition_completed_cb),
						self);
	  g_signal_connect (G_OBJECT(priv->m_normalView),
						"action_requested",
						G_CALLBACK (_rhayader_webbrowser_normalview_controller_action_requested_cb),
						self);

	  priv->m_url_speller = rhayader_webbrowser_normal_view_get_url_speller(RHAYADER_WEBBROWSER_NORMAL_VIEW(normalView));
	  g_assert(CLUTTER_IS_ACTOR(priv->m_url_speller));
	  g_signal_connect(priv->m_url_speller, "webbrowser_speller_data_entered",
	                                  G_CALLBACK(_rhayader_webbrowser_normalview_controller_urlrequest_cb), self);
	  g_signal_connect(priv->m_url_speller, "webbrowser_speller_pause",
	  	                                  G_CALLBACK(_rhayader_webbrowser_normalview_controller_pause_requested_cb), self);
	  g_signal_connect(priv->m_url_speller, "webbrowser_speller_scrolltotop",
	  	                                  G_CALLBACK(_rhayader_webbrowser_normalview_controller_scrolltotop_requested_cb), self);
#endif
	self->m_tab = rhayader_webbrowser_tab_new(is_standalone);
#ifdef WEBRUNTIME
	rhayader_webbrowser_tab_initialize(self->m_tab,rhayader_webbrowser_normal_view_get_tab(RHAYADER_WEBBROWSER_NORMAL_VIEW(priv->m_normalView)));
#else
    g_signal_connect(self->m_tab, "zoom_level_changed",
                                     G_CALLBACK(_rhayader_webbrowser_normalview_controller_zoomlevel_change_cb), self);
    g_signal_connect(self->m_tab, "title_change",
                                G_CALLBACK(_rhayader_webbrowser_normalview_controller_titlechange_cb), self);
    g_signal_connect(self->m_tab, "url_progress",
                                G_CALLBACK(_rhayader_webbrowser_normalview_controller_urlprogress_cb), self);
    g_signal_connect_after(self->m_tab, "webview_speller_alert",
                                    G_CALLBACK(_rhayader_webbrowser_normalview_controller_speller_alert_cb), self);

#endif
  }


RhayaderWebBrowserNormalViewController *
rhayader_webbrowser_normalview_controller_new (ClutterActor* normalView,gboolean is_standalone)
{
	RhayaderWebBrowserNormalViewController * self =  g_object_new (RHAYADER_TYPE_WEBBROWSER_NORMALVIEW_CONTROLLER, NULL);
	_rhayader_webbrowser_normalview_controller_initialize(self,normalView,is_standalone);
	return self;
}

/*
void rhayader_webbrowser_normalview_controller_save(RhayaderWebBrowserNormalViewController * self)
{
	rhayader_webbrowser_tab_save(self->m_tab);
}
*/

gboolean rhayader_webbrowser_normalview_controller_go_back(RhayaderWebBrowserNormalViewController * self)
{
	return rhayader_webbrowser_tab_go_back(self->m_tab);
}

void rhayader_webbrowser_normalview_controller_load_url(RhayaderWebBrowserNormalViewController * self,gchar* request_url)
{
//	RhayaderWebBrowserNormalViewControllerPrivate* priv = WEBBROWSER_NORMALVIEW_CONTROLLER_PRIVATE (self);
	g_assert(request_url);
//	g_assert(rhayader_webbrowser_normal_view_is_shown(RHAYADER_WEBBROWSER_NORMAL_VIEW(priv->m_normalView)));
#ifndef WEBRUNTIME
	rhayader_webbrowser_speller_set_url(self->priv->m_url_speller,(char*)request_url);
	 rhayader_webbrowser_speller_set_title(self->priv->m_url_speller,"Waiting");
#endif
	rhayader_webbrowser_tab_load_url(self->m_tab,request_url);
}

void rhayader_webbrowser_normalview_controller_scrolltoTop(RhayaderWebBrowserNormalViewController * self)
{
	rhayader_webbrowser_tab_scrolltoTop(self->m_tab);
}

#ifndef WEBRUNTIME
static void _rhayader_webbrowser_normalview_controller_view_transition_started_cb(ClutterActor *normal_view,gboolean is_shown,RhayaderWebBrowserNormalViewController * self)
{
	g_print("\n ######_rhayader_webbrowser_normalview_controller_tab_transition_completed_cb");

	if(!is_shown)
		rhayader_webbrowser_tab_stop(self->m_tab);
}

static void _rhayader_webbrowser_normalview_controller_view_transition_completed_cb(ClutterActor *normal_view,gboolean is_shown,RhayaderWebBrowserNormalViewController * self)
{

	RhayaderWebBrowserNormalViewControllerPrivate *priv = WEBBROWSER_NORMALVIEW_CONTROLLER_PRIVATE (self);

	g_print("\n ######_rhayader_webbrowser_normalview_controller_tab_transition_completed_cb");
	if(is_shown)
	{
		ClutterActor* web_view = rhayader_webbrowser_normal_view_get_tab(RHAYADER_WEBBROWSER_NORMAL_VIEW(normal_view));
		gchar* request_url;

		rhayader_webbrowser_tab_initialize(self->m_tab,web_view);
		//gchar* request_url = g_object_get_data(G_OBJECT(web_view),"request_url");
		request_url = g_object_get_data (G_OBJECT (self->m_tab), "request_url");

		g_object_steal_data(G_OBJECT(web_view),"title");
		if(request_url){
			gchar* url=NULL;
			 g_object_get(web_view,"url",&url,NULL);
			       if(g_strcmp0(url,request_url))
                   rhayader_webbrowser_normalview_controller_load_url(self,request_url);
						if(!g_strcmp0(request_url,"about:blank"))
						rhayader_webbrowser_speller_show(priv->m_url_speller);
		}
		else
		{
			gchar* url=NULL,*title=NULL;
			g_object_get(web_view,"url",&url,"title",&title,NULL);
			if(!url)
				rhayader_webbrowser_speller_show(priv->m_url_speller);

		}
	}
}

static void _rhayader_webbrowser_normalview_controller_view_fullscreen_transition_completed_cb(ClutterActor *normal_view,gboolean is_shown,RhayaderWebBrowserNormalViewController * self)
{
	g_print("\n ######_rhayader_webbrowser_normalview_controller_view_fullscreen_transition_completed_cb");

}




static void _rhayader_webbrowser_normalview_controller_action_requested_cb(
				ClutterActor *normal_view,eContextMenuOption action,RhayaderWebBrowserNormalViewController * self)
{
	//RhayaderWebBrowserNormalViewControllerPrivate* priv = WEBBROWSER_NORMALVIEW_CONTROLLER_PRIVATE (self);
	switch(action)
	{
		case BACK:
			rhayader_webbrowser_tab_go_back(self->m_tab);
			break;

		case FORWARD:
			rhayader_webbrowser_tab_go_fwd(self->m_tab);
			break;

		case RELOAD:
			rhayader_webbrowser_tab_reload(self->m_tab);
			break;

		case SAVEASPDF:
			rhayader_webbrowser_tab_saveaspdf(self->m_tab);
			break;

		default:
			g_assert(0);
			break;
	}
}

static void
_rhayader_webbrowser_normalview_controller_zoomlevel_change_cb(RhayaderWebBrowserTab* tab,gdouble zoom_level,RhayaderWebBrowserNormalViewController * self)
{

	//clutter_text_set_text(self->priv->m_locationText,rhayader_webbrowser_tab_get_url(tab));
	rhayader_webbrowser_normal_view_refresh(RHAYADER_WEBBROWSER_NORMAL_VIEW(self->priv->m_normalView));
}

static void
_rhayader_webbrowser_normalview_controller_urlrequest_cb(ClutterActor* urlspeller,gchar* request_url,RhayaderWebBrowserNormalViewController * self)
{
	//RhayaderWebBrowserNormalViewControllerPrivate* priv = WEBBROWSER_NORMALVIEW_CONTROLLER_PRIVATE (self);
	g_object_set_data(G_OBJECT(self->m_tab),"request_url",g_strdup(request_url));
 	rhayader_webbrowser_tab_load_url(self->m_tab,request_url);
}

static void
_rhayader_webbrowser_normalview_controller_pause_requested_cb(ClutterActor* urlspeller,RhayaderWebBrowserNormalViewController * self)
{
	//RhayaderWebBrowserNormalViewControllerPrivate* priv = WEBBROWSER_NORMALVIEW_CONTROLLER_PRIVATE (self);
	rhayader_webbrowser_tab_stop(self->m_tab);
}

static void
_rhayader_webbrowser_normalview_controller_scrolltotop_requested_cb(ClutterActor* urlspeller,RhayaderWebBrowserNormalViewController * self)
{
	//RhayaderWebBrowserNormalViewControllerPrivate* priv = WEBBROWSER_NORMALVIEW_CONTROLLER_PRIVATE (self);
	rhayader_webbrowser_tab_scrolltoTop(self->m_tab);
}

static void
_rhayader_webbrowser_normalview_controller_titlechange_cb(RhayaderWebBrowserTab* tab,gchar* title,RhayaderWebBrowserNormalViewController * self)
{
	g_object_set_data(G_OBJECT(self->m_tab),"title",g_strdup(title));
	rhayader_webbrowser_speller_set_url(self->priv->m_url_speller,rhayader_webbrowser_tab_get_url(tab));
	rhayader_webbrowser_speller_set_title(self->priv->m_url_speller,title);
}

static void
_rhayader_webbrowser_normalview_controller_urlprogress_cb(RhayaderWebBrowserTab* tab,gdouble load_percent,RhayaderWebBrowserNormalViewController * self)
{
	//rhayader_webbrowser_speller_set_url(self->priv->m_url_speller,rhayader_webbrowser_tab_get_url(tab));
	rhayader_webbrowser_speller_set_progress(self->priv->m_url_speller,load_percent);
}



static void
_rhayader_webbrowser_normalview_controller_speller_alert_cb(RhayaderWebBrowserTab* tab,gboolean is_shown,RhayaderWebBrowserNormalViewController * self)
{

	rhayader_webbrowser_normal_view_set_bottom_area_visible(RHAYADER_WEBBROWSER_NORMAL_VIEW(self->priv->m_normalView),!is_shown);
}
#endif

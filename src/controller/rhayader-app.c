/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-app.h"
#include "view/rhayader-window.h"
#include "rhayader-normalview-controller.h"
#include "rhayader-tab.h"
#include <mildenhall/mildenhall_ui_utility.h>
#include <canterbury.h>
#include <canterbury_app_handler.h>
#include <mildenhall/mildenhall_webview_widget.h>
#ifndef WEBRUNTIME
#include "rhayader-listview-controller.h"
#include "model/rhayader-list-model.h"
#endif

#include "messages-internal.h"

#define OFFLINE_HOMEPAGE 	g_filename_to_uri(HOMEPAGEDIR "/index.htm",NULL,NULL)
//#define OFFLINE_HOMEPAGE 	"file:///home/rhayader/jsconfirm.html"
//#define OFFLINE_HOMEPAGE 	"file:///home/rhayader/jalert.htm"
//#define OFFLINE_HOMEPAGE 	"file:///home/rhayader/htmlselect.html"
#define DEFAULT_HOMEPAGE 	"http://www.google.com"


G_DEFINE_TYPE (RhayaderWebBrowser, rhayader_webbrowser, G_TYPE_OBJECT)

#define WEBBROWSER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER, RhayaderWebBrowserPrivate))

struct _RhayaderWebBrowserPrivate
{
	gboolean								m_isStandalone;
	gfloat									m_toolbarWidth;
	MxWindow*								m_browser_window;
#ifndef WEBRUNTIME
	RhayaderWebBrowserListViewController* 		m_listview_controller;
	CanterburyMutterPlugin *mutter_proxy;
	gint cur_row;
#endif
	RhayaderWebBrowserNormalViewController*		m_normalview_controller;

	ClutterActor* 			m_curTab;



	gchar* 					m_homePage;
	gboolean				m_conn_status;

	gchar* m_request_url;
	GNetworkMonitor *network_monitor;

};

//private methods
#ifndef WEBRUNTIME
static void _rhayader_webbrowser_new_tab_requested_cb(RhayaderWebBrowserTab* tab,gchar* request_url,RhayaderWebBrowser *self);
static void _rhayader_webbrowser_tab_activated_cb(RhayaderWebBrowserListView* listView,ClutterActor *tab,RhayaderWebBrowser *self);
static void _rhayader_webbrowser_window_action_requested_cb(RhayaderWebbrowserWindow *window,eWindowOption action,RhayaderWebBrowser *self);
static void _rhayader_webbrowser_listview_transition_completed_cb(RhayaderWebBrowserListView *listview,gboolean is_shown,RhayaderWebBrowser *self);
#endif
#ifdef WEBRUNTIME
static gboolean _rhayader_webbrowser_app_load_webapp_url(RhayaderWebBrowser *self);
#endif

static void rhayader_webbrowser_dispose(GObject *object)
{

	RhayaderWebBrowser *self = RHAYADER_WEBBROWSER (object);
	RhayaderWebBrowserPrivate *priv = WEBBROWSER_PRIVATE (self);

	g_clear_object (&priv->network_monitor);

	G_OBJECT_CLASS (rhayader_webbrowser_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_class_init (RhayaderWebBrowserClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserPrivate));

  object_class->dispose = rhayader_webbrowser_dispose;
}

static void
rhayader_webbrowser_check_network_connection (RhayaderWebBrowser *self)
{
  RhayaderWebBrowserPrivate *priv = WEBBROWSER_PRIVATE (self);
  GNetworkConnectivity state = g_network_monitor_get_connectivity (priv->network_monitor);

  rhayader_webbrowser_set_connection_status (self, (state == G_NETWORK_CONNECTIVITY_FULL));
}

static void
rhayader_webbrowser_network_changed_cb (GNetworkMonitor *monitor,
                                        GParamSpec *pspec,
                                        gpointer user_data)
{
  rhayader_webbrowser_check_network_connection (user_data);
}

static void rhayader_webbrowser_init(RhayaderWebBrowser *self)
{
	RhayaderWebBrowserPrivate* priv = self->priv = WEBBROWSER_PRIVATE (self);
	//ClutterColor    black = { 0, 0, 0, 255 };

	if (clutter_get_accessibility_enabled())
	{
		char* bridgePath = g_module_build_path("/usr/lib/gtk-2.0/modules", "libatk-bridge");
		GModule* module = g_module_open(bridgePath, G_MODULE_BIND_LAZY);

		void (*symbol) (void);
		if (module != NULL && g_module_symbol(module, "gnome_accessibility_module_init", (gpointer*)&symbol))
			symbol();
		else
			g_warning("Failed to initialize atk-bridge.");
	}
//	rhayader_webview_widget_init_session("WebBrowser");
	priv->m_browser_window = rhayader_webbrowser_window_new ();

	priv->m_curTab = NULL;
	priv->m_conn_status = TRUE;
	priv->m_isStandalone = FALSE;

    priv->network_monitor = g_object_ref (g_network_monitor_get_default ());
    g_signal_connect (priv->network_monitor,
        "notify::network-available",
        G_CALLBACK (rhayader_webbrowser_network_changed_cb),
        self);
    rhayader_webbrowser_check_network_connection (self);
}


#ifndef WEBRUNTIME

static void _rhayader_browser_idle_animation_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer pUserData)
{
	GError *error;
	RhayaderWebBrowser *self=(RhayaderWebBrowser *)pUserData;
	self->priv->mutter_proxy = canterbury_mutter_plugin_proxy_new_finish(res , &error);

}

static void
_on_rhayader_browser_idle_animation_name_appeared(GDBusConnection *connection,
		const gchar     *name,
		const gchar     *name_owner,
		gpointer         pUserData)
{
	canterbury_mutter_plugin_proxy_new (connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Canterbury.Mutter.Plugin",
			"/org/apertis/Canterbury/Mutter/Plugin",
			NULL,
			_rhayader_browser_idle_animation_proxy_clb,
			pUserData);
}

static void _rhayader_browser_register_for_mutter(RhayaderWebBrowser *self)
{
	g_bus_watch_name (G_BUS_TYPE_SESSION,
					"org.apertis.Canterbury.Mutter.Plugin",
					G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
					_on_rhayader_browser_idle_animation_name_appeared,
					NULL,
					self,
					NULL);
}

static void _rhayader_browser_on_idle_animation_started_cb(GObject *source_object, GAsyncResult *res,
		gpointer pUserData)
{
	RhayaderWebBrowser *self = (RhayaderWebBrowser *)pUserData;
	GError *error = NULL;
	canterbury_mutter_plugin_call_start_idle_animation_finish(self->priv->mutter_proxy,
			res, &error);
}


static void _on_idle_animation_stop_cb(GObject *source_object, GAsyncResult *res,
		gpointer pUserData)
{
	RhayaderWebBrowser *self = (RhayaderWebBrowser *)pUserData;
	GError *error = NULL;
	canterbury_mutter_plugin_call_stop_idle_animation_finish(self->priv->mutter_proxy,
			res, &error);
}

static void
rhayader_webbrowser_start_idle_animation (RhayaderWebBrowser *self)
{
	if(self->priv->mutter_proxy)
	canterbury_mutter_plugin_call_start_idle_animation(self->priv->mutter_proxy, NULL,
			_rhayader_browser_on_idle_animation_started_cb, self);
}

static void
rhayader_webbrowser_stop_idle_animation (RhayaderWebBrowser *self)
{
	if(self->priv->mutter_proxy)
	canterbury_mutter_plugin_call_stop_idle_animation(self->priv->mutter_proxy,
					NULL, _on_idle_animation_stop_cb, self);
}

static void
busy_state_changed_cb (RhayaderWebBrowserTab *tab,
    gboolean busy,
    RhayaderWebBrowser *self)
{
  g_return_if_fail (RHAYADER_IS_WEBBROWSER (self));

  if (busy)
    rhayader_webbrowser_start_idle_animation (self);
  else
    rhayader_webbrowser_stop_idle_animation (self);
}

static gboolean _rhayader_webbrowser_app_take_snapshot_and_update_clb(gpointer userdata)
{
	RhayaderWebBrowserPrivate* priv = WEBBROWSER_PRIVATE (userdata);
	RhayaderWebBrowserListView *listview = RHAYADER_WEBBROWSER_LIST_VIEW (rhayader_webbrowser_window_get_listview (priv->m_browser_window));
	RhayaderWebBrowserNormalView *normalview = RHAYADER_WEBBROWSER_NORMAL_VIEW (rhayader_webbrowser_window_get_normalview (priv->m_browser_window));
	ThornburyModel *listModel = rhayader_webbrowser_list_view_get_model (listview);
	ClutterActor* web_view = rhayader_webbrowser_normal_view_get_tab (normalview);
	gchar *url=NULL;
	gchar *request_url;
	gchar *title;
	ClutterActor *snapshot;
	gpointer cairo_surface;
	GValue pSnapShot = { 0 };

	g_object_get (web_view,
		"url", &url,
		NULL);
	g_object_set_data (G_OBJECT (priv->m_normalview_controller->m_tab), "request_url", url);
	request_url = url;
	title = g_object_get_data (G_OBJECT (priv->m_normalview_controller->m_tab), "title");
	snapshot = rhayader_webbrowser_tab_get_get_snapshot (priv->m_normalview_controller->m_tab);
	cairo_surface = g_object_get_data (G_OBJECT (priv->m_normalview_controller->m_tab), "cairo-surface");
	            g_object_set_data(G_OBJECT(snapshot),"request_url",request_url);
	            g_object_set_data(G_OBJECT(snapshot),"title",title);
	            g_object_set_data(G_OBJECT(snapshot),"cairo-surface",cairo_surface);
				g_value_init(&pSnapShot, G_TYPE_POINTER);
				g_value_set_pointer(&pSnapShot, snapshot);
				 thornbury_model_insert_value(listModel, priv->cur_row, 0,&pSnapShot);
	return FALSE;
}

static void _rhayader_webbrowser_normal_view_started_cb(RhayaderWebBrowserNormalView* self,gboolean is_shown,gpointer user_data)
{
	RhayaderWebBrowserPrivate* priv = WEBBROWSER_PRIVATE (user_data);
if(!is_shown)
{
			if(priv->m_normalview_controller && priv->m_normalview_controller->m_tab)
			{
				g_timeout_add(20,_rhayader_webbrowser_app_take_snapshot_and_update_clb,user_data);
				rhayader_webbrowser_tab_set_focus(priv->m_normalview_controller->m_tab,FALSE);
				rhayader_webbrowser_tab_get_tabs_backforwardlist(priv->m_normalview_controller->m_tab,priv->cur_row);

}
}
else
{
	rhayader_webbrowser_tab_set_tabs_backforwardlist(priv->m_normalview_controller->m_tab,priv->cur_row);
}
}

static gboolean
_rhayader_webbrowser_app_load_url_on_focussed_row (gpointer data)
{
	RhayaderWebBrowser *self = data;
	RhayaderWebBrowserListView *listview = RHAYADER_WEBBROWSER_LIST_VIEW (rhayader_webbrowser_window_get_listview (self->priv->m_browser_window));
	ThornburyModel *m_listModel = rhayader_webbrowser_list_view_get_model (listview);
	gint row = rhayader_webbrowser_list_view_get_focused_row(self->priv->m_listview_controller->m_listView);
	ThornburyModelIter *pIter;
	GValue active_actor = { 0 };
	gpointer thumbnail;

	self->priv->cur_row=row;
	pIter=thornbury_model_get_iter_at_row (m_listModel, row);
	  	 thornbury_model_iter_get_value(pIter, 0, &active_actor);
	thumbnail = g_value_get_pointer (&active_actor);
	  	g_object_set_data(G_OBJECT(self->priv->m_normalview_controller->m_tab),"request_url",(gchar *)g_object_get_data(G_OBJECT(thumbnail),"request_url"));
	  	rhayader_webbrowser_tab_load_url(self->priv->m_normalview_controller->m_tab,(gchar *)g_object_get_data(G_OBJECT(thumbnail),"request_url"));
	return FALSE;
}


static void _rhayader_webbrowser_listview_app_controller_action_requested_cb(ClutterActor *list_view,eTabsMenuOption action,RhayaderWebBrowser * self)
{
	ThornburyModel *m_listModel = rhayader_webbrowser_list_view_get_model (RHAYADER_WEBBROWSER_LIST_VIEW (list_view));
	switch(action)
	{
		case NEWVIEW:
			if(self->priv->m_normalview_controller->m_tab)
		    {
				rhayader_webbrowser_tab_load_url(self->priv->m_normalview_controller->m_tab,"about:blank");
				g_object_set_data(G_OBJECT(self->priv->m_normalview_controller->m_tab),"request_url","about:blank");
		    }
			rhayader_webbrowser_listview_controller_new_tab(self->priv->m_listview_controller,"about:blank");

			   // g_object_set_data(G_OBJECT(snapshot),"title",title);
				//self->priv->m_request_url = NULL;
		       //g_idle_add((GSourceFunc)_rhayader_webbrowser_listview_controller_new_tab_cb,self);
			break;

		case CLOSEVIEW:
		{
			if( thornbury_model_get_n_rows(THORNBURY_MODEL(m_listModel)) > 1)
			{
			//	g_print("\n CLOSE VIEWWWWWWWWW \n");
				gint row = rhayader_webbrowser_list_view_get_focused_row (RHAYADER_WEBBROWSER_LIST_VIEW (list_view));
				rhayader_webbrowser_list_model_remove_entry (RHAYADER_WEBBROWSER_LIST_MODEL (m_listModel), row);
				rhayader_webbrowser_tab_clear_tabs_backforwardlist(self->priv->m_normalview_controller->m_tab,row);
				g_timeout_add(350,_rhayader_webbrowser_app_load_url_on_focussed_row,self);
			}
		}
			break;

		default:
			g_assert(0);
			break;
	}
}


#endif


void rhayader_webbrowser_initialize(RhayaderWebBrowser *self,gchar* request_url)
{
	RhayaderWebBrowserPrivate* priv = WEBBROWSER_PRIVATE (self);
	ClutterActor *normalview;
#ifndef WEBRUNTIME
	ClutterActor *listview;
	ThornburyModel *m_listModel;
	gint active_tab;
	ThornburyModelIter *pIter;
	GValue active_actor = { 0 };
	gpointer thumbnail;
#endif

	priv->m_request_url = g_strdup(request_url);
	priv->m_conn_status = TRUE;
    rhayader_webbrowser_window_initialize(priv->m_browser_window,priv->m_toolbarWidth);

#ifndef WEBRUNTIME
    priv->cur_row=0;
    g_signal_connect(priv->m_browser_window, "action_requested",
      	                  G_CALLBACK(_rhayader_webbrowser_window_action_requested_cb),self);
	listview = rhayader_webbrowser_window_get_listview (priv->m_browser_window);
	priv->m_listview_controller =rhayader_webbrowser_listview_controller_new(listview);
  	g_signal_connect_after(listview, "tab_activated",G_CALLBACK(_rhayader_webbrowser_tab_activated_cb),self);
	g_signal_connect_after(listview, "transition_completed",
	   	                  G_CALLBACK(_rhayader_webbrowser_listview_transition_completed_cb),self);
	 g_signal_connect_after (G_OBJECT(listview),
								"action_requested",
								G_CALLBACK (_rhayader_webbrowser_listview_app_controller_action_requested_cb),
								self);

	//ThornburyModel* listModel = (RhayaderWebBrowserListModel*)rhayader_webbrowser_list_view_get_model(listview);
	_rhayader_browser_register_for_mutter(self);
#endif
  	normalview = rhayader_webbrowser_window_get_normalview (priv->m_browser_window);

  	priv->m_normalview_controller = rhayader_webbrowser_normalview_controller_new(normalview,priv->m_isStandalone);
#ifndef WEBRUNTIME
  	m_listModel = rhayader_webbrowser_list_view_get_model (RHAYADER_WEBBROWSER_LIST_VIEW (listview));
  	active_tab = rhayader_webbrowser_list_model_get_last_session_active_tab (RHAYADER_WEBBROWSER_LIST_MODEL(m_listModel));
  	pIter=thornbury_model_get_iter_at_row (m_listModel, active_tab);
  	 thornbury_model_iter_get_value(pIter, 0, &active_actor);
  	 thumbnail = g_value_get_pointer (&active_actor);
  	 g_object_set_data(G_OBJECT(priv->m_normalview_controller->m_tab),"request_url",(gchar *)g_object_get_data(G_OBJECT(thumbnail),"request_url"));
  	rhayader_webbrowser_tab_create_default_backforwardlist(priv->m_normalview_controller->m_tab,thornbury_model_get_n_rows(m_listModel));
	g_signal_connect_after(normalview, "transition_started",
	   	                  G_CALLBACK(_rhayader_webbrowser_normal_view_started_cb),self);
	 g_signal_connect(priv->m_normalview_controller->m_tab, "new_tab_requested",G_CALLBACK(_rhayader_webbrowser_new_tab_requested_cb),self);

	g_signal_connect_object (priv->m_normalview_controller->m_tab,
		"busy-state-changed", G_CALLBACK (busy_state_changed_cb),
		self, 0);
#else
	  g_timeout_add(5000,(GSourceFunc)_rhayader_webbrowser_app_load_webapp_url,self);
#endif

}
#ifdef WEBRUNTIME
static gboolean
_rhayader_webbrowser_app_load_webapp_url(RhayaderWebBrowser *self)
{
	RhayaderWebBrowserPrivate* priv = WEBBROWSER_PRIVATE (self);
	if(priv->m_request_url)
	{
		g_print("\n loading url %s",priv->m_request_url);
		rhayader_webbrowser_normalview_controller_load_url(priv->m_normalview_controller,priv->m_request_url);
		priv->m_request_url = NULL;
	}
	return G_SOURCE_REMOVE;
}
#endif


RhayaderWebBrowser *
rhayader_webbrowser_new (gboolean is_standalone,
    gfloat window_width,
    gfloat window_height,
    gfloat toolbar_width)
{
	RhayaderWebBrowser *self;
	GApplication *default_gapplication;
	const gchar *app_id;
	LightwoodWebviewAssistant *assistant;

	default_gapplication = g_application_get_default ();
	g_return_if_fail (default_gapplication != NULL);
	app_id = g_application_get_application_id (default_gapplication);

	self =  g_object_new (RHAYADER_TYPE_WEBBROWSER, NULL);
	mx_window_set_window_size (self->priv->m_browser_window, abs(window_width), abs(window_height));
	mx_window_show (self->priv->m_browser_window);
	//if(!is_standalone)
	//	 mx_window_set_window_position(self->priv->m_browser_window,0,54.0);
	assistant = mildenhall_webview_widget_create_assistant(app_id,
		mx_window_get_clutter_stage (self->priv->m_browser_window));
	lightwood_webview_init_session (app_id, assistant);

	self->priv->m_isStandalone = is_standalone;
	self->priv->m_toolbarWidth = toolbar_width;

	return self;

}

MxWindow* rhayader_webbrowser_get_window(RhayaderWebBrowser *self)
{
	return self->priv->m_browser_window;
}
void rhayader_webbrowser_set_connection_status(RhayaderWebBrowser *self,gboolean conn_status)
{
	RhayaderWebBrowserPrivate* priv = WEBBROWSER_PRIVATE (self);
	if(priv->m_conn_status !=conn_status)
	{
		priv->m_conn_status=conn_status;
#ifndef WEBRUNTIME
		if(priv->m_conn_status == TRUE)
			rhayader_webbrowser_load_url(self,DEFAULT_HOMEPAGE);
		else
			rhayader_webbrowser_load_url(self,OFFLINE_HOMEPAGE);
#endif
	}

}


gboolean rhayader_webbrowser_goto_previousview(RhayaderWebBrowser *self)
{

#ifndef WEBRUNTIME
	return rhayader_webbrowser_window_switchto_previousview(self->priv->m_browser_window);
#else
	return rhayader_webbrowser_normalview_controller_go_back(self->priv->m_normalview_controller);
#endif


}

void rhayader_webbrowser_stop(RhayaderWebBrowser * self)
{
	rhayader_webbrowser_tab_stop_audio(self->priv->m_normalview_controller->m_tab);
}

#ifndef WEBRUNTIME
void
rhayader_webbrowser_load_url (RhayaderWebBrowser *self,
    const gchar *request_url)
{
	RhayaderWebBrowserPrivate* priv = WEBBROWSER_PRIVATE (self);
	if(!priv->m_conn_status)
		request_url = OFFLINE_HOMEPAGE;


	if( rhayader_webbrowser_window_switchto_listview(self->priv->m_browser_window))
	{
		rhayader_webbrowser_listview_controller_new_tab(self->priv->m_listview_controller,request_url);
	}
}

void rhayader_webbrowser_save(RhayaderWebBrowser * self)
{
	RhayaderWebBrowserPrivate* priv = WEBBROWSER_PRIVATE (self);
	gint active_tab=-1;
	ThornburyModel * model = rhayader_webbrowser_list_view_get_model(priv->m_listview_controller->m_listView);
	GError *error = NULL;

//	if(	rhayader_webbrowser_window_get_currentview(priv->m_browser_window) != LIST_VIEW)
		active_tab = rhayader_webbrowser_list_view_get_focused_row(priv->m_listview_controller->m_listView);
	if (!rhayader_webbrowser_list_model_save (RHAYADER_WEBBROWSER_LIST_MODEL (model),
		active_tab, &error))
	{
		WARNING ("%s", error->message);
		g_clear_error (&error);
	}
	rhayader_webbrowser_tab_save(priv->m_normalview_controller->m_tab);

}
static gboolean
rhayader_webbrowser_listview_new_tab_cb(RhayaderWebBrowser *self)
{
	if(self->priv->m_normalview_controller->m_tab)
    g_object_set_data(G_OBJECT(self->priv->m_normalview_controller->m_tab),"request_url",self->priv->m_request_url);
	rhayader_webbrowser_listview_controller_new_tab(self->priv->m_listview_controller,self->priv->m_request_url);
	self->priv->m_request_url = NULL;
	return G_SOURCE_REMOVE;
}

typedef struct _animation_data animation_data;
struct _animation_data
{
	RhayaderWebBrowserListView* listView;
	gint tab;
};

static gboolean
_rhayader_webbrowser_list_view_set_focused_row (gpointer data)
{
	animation_data* UserData = (animation_data*)data;
	if(UserData->listView != NULL)
	{
		rhayader_webbrowser_list_view_set_focused_row(UserData->listView,UserData->tab,TRUE);
	}
	return FALSE;
}


static void
_rhayader_webbrowser_listview_transition_completed_cb(RhayaderWebBrowserListView *listview,gboolean is_shown,RhayaderWebBrowser *self)

{
	RhayaderWebBrowserPrivate* priv = WEBBROWSER_PRIVATE (self);
	static gboolean ONCE=TRUE;
	if(is_shown )
	{
#ifndef WEBRUNTIME
		rhayader_webbrowser_stop_idle_animation (self);
#endif
        if(ONCE)
		{
			ThornburyModel *listModel;
			gint active_tab;

			ONCE=FALSE;
			listModel = rhayader_webbrowser_list_view_get_model (listview);
			active_tab = rhayader_webbrowser_list_model_get_last_session_active_tab (RHAYADER_WEBBROWSER_LIST_MODEL (listModel));
			if(!priv->m_request_url && !thornbury_model_get_n_rows(listModel))
			{
				priv->m_request_url = DEFAULT_HOMEPAGE;
			}
			if(priv->m_request_url)
			{
				g_print("\n loading url %s",priv->m_request_url);
				g_timeout_add(1000,(GSourceFunc)rhayader_webbrowser_listview_new_tab_cb,self);
				//rhayader_webbrowser_listview_controller_new_tab(priv->m_listview_controller);
			}
			else if(active_tab > -1)
			{
				animation_data *data = g_new0(animation_data,1);
			//	g_print("\n @@@@@@@@@@@@@@ on startup activating row %d @@@@@@@@@@",active_tab);
				data->listView = listview;
				data->tab = active_tab;
				g_timeout_add(1000,(GSourceFunc)_rhayader_webbrowser_list_view_set_focused_row,data);
				//rhayader_webbrowser_list_view_set_focused_row(listview,active_tab,TRUE);
			}
		}
		else
		{
			if(priv->m_normalview_controller->m_tab)
			rhayader_webbrowser_tab_set_focus(priv->m_normalview_controller->m_tab,FALSE);

			if(priv->m_request_url)
				{
					g_print("\n loading url %s",priv->m_request_url);
					g_timeout_add(1000,(GSourceFunc)rhayader_webbrowser_listview_new_tab_cb,self);
					//rhayader_webbrowser_listview_controller_new_tab(priv->m_listview_controller);
				}
		}
	}
}



static void _rhayader_webbrowser_window_action_requested_cb(RhayaderWebbrowserWindow *window,eWindowOption action,RhayaderWebBrowser *self)
{
	switch(action)
	{
		case WINDOW_SCROLLTO_TOP:
			rhayader_webbrowser_normalview_controller_scrolltoTop(self->priv->m_normalview_controller);
			break;
		default:
			break;
	}
}



static void _rhayader_webbrowser_tab_activated_cb(RhayaderWebBrowserListView* listView,ClutterActor *tab,RhayaderWebBrowser *self)
{

	RhayaderWebBrowserPrivate* priv = WEBBROWSER_PRIVATE (self);
	gchar *request_url;
	gchar *title;

	g_assert(CLUTTER_IS_ACTOR(tab));
	priv->cur_row=GPOINTER_TO_INT(g_object_get_data(G_OBJECT(tab),"row"));
	//g_print("\n _rhayader_webbrowser_tab_activated_cb %s",priv->m_request_url);
	if(self->priv->m_normalview_controller->m_tab)
	rhayader_webbrowser_tab_load_url(self->priv->m_normalview_controller->m_tab,"about:blank");
	priv->m_curTab = tab;

	request_url = g_object_get_data (G_OBJECT (tab), "request_url");
	title = g_object_get_data (G_OBJECT (tab), "title");
    g_object_set_data(G_OBJECT(self->priv->m_normalview_controller->m_tab),"request_url",request_url);
    g_object_set_data(G_OBJECT(self->priv->m_normalview_controller->m_tab),"title",title);

   // rhayader_webbrowser_tab_set_tabs_backforwardlist(self->priv->m_normalview_controller->m_tab,priv->cur_row);

	rhayader_webbrowser_window_switchto_normalview(self->priv->m_browser_window);
}


static void _rhayader_webbrowser_new_tab_requested_cb(RhayaderWebBrowserTab* tab,gchar* request_url,RhayaderWebBrowser *self)
{
	RhayaderWebBrowserPrivate* priv = WEBBROWSER_PRIVATE (self);
	g_print("\n _rhayader_webbrowser_new_tab_requested_cb %s",request_url);
	priv->m_request_url = g_strdup(request_url);
	rhayader_webbrowser_window_switchto_listview(priv->m_browser_window);

	//rhayader_webbrowser_listview_controller_new_tab(self->priv->m_listview_controller,request_url);
}

#endif






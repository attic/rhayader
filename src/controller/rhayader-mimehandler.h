/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_MIMEHANDLER_H
#define _RHAYADER_WEBBROWSER_MIMEHANDLER_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_MIMEHANDLER rhayader_webbrowser_mimehandler_get_type()

#define RHAYADER_WEBBROWSER_MIMEHANDLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_MIMEHANDLER, RhayaderWebBrowserMimeHandler))

#define RHAYADER_WEBBROWSER_MIMEHANDLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_MIMEHANDLER, RhayaderWebBrowserMimeHandlerClass))

#define RHAYADER_IS_WEBBROWSER_MIMEHANDLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_MIMEHANDLER))

#define RHAYADER_IS_WEBBROWSER_MIMEHANDLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_MIMEHANDLER))

#define RHAYADER_WEBBROWSER_MIMEHANDLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_MIMEHANDLER, RhayaderWebBrowserMimeHandlerClass))

typedef struct _RhayaderWebBrowserMimeHandler RhayaderWebBrowserMimeHandler;
typedef struct _RhayaderWebBrowserMimeHandlerClass RhayaderWebBrowserMimeHandlerClass;
typedef struct _RhayaderWebBrowserMimeHandlerPrivate RhayaderWebBrowserMimeHandlerPrivate;

struct _RhayaderWebBrowserMimeHandler
{
  GObject parent;

  RhayaderWebBrowserMimeHandlerPrivate *priv;
};

struct _RhayaderWebBrowserMimeHandlerClass
{
  GObjectClass parent_class;
};

GType rhayader_webbrowser_mimehandler_get_type (void) G_GNUC_CONST;

RhayaderWebBrowserMimeHandler *rhayader_webbrowser_mimehandler_new (GObject* browserTab,ClutterActor* parent);




G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_MIMEHANDLER_H */

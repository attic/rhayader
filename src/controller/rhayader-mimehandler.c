/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <glib/gstdio.h>
#include "rhayader-mimehandler.h"
#include <errno.h>
#include <string.h>

#include <didcot/didcot.h>
#include <mildenhall/mildenhall_webview_alert_handler.h>

#pragma GCC diagnostic push
/* FIXME: stop disabling these warnings when webkit-clutter is fixed.
 * https://bugs.apertis.org/show_bug.cgi?id=565 */
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <webkit/webkitiwebview.h>
#include <webkit/webkitnetworkresponse.h>
#include <webkit/webkitdownload.h>
#pragma GCC diagnostic pop

G_DEFINE_TYPE (RhayaderWebBrowserMimeHandler, rhayader_webbrowser_mimehandler, G_TYPE_OBJECT)

#define WEBBROWSER_MIMEHANDLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_MIMEHANDLER, RhayaderWebBrowserMimeHandlerPrivate))

struct _RhayaderWebBrowserMimeHandlerPrivate
{
  gchar *DownloadDir;
  MildenhallWebViewWidgetAlertHandler* m_dialog;
  DidcotLaunch* didcot_proxy;
};

typedef struct
{
	gchar* url;
	gchar* title;
}FeedDetails;

typedef struct
{
	RhayaderWebBrowserMimeHandler* m_self;
	WebKitDownload* m_download;
	gboolean 		m_is_save;
	gboolean 		m_is_saved;
}DownloadData;

typedef enum
{
	MMD_MSG_BOX_RESP_YES,
	MMD_MSG_BOX_RESP_CANCEL
}MmdLib_MsgBoxResp;
//private functions
static void
_rhayader_webbrowser_mime_detected_cb(ClutterActor* browserTab,gboolean is_save,WebKitDownload* download,RhayaderWebBrowserMimeHandler* self);
static void
_rhayader_webbrowser_mimehandler_download_status_cb (WebKitDownload* download, GParamSpec* pspec, DownloadData* download_data);
static gboolean
_rhayader_webbrowser_mimehandler_perform_download(DownloadData* download_data);

//static void
//_rhayader_webbrowser_feed_detected_cb(RhayaderWebBrowserTab* browserTab,guint type,RhayaderWebBrowserMimeHandler* self);
//static gboolean
//_rhayader_webbrowser_mimehandler_rssfeeds_cb(MmdLib_MsgBoxResp resp, gpointer user_data);

static void
rhayader_webbrowser_mimehandler_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_mimehandler_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_mimehandler_class_init (RhayaderWebBrowserMimeHandlerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserMimeHandlerPrivate));

  object_class->dispose = rhayader_webbrowser_mimehandler_dispose;
}

static void
rhayader_webbrowser_mimehandler_init (RhayaderWebBrowserMimeHandler *self)
{
  self->priv = WEBBROWSER_MIMEHANDLER_PRIVATE (self);
  self->priv->DownloadDir = NULL;
  self->priv->m_dialog = NULL;

  self->priv->didcot_proxy = didcot_launch_proxy_new_for_bus_sync(
		  	  	  	  	  	  	  	  	  G_BUS_TYPE_SESSION,
		  	  	  	  	  	  	  	  	  G_DBUS_PROXY_FLAGS_NONE,
		  	  	  	  	  	  	  	  	  "Didcot.Service",
		  	  	  	  	  	  	  	  	  "/Didcot/Launch/Service",
		  	  	  	  	  	  	  	  	  NULL,NULL);

  g_assert(self->priv->didcot_proxy != NULL);
}

static void
rhayader_webbrowser_mimehandler_register (RhayaderWebBrowserMimeHandler *self,
    GObject *browserTab,
    ClutterActor *parent)
{
	self->priv->m_dialog = MILDENHALL_WEB_VIEW_WIDGET_ALERT_HANDLER (
		mildenhall_web_view_widget_alert_handler_new (parent, NULL, NULL));
	g_signal_connect(browserTab,"mime_detected",(GCallback)_rhayader_webbrowser_mime_detected_cb,self);
	//g_signal_connect(browserTab,"feed_detected",(GCallback)_rhayader_webbrowser_feed_detected_cb,self);
}

RhayaderWebBrowserMimeHandler *
rhayader_webbrowser_mimehandler_new (GObject* browserTab,ClutterActor* parent)
{
	RhayaderWebBrowserMimeHandler* self =  g_object_new (RHAYADER_TYPE_WEBBROWSER_MIMEHANDLER, NULL);
	rhayader_webbrowser_mimehandler_register(self,browserTab,parent);
    return self;
}


/*static void
_rhayader_webbrowser_feed_detected_cb(RhayaderWebBrowserTab* browserTab,guint type,RhayaderWebBrowserMimeHandler* self)
{

   //if(type) return;
     FeedDetails* feed_details = g_new(FeedDetails,1);
     feed_details->title = g_strdup(rhayader_webbrowser_tab_get_title(browserTab));
     feed_details->url = g_strdup(rhayader_webbrowser_tab_get_url(browserTab));
     gchar* mesg = NULL;
     if(!type)
     {
         //mesg = g_strdup_printf("Do you want to subscribe to this feed?%.35s...",feed_details->url);
    	 //rhayader_webbrowser_prompt_user(MMD_MSG_BOX_TYPE_CONFIRM_YN,mesg,NULL,_rhayader_webbrowser_mimehandler_rssfeeds_cb,feed_details);

    	 g_print("\n ******feed detected**********");
    	 _rhayader_webbrowser_mimehandler_rssfeeds_cb(MMD_MSG_BOX_RESP_YES,feed_details);

     }
     else
    	 //rhayader_webbrowser_show_mesg(g_strdup_printf("podcast detected?%.35s",feed_details->url));
    	 g_print("\n ******podcast detected**********");
}
static gboolean
_rhayader_webbrowser_mimehandler_rssfeeds_cb(MmdLib_MsgBoxResp resp, gpointer user_data)
{
	if(resp != MMD_MSG_BOX_RESP_YES)
		return 	TRUE;

	FeedDetails* feed_details = (FeedDetails*)user_data;
	//rhayader_dbw_client_subscribefor_feeds(feed_details->url,feed_details->title);
	g_print("\n subscribe for feeds %s",feed_details->title);
	return TRUE;
}*/



static void
_rhayader_webbrowser_mime_detected_cb(ClutterActor* browserTab,gboolean is_save,WebKitDownload* download,RhayaderWebBrowserMimeHandler* self)
{

   //g_print("\n RHAYADER-WEBBROWSER: trying to share- uri = %s  mime_type= %s",webkit_download_get_uri(download),mime_type);
   //rhayader_dbw_utils_share_data(RHAYADER_BROWSER_APP_NAME, "LINK", RHAYADER_DATA_EXCHANGE_SECURITY_NORMAL, 100, mime_type, webkit_download_get_uri(download));

 /*  DownloadData* download_data = g_new(DownloadData,1);
   download_data->m_download = download;
   download_data->m_is_save = is_save;
   download_data->m_self = self;
 //  gchar* mesg = NULL;
   _rhayader_webbrowser_mimehandler_perform_download(download_data);*/




   const gchar* mime_type = (gchar*)g_object_get_data(G_OBJECT(download),"mime-type");
   if(!g_strcmp0(mime_type,"image/*"))
   {
   	  DownloadData* download_data = g_new(DownloadData,1);

   	   download_data->m_download = download;
   	   download_data->m_is_save = is_save;
   	   download_data->m_self = self;
   	_rhayader_webbrowser_mimehandler_perform_download(download_data);
   }
   else
   {

      const gchar* downloaded_file = webkit_download_get_uri(download);
      gchar *mesg = g_strdup_printf("Do you want to open %s?",downloaded_file);
      gboolean is_continue=FALSE;
      GApplication *default_gapplication;
      const gchar *app_id = NULL;

      default_gapplication = g_application_get_default ();
      g_return_if_fail (default_gapplication != NULL);
      app_id = g_application_get_application_id (default_gapplication);

      mildenhall_web_view_widget_alert_handler_show_confirm(self->priv->m_dialog,mesg,&is_continue);
      if (is_continue)
        {
          didcot_launch_call_open_uri_sync (
              self->priv->didcot_proxy, downloaded_file,
              NULL, NULL);
        }
      g_free(mesg);
      mesg=NULL;
   }






}

static gboolean
_rhayader_webbrowser_mimehandler_perform_download(DownloadData* download_data)
{

	const gchar *mime_type = g_object_get_data (G_OBJECT (download_data->m_download), "mime-type");
	gchar *filename = NULL;
	gchar *DownloadDir = NULL;
	gchar *uri;

	if(mime_type == NULL)
	{
		SoupMessage* res = webkit_network_response_get_message(
								webkit_download_get_network_response(download_data->m_download));
		mime_type = soup_message_headers_get_content_type(res->response_headers,NULL);
	}
	g_print("\n download file type=%s",mime_type);
	if(download_data->m_is_save)
	{
		 if(g_strstr_len(mime_type,50,"image") != NULL)
			 DownloadDir = (gchar *)g_build_filename ( g_get_home_dir(),IMAGEDIR , NULL);
		 else
			 DownloadDir = (gchar *)g_build_filename ( g_get_home_dir(),DOCDIR , NULL);
	}
	else
		DownloadDir = (gchar *)g_build_filename ( g_get_tmp_dir (), NULL);

	 if(!g_file_test (DownloadDir, G_FILE_TEST_IS_DIR))
	 {
		 int err = g_mkdir_with_parents(DownloadDir, 0755);
		 if(err == -1)g_print("error = %s",strerror(errno));
	 }
//rhayader_webbrowser_idle_animation_start();
	 filename = g_strdup_printf("%s/%s",DownloadDir,webkit_download_get_suggested_filename(download_data->m_download));
    uri = g_filename_to_uri(filename, NULL, NULL);
    webkit_download_set_destination_uri(download_data->m_download, uri);
    g_signal_connect(download_data->m_download,"notify::status",(GCallback)_rhayader_webbrowser_mimehandler_download_status_cb,download_data);
    webkit_download_start(download_data->m_download);
    g_free(filename);
    g_free(uri);

	return TRUE;
}


/*static gboolean
_rhayader_webbrowser_mimehandler_openOption_cb(MmdLib_MsgBoxResp resp, gpointer user_data)
{
	if(resp != MMD_MSG_BOX_RESP_YES)
		return 	TRUE;

	DownloadData* download_data = (DownloadData*) user_data;
	const gchar* downloaded_file = webkit_download_get_destination_uri(download_data->m_download);
	const gchar* mime_type = (gchar*)g_object_get_data(G_OBJECT(download_data->m_download),"mime-type");
	if(mime_type == NULL)
	{
		SoupMessage* res = webkit_network_response_get_message(
								webkit_download_get_network_response(download_data->m_download));
		mime_type = soup_message_headers_get_content_type(res->response_headers,NULL);
	}
	g_print("\n open %s of %s",downloaded_file,mime_type);
	return TRUE;
}*/




static void
_rhayader_webbrowser_mimehandler_download_status_cb (WebKitDownload* download, GParamSpec* pspec, DownloadData* download_data)
{
	const gchar* filename = webkit_download_get_suggested_filename(download_data->m_download);
	WebKitDownloadStatus status = webkit_download_get_status(download_data->m_download);
	switch(status)
	{
		case WEBKIT_DOWNLOAD_STATUS_STARTED:
		{
			gchar* mesg = NULL;
			gboolean is_continue = TRUE;

			if(download_data->m_is_save)
				mesg = g_strdup_printf("saving....%s",filename);
			else
				mesg = g_strdup_printf("opening....%s",webkit_download_get_uri(download_data->m_download));

			mildenhall_web_view_widget_alert_handler_show_confirm(download_data->m_self->priv->m_dialog,mesg,&is_continue);
			if(!is_continue)
			{
				gchar* dest_file = g_filename_from_uri(
												webkit_download_get_destination_uri(download_data->m_download),
												NULL,NULL);
					webkit_download_cancel(download_data->m_download);
					g_print("\n deleting %s",dest_file);
					g_unlink (dest_file);
			}
			g_free(mesg);
		}
			break;

		case WEBKIT_DOWNLOAD_STATUS_FINISHED:
		{
			//rhayader_webbrowser_idle_animation_stop();
			SoupMessage* res = webkit_network_response_get_message(
									webkit_download_get_network_response(download_data->m_download));
			const gchar* mime_type = soup_message_headers_get_content_type(res->response_headers,NULL);
			const gchar* downloaded_file = webkit_download_get_destination_uri(download_data->m_download);

			if(download_data->m_is_save)
			{
				if(!download_data->m_is_saved)
				{
					break;
				}
				//rhayader_webbrowser_hide_mesg();
				g_print("\n downloaded: %s",downloaded_file);

			}
			else
			{
				//rhayader_webbrowser_hide_mesg();
				g_print("\n saved in temp: %s,%s",mime_type,downloaded_file);
			/*	didcot_launch_call_launch_app_with_file_url_sync(
						download_data->m_self->priv->didcot_proxy, app_id, downloaded_file,
						NULL,NULL);*/
				//rhayader_dbw_utils_share_data(RHAYADER_BROWSER_APP_NAME,
					//					ATTACHMENT_DATA, RHAYADER_DATA_EXCHANGE_SECURITY_NORMAL,100, mime_type,
						//				downloaded_file);
			}
		}
			break;

		case WEBKIT_DOWNLOAD_STATUS_CANCELLED:
		{
			//rhayader_webbrowser_hide_mesg();
			//rhayader_webbrowser_idle_animation_stop();
		}
			break;

		case WEBKIT_DOWNLOAD_STATUS_ERROR:
		{
			gchar *mesg = NULL;

			//rhayader_webbrowser_idle_animation_stop();
			if(download_data->m_is_save)
				mesg = g_strdup_printf("unable to save:%s",filename);
			else
				mesg = g_strdup_printf("unable to download:%s",filename);
			mildenhall_web_view_widget_alert_handler_show_alert(download_data->m_self->priv->m_dialog,mesg);
			g_print("\n %s",mesg);

			g_free(mesg);
		}
			break;

		default:
			break;
	}
}


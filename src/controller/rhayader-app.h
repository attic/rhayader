/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_H_
#define _RHAYADER_WEBBROWSER_H_

#include <glib-object.h>
#include <clutter/clutter.h>

#pragma GCC diagnostic push
/* FIXME: stop disabling these warnings when Mx is fixed.
 * https://bugs.apertis.org/show_bug.cgi?id=567 */
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER            (rhayader_webbrowser_get_type ())
#define RHAYADER_WEBBROWSER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), RHAYADER_TYPE_WEBBROWSER, RhayaderWebBrowser))
#define RHAYADER_IS_WEBBROWSER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RHAYADER_TYPE_WEBBROWSER))


typedef struct _RhayaderWebBrowser       RhayaderWebBrowser;
typedef struct _RhayaderWebBrowserClass  RhayaderWebBrowserClass;
typedef struct _RhayaderWebBrowserPrivate RhayaderWebBrowserPrivate;

/*The browser actor which is made a new type */
struct _RhayaderWebBrowser
{
	GObject parent;
	RhayaderWebBrowserPrivate *priv;
};

/*Protoype for the get type. This creates the new object */
struct _RhayaderWebBrowserClass
{	
	GObjectClass parent;
};

GType rhayader_webbrowser_get_type (void);

/*Protoype for the get type. This creates the new object */
RhayaderWebBrowser *rhayader_webbrowser_new (gboolean is_standalone,
    gfloat window_width,
    gfloat window_height,
    gfloat toolbar_width);
void rhayader_webbrowser_initialize(RhayaderWebBrowser *self,gchar* request_url);
MxWindow* rhayader_webbrowser_get_window(RhayaderWebBrowser *self);
void rhayader_webbrowser_set_connection_status(RhayaderWebBrowser *self,gboolean conn_status);
gboolean rhayader_webbrowser_goto_previousview(RhayaderWebBrowser *self);
void rhayader_webbrowser_save(RhayaderWebBrowser * self);
#ifndef WEBRUNTIME
void rhayader_webbrowser_load_url(RhayaderWebBrowser *self,
    const gchar *request_url);
#endif
void rhayader_webbrowser_stop(RhayaderWebBrowser * self);
G_END_DECLS

#endif 

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-db.h"
#include <seaton-preference.h>

#include <canterbury/canterbury.h>

#include "messages-internal.h"

G_DEFINE_TYPE (RhayaderWebbrowserDB, rhayader_webbrowser_db, G_TYPE_OBJECT)

#define WEBBROWSER_DB_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_DB, RhayaderWebbrowserDBPrivate))

struct _RhayaderWebbrowserDBPrivate
{
	SeatonPreference* m_db;

};

static GObject* rhayader_webbrowser_db_contruct(GType gtype, guint n_properties,
		GObjectConstructParam* properties)
{
	static GObject* self = NULL;
	if (self == NULL)
	{

		self = G_OBJECT_CLASS (rhayader_webbrowser_db_parent_class)->constructor(
				gtype, n_properties, properties);
		g_object_add_weak_pointer(self, (gpointer) &self);
	}
	else
	{
		g_object_ref(self);
	}

	return self;
}

static void
rhayader_webbrowser_db_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_db_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_db_class_init (RhayaderWebbrowserDBClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebbrowserDBPrivate));
  object_class->constructor = rhayader_webbrowser_db_contruct;
  object_class->dispose = rhayader_webbrowser_db_dispose;
}

static void
rhayader_webbrowser_db_init (RhayaderWebbrowserDB *self)
{
  int inResult;
  SeatonFieldParam pfieldparam[2];
  const gchar *bundle_id;
  GApplication *default_gapplication;
  const gchar *app_id = NULL;

  bundle_id = cby_get_bundle_id ();
  g_return_if_fail (bundle_id != NULL);
  default_gapplication = g_application_get_default ();
  g_return_if_fail (default_gapplication != NULL);
  app_id = g_application_get_application_id (default_gapplication);

  self->priv = WEBBROWSER_DB_PRIVATE (self);
  self->priv->m_db = seaton_preference_new();

  inResult = seaton_preference_open (self->priv->m_db,
      bundle_id, app_id, SEATON_PREFERENCE_APP_INTERNAL_DB);

  if (inResult != 0)
    {
      ERROR ("Failed to open preference database: seaton error #%d", inResult);
      return;
    }

	pfieldparam[0].pKeyName= g_strdup("url");
	pfieldparam[0].inFieldType = SQLITE_TEXT;
	pfieldparam[1].pKeyName= g_strdup("thumbnail");
	pfieldparam[1].inFieldType = SQLITE_TEXT;

	inResult =  seaton_preference_install(self->priv->m_db, "Tabs", pfieldparam, 2 );

  if (inResult != 0)
    {
      ERROR ("Failed to install Tabs table: seaton error #%d", inResult);
      return;
    }

	pfieldparam[0].pKeyName= g_strdup("indx");
	pfieldparam[0].inFieldType = SQLITE_INTEGER;
	inResult =  seaton_preference_install(self->priv->m_db, "ActiveTab", pfieldparam, 1 );

  if (inResult != 0)
    {
      ERROR ("Failed to install Tabs table: seaton error #%d", inResult);
      return;
    }
}

RhayaderWebbrowserDB *
rhayader_webbrowser_db_get_singleton (void)
{
  return g_object_new (RHAYADER_TYPE_WEBBROWSER_DB, NULL);
}

void rhayader_webbrowser_db_save_session(RhayaderWebbrowserDB * self,sBrowserTab* tabs,guint count,gint active_tab)
{
	GHashTable*	hash = g_hash_table_new(g_str_hash, g_str_equal);
	guint ind=0;
	int inResult =0;
	//GlsPdiAppdata *appdata= g_new0(GlsPdiAppdata ,count);

	seaton_preference_remove (self->priv->m_db, "Tabs", NULL, NULL);

	for(ind=0;ind<count;ind++)
	{

		      if(NULL == tabs[ind].url)
		      tabs[ind].url=g_strdup("about:blank");
		      if(NULL == tabs[ind].thumbnail)
		       tabs[ind].thumbnail=g_strdup(" ");
	           /*	appdata[ind].searchtext=tabs[ind].url;
		      		appdata[ind].displaytext=tabs[ind].url;
		      		appdata[ind].mime_type="mt_browse";
		      		GVariantBuilder *pGvb = g_variant_builder_new ( G_VARIANT_TYPE ("a{ss}") );
		      		g_variant_builder_add (pGvb,"{ss}","url",tabs[ind].url);
		      		GVariant* arguments = g_variant_builder_end (pGvb);
		      		appdata[ind].arguments=arguments;
		      		appdata[ind].imagepath=g_strconcat("file://",tabs[ind].thumbnail,NULL);*/
		g_hash_table_insert (hash, (gchar *) "url", tabs[ind].url);
		g_hash_table_insert (hash, (gchar *) "thumbnail", tabs[ind].thumbnail);
		inResult =seaton_preference_add_data (self->priv->m_db, "Tabs", hash );
		if (inResult != 0)
		{
			ERROR ("Failed to save tab: seaton error #%d", inResult);
		}
	}


	g_hash_table_remove_all(hash);
	seaton_preference_remove(self->priv->m_db,"ActiveTab",NULL,NULL);

	g_hash_table_insert (hash, (gchar *) "indx",
		g_strdup_printf ("%d",active_tab));
	inResult =seaton_preference_add_data (self->priv->m_db, "ActiveTab", hash );

	if (inResult != 0)
	{
		ERROR ("Failed to save active tab: seaton error #%d", inResult);
	}

}

guint  rhayader_webbrowser_db_retrieve_session(RhayaderWebbrowserDB * self,sBrowserTab** tabs)
{

	GHashTable*     hashChk;
//	GList			*list;
//	GList 			*ptr;
//	gint   			iFieldSize;

//	gint				inVal;
//	gint 			inResult;
	guint				inNumRows;
//	gchar			cVal[512];

	GPtrArray* gpArray = seaton_preference_get(self->priv->m_db, "Tabs", NULL, NULL );
	guint count;
	sBrowserTab *urls;

	if(!gpArray || (gpArray->len == 0))		return 0;

	count = gpArray->len;
	urls = g_new (sBrowserTab, gpArray->len);
	for (inNumRows = 0; inNumRows < count; inNumRows++)
	{
		hashChk = g_ptr_array_index(gpArray , inNumRows);
		//g_assert(g_hash_table_size(hashChk) ==1);
		urls[inNumRows].url = g_strdup(g_hash_table_lookup(hashChk,"url"));
		urls[inNumRows].thumbnail = g_strdup(g_hash_table_lookup(hashChk,"thumbnail"));
		g_print("\n retrieved session url %s",urls[inNumRows].url);
	}
	*tabs = urls;
	return count;

}

gint  rhayader_webbrowser_db_retrieve_last_active_tab(RhayaderWebbrowserDB * self)
{
	GHashTable*     hashChk=NULL;
	gint active_tab=-1;
	GPtrArray* gpArray = seaton_preference_get(self->priv->m_db, "ActiveTab", NULL, NULL );
	if(gpArray && gpArray->len)
	{
		hashChk = g_ptr_array_index(gpArray , 0);
		if(g_hash_table_size(hashChk) > 0)
			active_tab = g_ascii_strtoll (g_hash_table_lookup(hashChk,"indx"),NULL,0);
	}
	return active_tab;
}

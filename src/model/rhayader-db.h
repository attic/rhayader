/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __RHAYADER_WEBBROWSER_DB_H__
#define __RHAYADER_WEBBROWSER_DB_H__

#include <glib-object.h>


G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_DB rhayader_webbrowser_db_get_type()

#define RHAYADER_WEBBROWSER_DB(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_DB, RhayaderWebbrowserDB))

#define RHAYADER_WEBBROWSER_DB_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_DB, RhayaderWebbrowserDBClass))

#define RHAYADER_IS_WEBBROWSER_DB(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_DB))

#define RHAYADER_IS_WEBBROWSER_DB_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_DB))

#define RHAYADER_WEBBROWSER_DB_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_DB, RhayaderWebbrowserDBClass))

typedef struct _RhayaderWebbrowserDB RhayaderWebbrowserDB;
typedef struct _RhayaderWebbrowserDBClass RhayaderWebbrowserDBClass;
typedef struct _RhayaderWebbrowserDBPrivate RhayaderWebbrowserDBPrivate;

struct _RhayaderWebbrowserDB
{
  GObject parent;

  RhayaderWebbrowserDBPrivate *priv;
};

struct _RhayaderWebbrowserDBClass
{
  GObjectClass parent_class;
};

typedef struct
{
	gchar* url;
	gchar* thumbnail;

}sBrowserTab;

GType rhayader_webbrowser_db_get_type (void) G_GNUC_CONST;

RhayaderWebbrowserDB *rhayader_webbrowser_db_get_singleton (void);
void rhayader_webbrowser_db_save_session(RhayaderWebbrowserDB * self,sBrowserTab* tabs,guint count,gint active_tab);
guint  rhayader_webbrowser_db_retrieve_session(RhayaderWebbrowserDB * self,sBrowserTab** tabs);
gint  rhayader_webbrowser_db_retrieve_last_active_tab(RhayaderWebbrowserDB * self);



G_END_DECLS

#endif /* __RHAYADER_WEBBROWSER_DB_H__ */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _RHAYADER_WEBBROWSER_LIST_MODEL_H
#define _RHAYADER_WEBBROWSER_LIST_MODEL_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include "libthornbury-model.h"

G_BEGIN_DECLS

#define RHAYADER_TYPE_WEBBROWSER_LIST_MODEL rhayader_webbrowser_list_model_get_type()

#define RHAYADER_WEBBROWSER_LIST_MODEL(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_MODEL, RhayaderWebBrowserListModel))

#define RHAYADER_WEBBROWSER_LIST_MODEL_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  RHAYADER_TYPE_WEBBROWSER_LIST_MODEL, RhayaderWebBrowserListModelClass))

#define RHAYADER_IS_WEBBROWSER_LIST_MODEL(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_MODEL))

#define RHAYADER_IS_WEBBROWSER_LIST_MODEL_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  RHAYADER_TYPE_WEBBROWSER_LIST_MODEL))

#define RHAYADER_WEBBROWSER_LIST_MODEL_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  RHAYADER_TYPE_WEBBROWSER_LIST_MODEL, RhayaderWebBrowserListModelClass))

typedef struct _RhayaderWebBrowserListModel RhayaderWebBrowserListModel;
typedef struct _RhayaderWebBrowserListModelClass RhayaderWebBrowserListModelClass;
typedef struct _RhayaderWebBrowserListModelPrivate RhayaderWebBrowserListModelPrivate;

struct _RhayaderWebBrowserListModel
{
  ThornburyModel parent;

  RhayaderWebBrowserListModelPrivate *priv;
};

struct _RhayaderWebBrowserListModelClass
{
	ThornburyModelClass parent_class;
};

GType rhayader_webbrowser_list_model_get_type (void) G_GNUC_CONST;

RhayaderWebBrowserListModel *rhayader_webbrowser_list_model_get_singleton (void);
void rhayader_webbrowser_list_model_add_entry(RhayaderWebBrowserListModel* self,ClutterActor* tab,gint index);
void rhayader_webbrowser_list_model_remove_entry(RhayaderWebBrowserListModel* self,gint index);
gboolean rhayader_webbrowser_list_model_save (RhayaderWebBrowserListModel *self,
    gint active_tab,
    GError **error);
typedef ClutterActor* (webview_factory)(void);
void rhayader_webbrowser_list_model_restore_last_session(RhayaderWebBrowserListModel* self,webview_factory* webview_create);
gint rhayader_webbrowser_list_model_get_last_session_active_tab(RhayaderWebBrowserListModel* self);



G_END_DECLS

#endif /* _RHAYADER_WEBBROWSER_LIST_MODEL_H */

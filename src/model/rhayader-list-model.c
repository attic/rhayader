/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rhayader-list-model.h"
#include "rhayader-db.h"
#include <libthornbury-ui-texture.h>

#include <errno.h>

#include <canterbury/canterbury.h>

G_DEFINE_TYPE (RhayaderWebBrowserListModel, rhayader_webbrowser_list_model, THORNBURY_TYPE_MODEL)

enum
{
  COLUMN_TAB,
  COLUMN_LAST
};

enum
{
	ITEM_FOCUS,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0, };

#define WEBBROWSER_LIST_MODEL_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), RHAYADER_TYPE_WEBBROWSER_LIST_MODEL, RhayaderWebBrowserListModelPrivate))

struct _RhayaderWebBrowserListModelPrivate
{
	RhayaderWebbrowserDB* m_browser_db;
};

static GObject* rhayader_webbrowser_list_model_contruct(GType gtype, guint n_properties,
		GObjectConstructParam* properties)
{
	static GObject* self = NULL;
	if (self == NULL)
	{

		self = G_OBJECT_CLASS (rhayader_webbrowser_list_model_parent_class)->constructor(
				gtype, n_properties, properties);
		g_object_add_weak_pointer(self, (gpointer) &self);
	}
	else
	{
		g_object_ref(self);
	}

	return self;
}

static void
rhayader_webbrowser_list_model_get_property (GObject    *object,
                                        guint       property_id,
                                        GValue     *value,
                                        GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
rhayader_webbrowser_list_model_set_property (GObject      *object,
                                        guint         property_id,
                                        const GValue *value,
                                        GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
rhayader_webbrowser_list_model_dispose (GObject *object)
{
  G_OBJECT_CLASS (rhayader_webbrowser_list_model_parent_class)->dispose (object);
}

static void
rhayader_webbrowser_list_model_class_init (RhayaderWebBrowserListModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (RhayaderWebBrowserListModelPrivate));

  object_class->constructor = rhayader_webbrowser_list_model_contruct;
  object_class->get_property = rhayader_webbrowser_list_model_get_property;
  object_class->set_property = rhayader_webbrowser_list_model_set_property;
  object_class->dispose = rhayader_webbrowser_list_model_dispose;


  signals[ITEM_FOCUS] =
    g_signal_new ("item_focus",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  0,
                  NULL, NULL,
                  g_cclosure_marshal_VOID__UINT,
                  G_TYPE_NONE, 1,G_TYPE_UINT);
}

static void
rhayader_webbrowser_list_model_init (RhayaderWebBrowserListModel *self)
{
  self->priv = WEBBROWSER_LIST_MODEL_PRIVATE (self);
  self->priv->m_browser_db = rhayader_webbrowser_db_get_singleton ();
}

RhayaderWebBrowserListModel *
rhayader_webbrowser_list_model_get_singleton (void)
{
	RhayaderWebBrowserListModel *self;
	guint n_columns = COLUMN_LAST;
	const gchar * const names[] = { "tab" };
	GType types[] = { G_TYPE_POINTER };

	self = g_object_new (RHAYADER_TYPE_WEBBROWSER_LIST_MODEL, NULL);
	thornbury_model_set_names (THORNBURY_MODEL(self), n_columns, names);
	thornbury_model_set_types (THORNBURY_MODEL(self), n_columns, types);

	return self;
}


static gboolean _rhayader_webbrowser_list_model_notify_cb(RhayaderWebBrowserListModel* self)
{
//	g_print("\n signalling focussing item %d",g_object_get_data(G_OBJECT(self),"focus_item"));
	g_signal_emit(self,signals[ITEM_FOCUS],0,(guint)g_object_get_data(G_OBJECT(self),"focus_item"));
//	g_print("\n signalled focussing item %d",g_object_get_data(G_OBJECT(self),"focus_item"));
	return G_SOURCE_REMOVE;
}


void rhayader_webbrowser_list_model_add_entry(RhayaderWebBrowserListModel* self,ClutterActor* tab,gint index)
{
	if(index < 0)
		index = thornbury_model_get_n_rows(THORNBURY_MODEL(self));
	g_assert(index <= thornbury_model_get_n_rows(THORNBURY_MODEL(self)));

	//g_object_ref(tab); ROHAN
	g_print("\n adding new element at %d num rows=%d",index,thornbury_model_get_n_rows(THORNBURY_MODEL(self)));
	/*thornbury_model_insert(THORNBURY_MODEL(self),
					 index,
					  0,tab, //row entry
					  -1);*/
	thornbury_model_append(THORNBURY_MODEL(self),0,tab,-1);
	g_object_set_data(G_OBJECT(self),"focus_item",(gpointer)index);
	g_timeout_add(250,(GSourceFunc)_rhayader_webbrowser_list_model_notify_cb,self);

}



void rhayader_webbrowser_list_model_remove_entry(RhayaderWebBrowserListModel* self,gint index)
{
	gboolean is_middle = (index >= 0);
	if(!is_middle)
		index = thornbury_model_get_n_rows(THORNBURY_MODEL(self))-1;
	thornbury_model_remove(THORNBURY_MODEL(self),index);
	//g_object_set_data(self,"focus_item",is_middle);
	//g_idle_add((GSourceFunc)_rhayader_webbrowser_list_model_notify_cb,self);
}

void rhayader_webbrowser_list_model_restore_last_session(RhayaderWebBrowserListModel* self,webview_factory* webview_create)
{
	sBrowserTab* tabs;
	guint ind=0;
	guint count = rhayader_webbrowser_db_retrieve_session(self->priv->m_browser_db,&tabs);
   //g_print("\n restoring %d tabs,active=%d",count,active_tab);
	for(ind=0;ind<count;ind++)
	{
		ClutterActor *tab = NULL;
		ClutterActor *texture;

		tab = thornbury_ui_texture_create_new(tabs[ind].thumbnail,0,0,FALSE,FALSE);
		if(tab == NULL)
		 tab = thornbury_ui_texture_create_new(IMAGEDIR"/default_tab.png",0,0,FALSE,FALSE);
		g_assert(CLUTTER_IS_ACTOR(tab));
		g_object_ref(tab);
		g_object_set_data(G_OBJECT(tab),"request_url",(gpointer)(tabs[ind].url));
		texture = thornbury_ui_texture_create_new (tabs[ind].thumbnail, 0, 0, FALSE, FALSE);
		g_object_set(G_OBJECT(tab),"thumbnail",texture,NULL);
		g_object_set(G_OBJECT(tab),"show_thumbnail",TRUE,NULL);
		thornbury_model_insert(THORNBURY_MODEL(self),
							ind,
							0,tab, //row entry
							-1);
	}
	return;

}

gint rhayader_webbrowser_list_model_get_last_session_active_tab(RhayaderWebBrowserListModel* self)
{
	return rhayader_webbrowser_db_retrieve_last_active_tab(self->priv->m_browser_db);
}

gboolean
rhayader_webbrowser_list_model_save (RhayaderWebBrowserListModel *self,
    gint active_tab,
    GError **error)
{
	sBrowserTab tabs[20];
	gchar *url = NULL;
	ThornburyModelIter *iter;
	guint index = 0;
	ClutterActor *tab = NULL;
	const gchar *persistence_path;
	gchar *image_dir;

	if( !thornbury_model_get_n_rows(THORNBURY_MODEL(self)) )
	{
		/* nothing to save */
		return TRUE;
	}

	iter = thornbury_model_get_first_iter (THORNBURY_MODEL (self));

	persistence_path = cby_get_persistence_path (error);

	if (persistence_path == NULL)
	{
		g_prefix_error (error, "Could not save browser state: ");
		return FALSE;
	}

	image_dir = g_build_filename (persistence_path, "images", NULL);

	if (g_mkdir_with_parents (image_dir, 0700) < 0)
	{
		int saved_errno = errno;

		g_set_error (error, G_IO_ERROR,
			g_io_error_from_errno (saved_errno),
			"Could not save browser state: cannot "
			"create \"%s\": %s", image_dir,
			g_strerror (saved_errno));
		g_free (image_dir);
		return FALSE;
	}

	while(!thornbury_model_iter_is_last(iter))
	{
		thornbury_model_iter_get(iter,0,&tab,-1);
		tabs[index].url = NULL;
		//g_object_get(G_OBJECT(tab),"url",&url,NULL);
	//	if(!url)
			url = g_object_get_data(G_OBJECT(tab),"request_url");
		tabs[index].url = g_strdup(url);
		if((tabs[index].url != NULL ) && !g_str_has_prefix(tabs[index].url,"file://") && g_strcmp0(tabs[index].url,"about:blank")){
			gchar *image_name = g_strdup_printf ("%s/image%d.png", image_dir, index);
			gpointer cairo_surface = g_object_get_data (G_OBJECT(tab), "cairo-surface");

			g_print("\n saving %s",tabs[index].url);
			//extract thumbnail from tab
			//tabs[index].thumbnail = g_strdup_printf("%d.png",index);
			if(cairo_surface)
					{
				    cairo_status_t status=cairo_surface_write_to_png((cairo_surface_t *)cairo_surface,image_name);
					g_print("\n %s \n",cairo_status_to_string(status));
					g_print("\n image_name is %s\n",image_name);
					}
			tabs[index].thumbnail = image_name;
			index++;
		}

		thornbury_model_iter_next(iter);
	}
	g_free (image_dir);
	rhayader_webbrowser_db_save_session(self->priv->m_browser_db,tabs,index,active_tab);
	return TRUE;
}


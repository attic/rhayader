/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdlib.h>
#include <glib-object.h>
#include "clutter/clutter.h"
#include <libsoup/soup.h>

#include <canterbury.h>
#include <canterbury/canterbury.h>
#include "controller/rhayader-app.h"

#pragma GCC diagnostic push
/* FIXME: stop disabling these warnings when the libraries are fixed.
 * https://bugs.apertis.org/show_bug.cgi?id=565 (WebKit-Clutter)
 * https://bugs.apertis.org/show_bug.cgi?id=567 (Mx) */
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#include <webkit/webkitiwebview.h>
#pragma GCC diagnostic pop

static MxApplication *global_application;
static MxWindow* window=NULL;
static RhayaderWebBrowser* browser = NULL;
Window win = 0;
//{ "localserver.port", 'p', 0, G_OPTION_ARG_INT, &optionLocalport,
//"localserver port to access the webapp.", 0 },

static CanterburyAppManager *proxy=NULL;
static CanterburyHardKeys* hardkey_proxy = NULL;

static void startup(GApplication *application, const char *startup_uri);
static void register_exit_functionalities(void);
static void
rhayader_webbrowser_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer user_data);
static void
rhayader_webbrowser_call_register_my_app_clb( GObject *source_object,GAsyncResult *res,gpointer user_data);
static gboolean
rhayader_webbrowser_app_state_clb(CanterburyAppManager *object, const gchar *arg_optionAppName,guint arg_app_state,GVariant *arg_arguments);
static void
rhayader_webbrowser_name_vanished(GDBusConnection *connection,const gchar     *name,gpointer         user_data);
static void
rhayader_webbrowser_name_appeared (GDBusConnection *connection,
		const gchar     *name,
		const gchar     *name_owner,
		gpointer         user_data);
//static gboolean _rhayader_webbrowser_check_connectivity();

static gboolean
rhayader_webbrowser_handle_back_press_status_cb( CanterburyHardKeys *object,
		const gchar *arg_optionAppName);



static gchar* optionAppName = NULL;
static gchar* optionWindowsize = "730*480";
static gdouble window_width=730,window_height=480;
static gdouble optionToolbarwidth = 65.0;
static gchar* optionHomePage = NULL;
static gchar* QueryValue = NULL;
static gboolean optionStandalone=FALSE;
static const GOptionEntry BrowserOptionEntries[] =
{
		{ "webapp", 'n', 0, G_OPTION_ARG_STRING, &optionAppName,
				"installed webapp name.", 0 },
				{ "window size", 'w', 0, G_OPTION_ARG_STRING, &optionWindowsize,
						"browser window size.default = 730*480.", 0 },
						{ "toolbar", 't', 0, G_OPTION_ARG_DOUBLE, &optionToolbarwidth,
								"Show toolbars.default=65.0", 0 },
								{ "standalone", 's', 0, G_OPTION_ARG_NONE, &optionStandalone,
										"run standalone w/o app_manager.default=no", 0 },
										{ "homepage", 'h', 0, G_OPTION_ARG_STRING, &optionHomePage,
												"index page .default = news.google.de.", 0 },
												{ 0, 0, 0, 0, 0, 0, 0 }
};

static const GOptionEntry WebruntimeOptionEntries[] =
{
				{ "webapp", 'n', 0, G_OPTION_ARG_STRING, &optionAppName,
						"installed webapp name.", 0 },
						{ "window size", 'w', 0, G_OPTION_ARG_STRING, &optionWindowsize,
								"browser window size.default = 730*480.", 0 },
								{ "standalone", 's', 0, G_OPTION_ARG_NONE, &optionStandalone,
										"run standalone w/o app_manager.default=no", 0 },
										{ "homepage", 'h', 0, G_OPTION_ARG_STRING, &optionHomePage,
												"index page .default = news.google.de.", 0 },
												{ "query", 'q', 0, G_OPTION_ARG_STRING, &QueryValue,
												   "query value to be passed for webapp", 0 },
												{ 0, 0, 0, 0, 0, 0, 0 }
};

static void parse_options(int* argc1, char*** argv1)
{
	GOptionContext* optionContext = g_option_context_new("");
	GOptionGroup *optionGroup;
	int i;
	GError *err = NULL;
	gchar **resol;

#ifndef WEBRUNTIME
	for(i=0 ; i<*argc1 ; i++)
	{
		g_print("\n In BROWSER %s",(*argv1)[i]);
		if(g_strcmp0((*argv1)[i],"url") == 0)
		{
			optionHomePage = g_strdup((*argv1)[i+1]);
			optionAppName = g_strdup (cby_get_bundle_id ());
			return;
		}
	}
#endif
	
#ifndef WEBRUNTIME
	optionGroup = g_option_group_new("WebBrowser",
			"Rhayader WebBrowser runtime",
			"Rhayader WebBrowser runtime options",
			0, 0);
	g_option_group_add_entries(optionGroup, BrowserOptionEntries);

#else

			for (i = 0; i < *argc1; i++)
			{
				g_print("\n In WEBRUNTIME %s", (*argv1)[i]);
				if (g_strcmp0 ((*argv1)[i], "app-name") == 0)
				{
					optionAppName = g_strdup ((*argv1)[i + 1]);
				}
			}

	optionGroup = g_option_group_new("Webruntime",
			"Rhayader Webruntime runtime",
			"Rhayader Webruntime runtime options",
			0, 0);
	g_option_group_add_entries(optionGroup, WebruntimeOptionEntries);
#endif

	g_option_context_set_main_group(optionContext, optionGroup);

	if (!g_option_context_parse(optionContext, argc1, argv1, &err))
	{
		g_error("insufficiant args or failed to parse arguments: %s", err->message);
		return ;
	}
	g_option_context_free(optionContext);

	resol = g_strsplit (optionWindowsize, "*", 2);
	window_width = g_ascii_strtod(resol[0],NULL);
	window_height = g_ascii_strtod(resol[1],NULL);

	if (optionAppName == NULL)
		optionAppName = g_strdup (cby_get_bundle_id ());

#ifdef WEBRUNTIME
	optionToolbarwidth = 0;

		optionHomePage = g_filename_to_uri(
				(gchar *)g_build_filename ( optionHomePage ,g_strdup_printf("%s.html",optionAppName), NULL),
				NULL,NULL);

		  if(QueryValue)
			  optionHomePage = g_strdup_printf("%s#/query/%s",optionHomePage,QueryValue);
#endif



}

static void
startup(GApplication *application, const char *startup_uri)
{

	browser = rhayader_webbrowser_new (optionStandalone, window_width, window_height, optionToolbarwidth);
	register_exit_functionalities();
	window = rhayader_webbrowser_get_window(browser);
	g_assert(MX_WINDOW(window));
	mx_application_add_window(MX_APPLICATION(application), window);


	// The view.
	if(!optionStandalone)
	{
		g_bus_watch_name (G_BUS_TYPE_SESSION,
				"org.apertis.Canterbury",
				G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
				rhayader_webbrowser_name_appeared,
				rhayader_webbrowser_name_vanished,
				window,
				NULL);
	}

	else
	{

		rhayader_webbrowser_initialize(browser,optionHomePage);
		//rhayader_webbrowser_set_connection_status(browser,_rhayader_webbrowser_check_connectivity());
		//rhayader_webbrowser_set_connection_status(browser,TRUE);
	}

	// mx_application_run(application);

}

int launch_webruntime(int argc, char** argv)
{
	ClutterInitError error;

	parse_options(&argc, &argv);
	g_return_val_if_fail (g_application_id_is_valid (optionAppName), 2);
	g_print("\n optionAppName=%s, optionWindowsize=%f*%f,optionToolbarwidth=%f,optionHomePage=%s,optionStandalone=%d",
			optionAppName,window_width,window_height,optionToolbarwidth,optionHomePage,optionStandalone);
	
	error = clutter_init (&argc, &argv);
	g_assert(error == CLUTTER_INIT_SUCCESS);

	//	application = mx_application_new (optionAppName,MX_APPLICATION_SINGLE_INSTANCE);
	global_application = mx_application_new (optionAppName, 0);

	g_signal_connect (global_application, "startup", G_CALLBACK(startup), 0);
	g_application_run (G_APPLICATION (global_application), argc, argv);

#ifndef WEBRUNTIME
	rhayader_webbrowser_save(browser);
#else
	rhayader_webbrowser_stop(browser);
#endif

	return 0;
}

static void rhayader_webbrowser_call_register_my_app_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	GError *error = NULL;

	g_print("\n rhayader_webbrowser_call_register_my_app_clb");
	canterbury_app_manager_call_register_my_app_finish (
			proxy,
			res,
			&error);

}

static void
rhayader_webbrowser_name_appeared (GDBusConnection *connection,
		const gchar     *name,
		const gchar     *name_owner,
		gpointer         user_data)
{
	//GError *error;
	g_print("\n name appeared\n");
	canterbury_app_manager_proxy_new(connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Canterbury",
			"/org/apertis/Canterbury/AppManager",
			NULL,
			rhayader_webbrowser_proxy_clb,
			user_data);


}

static void
rhayader_webbrowser_name_vanished(GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data)
{
	g_print("name_vanished \n");
	if(NULL != proxy)
		g_object_unref(proxy);
}

static void rhayader_webbrowser_proxy_clb( GObject *source_object,
		GAsyncResult *res,
		gpointer user_data)
{
	GError *error;
	/* finishes the proxy creation and gets the proxy ptr */
	proxy = canterbury_app_manager_proxy_new_finish(res , &error);
	if(proxy == NULL) {
		g_print("error %s \n",g_dbus_error_get_remote_error(error));
	}
	g_print("\n rhayader_webbrowser_proxy_clb");
	g_signal_connect(proxy,"new-app-state",(GCallback)rhayader_webbrowser_app_state_clb,user_data);
	canterbury_app_manager_call_register_my_app (
			proxy,
			optionAppName,
			0,
			NULL,
			rhayader_webbrowser_call_register_my_app_clb,
			user_data);

	hardkey_proxy = canterbury_hard_keys_proxy_new_for_bus_sync(
			G_BUS_TYPE_SESSION,G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			"org.apertis.Canterbury","/org/apertis/Canterbury/HardKeys",NULL,NULL);
	g_signal_connect (hardkey_proxy,
			"back-press-register-response",
			G_CALLBACK (rhayader_webbrowser_handle_back_press_status_cb),
			NULL);

	//hardkey/backbutton proxy
	canterbury_hard_keys_call_register_back_press_sync (hardkey_proxy, optionAppName, NULL, NULL);


}

static gboolean
rhayader_webbrowser_initui (gpointer nil G_GNUC_UNUSED)
{
	g_print("\n rhayader_webbrowser_initui.....");
	rhayader_webbrowser_initialize(browser,optionHomePage);
	return FALSE;
}

gboolean rhayader_webbrowser_app_state_clb(CanterburyAppManager *object,
		const gchar *arg_app_name,
		guint arg_app_state,
		GVariant *arg_arguments)
{
	g_print("\n rhayader_webbrowser_app_state_clb");
	if (g_strcmp0 (arg_app_name, optionAppName) == 0)
	{

		switch( arg_app_state )
		{
		case CANTERBURY_APP_STATE_START:
			g_timeout_add(100,rhayader_webbrowser_initui,NULL);
			//g_print("********SUDOKU is in normal state ******** \n" );
			break;

		case CANTERBURY_APP_STATE_BACKGROUND:
			/* see if it can do a clutter actor hide */
			//  g_print("********SUDOKU is in background state ******** \n" );
			break;

		case CANTERBURY_APP_STATE_RESTART:
			/* check if the application needs to resynchronize with its server */
			// g_print("********SUDOKU is in restart state ******** \n" );
			break;

		case CANTERBURY_APP_STATE_OFF:
			/* store all persistence infornmation immediately.. and call your exit 0 else app manager will do it very soon.. 5 sec */
			// g_print("********SUDOKU is in off state ******** \n" );
#ifndef WEBRUNTIME
			rhayader_webbrowser_save(browser);
#else
			rhayader_webbrowser_stop(browser);
#endif

			break;

		case CANTERBURY_APP_STATE_PAUSE:
			/* store all persistence infornmation immediately..*/
			// g_print("********SUDOKU is in pause state ******** \n" );
			break;

		default:
			// g_print("SUDOKU app in unknown state ???  \n" );
			break;
		}

	}

	return TRUE;
}

/*********************************************************************************************
 * Function:    termination_handler
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void rhayader_webbrowser_termination_handler (int signum)
{
	g_print("\n %s !!!!!!termination_handler called.....signum=%d",optionAppName,signum);
	g_application_quit (G_APPLICATION (global_application));
	//	exit(0);
}

/*********************************************************************************************
 * Function:    register_exit_functionalities
 * Description:
 * Parameters:
 * Return:      void
 ********************************************************************************************/
static void register_exit_functionalities(void)
{


	struct sigaction action;
	action.sa_handler = rhayader_webbrowser_termination_handler;
	action.sa_flags = 0;
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGHUP, &action, NULL);
	sigaction(SIGTERM, &action, NULL);
}

static gboolean
rhayader_webbrowser_handle_back_press_status_cb( CanterburyHardKeys *object,
		const gchar *arg_optionAppName)
{
	if (g_strcmp0 (arg_optionAppName, optionAppName) == 0)
	{
		gboolean back_handled = rhayader_webbrowser_goto_previousview(browser);
		canterbury_hard_keys_call_back_press_consumed_sync (hardkey_proxy, optionAppName, back_handled, NULL, NULL);

	}
	return FALSE;
}

/*
static gboolean _rhayader_webbrowser_check_connectivity()
{
	SoupSession* session = webkit_get_default_session();
	SoupMessage* msg = soup_message_new("GET","http://www.google.de");
	guint status = soup_session_send_message(session,msg);
	g_print("\n status=%d ,SOUP_STATUS_CANT_RESOLVE=%d",status,SOUP_STATUS_CANT_RESOLVE);
	return (status == SOUP_STATUS_OK);
} */
